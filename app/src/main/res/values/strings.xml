<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright © 2014 Benedikt Stricker
  ~
  ~ This file is part of Awarenessy.
  ~
  ~ Awarenessy is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ Awarenessy is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ Diese Datei ist Teil von Awarenessy.
  ~
  ~ Awarenessy ist Freie Software: Sie können es unter den Bedingungen
  ~ der GNU General Public License, wie von der Free Software Foundation,
  ~ Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  ~ veröffentlichten Version, weiterverbreiten und/oder modifizieren.
  ~
  ~ Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
  ~ OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  ~ Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  ~ Siehe die GNU General Public License für weitere Details.
  ~
  ~ Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  ~ Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
  -->

<resources>

    <string name="app_name">Awarenessy</string>

    <!-- Activity titles -->
    <string name="title_activity_permission_info">Permission Information</string>
    <string name="title_activity_contact_statistic">Contact Statistic</string>
    <string name="title_activity_log">Logs</string>

    <!-- Service titles -->
    <string name="activity_recognition_service_name">ActivityRecognition</string>

    <!-- Action/Menu names -->
    <string name="action_settings">Settings</string>
    <string name="action_info">Information</string>

    <!-- hint when some features are disabled -->
    <string name="nav_drawer_no_phone">Some functions only work on phones</string>

    <!-- Module names -->
    <string name="overview">Overview</string>
    <string name="check_dangerous_apps">Dangerous Apps</string>
    <string name="sms_statistic">SMS Statistic</string>
    <string name="movement_profile">Movement Profile</string>
    <string name="phone_provider">Phone Provider</string>
    <string name="data_usage">Data Usage</string>
    <string name="privacy">Privacy</string>

    <!-- Nav Drawer Menu Items -->
    <string-array name="nav_drawer_items">
        <item>@string/overview</item>
        <item>@string/check_dangerous_apps</item>
        <item>@string/sms_statistic</item>
        <item>@string/movement_profile</item>
        <item>@string/phone_provider</item>
        <item>@string/data_usage</item>
        <item>@string/privacy</item>
    </string-array>

    <string name="sms">Messages</string>
    <string name="calls">Calls</string>
    <string name="positions">Positions</string>

    <string-array name="db_tables_no_phone">
        <item>@string/positions</item>
    </string-array>

    <string-array name="db_tables">
        <item>@string/sms</item>
        <item>@string/calls</item>
        <item>@string/positions</item>
    </string-array>

    <string name="loading">Loading…</string>
    <string name="exit_hint">Press BACK again to exit the application.</string>

    <!-- Image descriptions -->
    <string name="desc_app_icon">Icon of the Application</string>
    <string name="desc_danger_indicator">Danger of the permission.</string>
    <string name="desc_drawer_item_icon">Item Icon</string>

    <!-- Info dialogs that will be displayed at first module start -->
    <string name="sms_statistic_dialog_info">This module shows some interesting facts and figures about your text messaging.\n\nIn the top area there are some general facts and in the bottom section there are your top messaging contacts.\n\nIf you click on one of them, a detailed statistic about this contact is shown.</string>
    <string name="contact_statistic_dialog_info">This module shows a detailed statistic about your messaging with the clicked contact.\n\nThe lower section shows the most written words in your sms history with this contact.</string>
    <string name="provider_dialog_info">This module shows some information which your phone provider knows about you.\n\nYou provide the most information when signing the phone contract.\nOther information, like the current location, are known due the permanent registration of your device in the phone carriers cell towers.</string>
    <string name="data_usage_dialog_info">This module shows the data traffic your apps have been produced since the last reboot.\n\n<b>Note:</b>\nSome apps can use other apps to do some task. Therefore traffic generated by some activities like watching youtube or app updates from Google Play Store can add traffic to other apps like android.media.</string>
    <string name="movement_dialog_info">This module shows your movement since the installation of the app.\n\nFurthermore your messages and calls will be marked and can be shown or hidden. If you log in to Facebook, you will see places which you have added to your posts.</string>
    <string name="privacy_dialog_info">In this module you can define what data Awarenessy should use.\n\nThe more you allow, the more accurate the information in the other modules become.</string>
    <string name="overview_info_dialog_1">In this module some general information will be displayed.\n\nFor example your most common places in the last two weeks, sorted by day and night.</string>
    <string name="overview_info_dialog_2">Furthermore your travelled distance and your contribution to facebook due your everyday facebook use.</string>
    <!-- Dialog -->
    <string name="start_aw_title">Start Self-Experiment</string>
    <string name="start_aw_detail_phone">Awarenessy wants to make you aware of what data you disclose with your everyday smart phone usage. Therefore different data will be collected (Messages, call history, positions) to visualize them for you in different modules.</string>
    <string name="start_aw_detail_tablet">Awarenessy wants to make you aware of what data you disclose with your everyday tablet usage. Therefore different data will be collected (Messages, call history, positions) to visualize them for you in different modules.</string>
    <string name="start_aw_important"><b>IMPORTANT: </b>The data is only stored locally on your device, will never be send anywhere and will only be used for the purpose of this app.</string>
    <string name="start_aw_used_data">At the start following data will be used:</string>
    <string name="start_aw_change_data">This can be changed anytime in \'%1$s\'.</string>

    <!-- Buttons -->
    <string name="start_aw_ok">Give it a try!</string>
    <string name="start_later">Maybe later …</string>


    <string name="app_id">1498095313743237</string>
    <string name="fb_welcome">Welcome %1$s %2$s</string>
    <string name="fb_postings">Facebook Postings</string>
    <string name="update_sms_dialog_titel">Updating SMS database…</string>

    <string name="take_a_while_message">This can take some time.</string>


</resources>
