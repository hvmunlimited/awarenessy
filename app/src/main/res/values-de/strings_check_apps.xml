<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright © 2014 Benedikt Stricker
  ~
  ~ This file is part of Awarenessy.
  ~
  ~ Awarenessy is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ Awarenessy is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~ Diese Datei ist Teil von Awarenessy.
  ~
  ~ Awarenessy ist Freie Software: Sie können es unter den Bedingungen
  ~ der GNU General Public License, wie von der Free Software Foundation,
  ~ Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  ~ veröffentlichten Version, weiterverbreiten und/oder modifizieren.
  ~
  ~ Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
  ~ OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  ~ Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  ~ Siehe die GNU General Public License für weitere Details.
  ~
  ~ Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  ~ Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
  -->

<resources>

    <string name="permission_info_normal">Eine Berechtigung mit niedrigem Risiko, die nur Anfragen zu isolierten App Funktionen gewährt.
        Diese Berechtigungen bergen nur minimale Gefahr für das System, anderer Apps oder den Benutzer. Android gewährt diese Berechtigungen automatisch ohne den Benutzer zu fragen (obwohl der Benutzer immer die Möglichkeit hat, diese vor der Installation zu prüfen). Z. B. den Vibrationsalarm zu steuern oder WLAN-Verbindungen abrufen.</string>
    <string name="permission_info_signature">Eine Berechtigung, die von anderen Apps gewährt werden kann, um so auf deren Daten zuzugreifen. Diese Berechtigungen
        erlaubt Android nur, wenn die App, die diese Berechtigung benötigt, das gleiche Zertifikat besitzt wie die App die diese Berechtigung vergibt. Ist dies der Fall, vergibt Android diese Berechtigung automatisch, ohne den Benutzer zu benachrichtigen oder explizit zu fragen. Z. B. Daten zur Google Cloud senden (C2D_Message).</string>
    <string name="permission_info_dangerous">Eine Berechtigung mit hohem Risiko, die der App Zugang zu privaten Benutzerdaten, Kontrolle über das Gerät oder negative Auswirkungen auf den Benutzer haben könnte. Da diese Berechtigungen ein potenzielles Risiko für den Benutzer darstellen, müssen diese explizit vom Benutzer bestätigt werden und können nicht vom System automatisch gewährt werden. Z.B. Daten ändern oder löschen oder SMS empfangen und senden.</string>
    <string name="permission_info_sig_or_system">Eine Berechtigung, die nur Apps gewährt werden, die mit dem Android System installiert werden, oder die das gleichen Zertifikat besitzt wie die App die diese Berechtigung vergibt. Diese Berechtigung sollte meist nicht nötig sein, da die \'Signatur-Berechtigung\' meist genügt. Ausnahmen sind hierbei Hersteller die ihre eigene Benutzeroberfläche bieten (z.B. Samsung, HTC, Sony, …), wo eine Verankerung im System nötig ist.</string>
    <string name="permission_info_dev">Zusätzliche Kennzeichnung einer Berechtigung, die nur während der Entwicklung der App verwendet werden sollte.</string>
    <string name="permission_info_system">Zusätzliche Kennzeichnung einer Berechtigung, die jeder System App gewährt werden kann. Diese Kennzeichung sollte meist nicht nötig sein, da die \'Signatur-Berechtigung\' meist genügt. Ausnahmen sind hierbei Hersteller die ihre eigene Benutzeroberfläche bieten (z.B. Samsung, HTC, Sony, …), wo eine Verankerung im System nötig ist.</string>
    <string name="permission_info_system_and_dev">Zusätzliche Kennzeichnungen einer Berechtigung:\n\n<b>System</b>\nZusätzliche Kennzeichnung einer Berechtigung, die jeder System App gewährt werden kann. Diese Kennzeichung sollte meist nicht nötig sein, da die \'Signatur-Berechtigung\' meist genügt. Ausnahmen sind hierbei Hersteller die ihre eigene Benutzeroberfläche bieten (z.B. Samsung, HTC, Sony, …), wo eine Verankerung im System nötig ist.\n\n<b>Entwicklung</b>\nZusätzliche Kennzeichnung einer Berechtigung, die nur während der Entwicklung der App verwendet werden sollte.    </string>
    <string name="no_app_installed">Keine Apps installiert</string>

    <string name="dangerous_app">Gefährliche App</string>
    <string name="med_dangerous_app">Moderat gefährliche App</string>
    <string name="harmless_app">Ungefährliche App</string>
    <string name="info_check_apps">Awarenessy überprüft die installierten Apps und prüft sie auf ihr potentielles Risiko für deine Daten und Privatsphäre.
        Dazu bewertet es die App anhand der Berechtigungen die benötigt werden und vergibt pro App \'Risiko-Punkte\', mithilfe derer die App in einer von drei Klassen eingeteilt wird:
        </string>
    <string name="info_check_apps2">Je höher der Risiko-Punktestand ist, desto höher ist die potentielle Gefahr für deine Daten.</string>

    <string name="info_check_apps_note">
      <b>Hinweis:</b>\nEinige vorinstallierte Apps wie \'Einstellungen\' oder manche Google Apps benötigen viele Berechtigungen da sie sehr tief im Android System eingebunden sind.
        Trotzdem können sie als ungefährlich betrachtet werden.
    </string>


    <!-- Different kinds of permissions-->
    <string name="perm_danger">Gefährliche Berechtigung</string>
    <string name="perm_normal">Normale Berechtigung</string>
    <string name="perm_signature">Signatur-Berechtigung</string>
    <string name="perm_sign_or_system">Signatur- oder Systemberechtigung</string>

    <!-- Additional flags for permissions-->
    <string name="perm_flag_systemAndDev">System- und Entwicklungsberechtigung</string>
    <string name="perm_flag_system">Systemberechtigung</string>
    <string name="perm_flag_dev">Entwicklungsberechtigung</string>

    <string name="no_permissions">Diese App benötigt keine Berechtigungen</string>
    <string name="app_detail_hint">Wähle links eine App aus</string>
    <string name="hint_search_app">Suche App…</string>


</resources>