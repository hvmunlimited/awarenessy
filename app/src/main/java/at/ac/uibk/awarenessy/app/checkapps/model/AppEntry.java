/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps.model;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import at.ac.uibk.awarenessy.app.utils.Log;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import at.ac.uibk.awarenessy.app.BuildConfig;

/**
 * Class that represents a installed application on the device.
 * <p/>
 * Contains information about the name, icon, danger of the app.
 */
public class AppEntry implements Parcelable {


    private static final String TAG = "AppEntry";

    private final PackageInfo mPackageInfo;
    private final String      mName;
    private final Drawable    mIcon;

    // will be calculated in constructor
    private final List<MyPermissionInfo> mPermissions;

    // will be calculated in constructor
    private final Danger mDanger;


    /**
     * Class Constructor
     * <p/>
     * Create a new {@link AppEntry} object that
     * represents a installed application on the device.
     * <p/>
     * The first parameter is needed to obtain access to the apps resources like Strings and Icons.
     * <p/>
     * The second parameter is the main part of this class and is obtained from {@link android
     * .content.pm.PackageManager}.
     *
     * @param context     context of the calling class
     * @param packageInfo package of the app
     */
    public AppEntry(Context context, PackageInfo packageInfo) {

        mPackageInfo = packageInfo;
        mName = packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString();
        mIcon = packageInfo.applicationInfo.loadIcon(context.getPackageManager());
        mPermissions = loadPermissionInfoList(context);
        mDanger = rateApp();

    }


    /**
     * Perform alphabetical comparison of application entry objects with the use of {@link java.text.Collator}.
     */
    public static final Comparator<AppEntry> ALPHA_COMPARATOR = new Comparator<AppEntry>() {

        // use Collator for advanced comparison (respects Locales)
        private final Collator sCollator = Collator.getInstance();

        @Override
        public int compare(AppEntry appEntry, AppEntry appEntry2) {
            return sCollator.compare(appEntry.getName(), appEntry2.getName());
        }
    };

    /**
     * Perform comparison between danger points of application entry objects.
     */
    public static final Comparator<AppEntry> DANGER_COMPARATOR = new Comparator<AppEntry>() {


        @Override
        public int compare(AppEntry appEntry, AppEntry appEntry2) {
            return appEntry2.getDanger().getDangerPoints() - appEntry.getDanger().getDangerPoints();
        }
    };


    /**
     * Returns the {@link android.content.pm.PackageInfo} of the app.
     * This contains most information about the app, like name, icon, permissions, flags, ... and is the main class member of this class.
     *
     * @return the PackageInfo (main information) of the app
     */
    public PackageInfo getPackageInfo() {
        return mPackageInfo;
    }


    /**
     * Returns the readable name (how it will be displayed on the device) of the app.
     *
     * @return the readable app name
     */
    public String getName() {


        return mName;
    }

    /**
     * Returns the Icon of this App.
     *
     * @return Drawable Icon of the App
     */
    public Drawable getIcon() {


        return mIcon;
    }


    /**
     * Returns the list of permissions of the app or an apps_empty_permissions list if this app needs no permission.
     *
     * @return list of permissions
     */
    public List<MyPermissionInfo> getPermissions() {
        return mPermissions;
    }

    /**
     * Returns the danger of this app, or null if the app hasn't been rated yet.
     *
     * @return danger or null, if not rated yet
     */
    public Danger getDanger() {
        return mDanger;
    }


    /**
     * Returns the PermissionInfo objects for the Permission Strings.
     * <p/>
     * PermissionInfo contains the Name, Description, Group, ... of the Permission, whereas the Permission string only contains a constant package name like "android.permission.CAMERA".
     *
     * @return list of PermissionInfo
     */
    private List<MyPermissionInfo> loadPermissionInfoList(Context context) {

        String[] perms = mPackageInfo.requestedPermissions;


        // if no permission are needed, return apps_empty_permissions List
        if (perms == null) {
            return new ArrayList<MyPermissionInfo>();
        }

        List<MyPermissionInfo> list = new ArrayList<MyPermissionInfo>(perms.length);

        for (String perm : perms) {

            try {
                PermissionInfo permInfo = context.getPackageManager().getPermissionInfo(perm, 0);

                MyPermissionInfo newPermInfo = new MyPermissionInfo(context, permInfo);

                list.add(newPermInfo);

            } catch (PackageManager.NameNotFoundException e) {
//                Log.d(TAG, "Ignoring unknown permission:" + perm);
            }

        }

        return list;

    }

    /**
     * Rate the specified app.
     * <p/>
     * Categorizes the specified app on the basis of their requested Permissions.
     * <p/>
     * Each permission is has a protection level that characterize the potential risk of the app requesting this permission.
     * Each protection level is certain "danger points" worth:
     * <p/>
     * - normal permission      1 point
     * <p/>
     * - dangerous permission   5 points
     * <p/>
     * - signature permission   2 point
     * <p/>
     * - signature or system permission 3 points
     * <p/>
     * Signature permission:  Special permissions that will be granted automatically if the requesting application is signed with the same certificate as the application that declared the permission.
     * <p/>
     * Signature or system permission: Special permission that the system grants only to applications that are in the Android system image or that are signed with the same certificate as the application that declared the permission. Should only be used in special situations, use signature permission when possible.0
     * <p/>
     * According to the danger points a app gets for its whole permissions, the app will be categorized into different categories.
     * <p/>
     * Categories are defined in {@link EnumDanger}.
     * <p/>
     *
     * @return Danger of this app
     * @see <a href="http://developer.android.com/guide/topics/manifest/permission-element.html">Permission Element</a>  on <a href="http://developer.android.com">developer.android.com</a>
     */
    private Danger rateApp() {

        Danger danger;
        int dangerPoints = 0;


        for (MyPermissionInfo perm : mPermissions) {

            dangerPoints += perm.getDangerPoints();

        }

        dangerPoints /= 2;

        // danger points cap
        if (dangerPoints > 100) {
            dangerPoints = 100;
        }
        danger = new Danger(dangerPoints);



//            Log.d(TAG, "Danger-Rating of " + mName + ": " + danger.getDanger() + "(" + dangerPoints + ")");

        return danger;

    }


    @Override
    public String toString() {
        return mName;
    }

    /*
     *   Parcelable Implementation
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {

        out.writeString(mName);
        out.writeParcelable(mPackageInfo, flags);
        out.writeParcelable(mDanger, flags);
        out.writeTypedList(mPermissions);

        if (mIcon != null) {
            Bitmap bitmap = ((BitmapDrawable) mIcon).getBitmap();
            out.writeParcelable(bitmap, flags);
        } else {
            out.writeParcelable(null, flags);
        }


    }

    public static final Creator<AppEntry> CREATOR = new Creator<AppEntry>() {

        @Override
        public AppEntry createFromParcel(Parcel parcel) {
            return new AppEntry(parcel);
        }

        @Override
        public AppEntry[] newArray(int size) {
            return new AppEntry[size];
        }
    };

    private AppEntry(Parcel source) {

        mName = source.readString();
        mPackageInfo = source.readParcelable(PackageInfo.class.getClassLoader());
        mDanger = source.readParcelable(Danger.class.getClassLoader());
        mPermissions = source.createTypedArrayList(MyPermissionInfo.CREATOR);

        Bitmap bitmap = source.readParcelable(getClass().getClassLoader());
        if (bitmap != null) {
            mIcon = new BitmapDrawable(bitmap);
        } else {
            mIcon = null;
        }

    }


}
