/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.util.Pair;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.CallDao;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.dao.SmsDao;
import de.greenrobot.dao.query.CloseableListIterator;
import de.greenrobot.dao.query.QueryBuilder;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Statistic class that is responsible for calculating the message statistic.
 * <p/>
 * Provide different metrics:
 * - First Message overall
 * - Last Messge overall
 * - First Message of a specific contact
 * - Last Message of a specific contact
 * - Sum of overall sent sms
 * - Sum of overall received sms
 * - Sum of sent sms of a specific contact
 * - Sum of received sms of a specfic contact
 * - Sum of overall sms
 * - Sum of sms of a specific contact
 * - Overall top words
 * - Top words of a specific contact
 */
public class SmsStatistic {

    private static final String TAG = SmsStatistic.class.getSimpleName();

    private static final Pattern PATTERN         = Pattern.compile("(\\s|\\p{Punct})+");
    private static final int     MIN_WORD_LENGTH = 2;
    public static final  int     MIN_FREQUENCY   = 5;

    private final Set<String> mStopWords = new HashSet<String>();

    // need db instead of DaoSession for the raw-query in calcTop
    private final SQLiteDatabase mDatabase;

    private final SmsDao  mSmsDao;

    public SmsStatistic(Context context) {


        DaoSession daoSession = Utils.getDaoSession(context);

        mDatabase = daoSession.getDatabase();
        mSmsDao = daoSession.getSmsDao();

        // read all stopwords and add them to the set
        Collections.addAll(mStopWords, context.getResources().getStringArray(R.array.stop_words));

    }


    public Pair<String, Integer> getMostPhonedContact() {


        // use rawquery because either GreenDao nor Android-SQLite query provide proper count
        // querying
        String rawQuery =
                "SELECT " + CallDao.Properties.Number.columnName + ", " +
                "count(" + CallDao.Properties.Number.columnName + ")" +
                " FROM " + CallDao.TABLENAME +
                " GROUP" + " BY " + CallDao.Properties.Number.columnName +
                " ORDER BY count(" + CallDao.Properties.Number.columnName + ")" +
                " DESC";


        Cursor cursor = mDatabase.rawQuery(rawQuery, null);


        Pair<String, Integer> topContact = null;
        String number;
        int count;

        if (cursor.moveToNext()) {

            // SELECT NUMBER, count(NUMBER) FROM CALL GROUP BY NUMBER ORDER
            // BY count(NUMBER) DESC
            number = cursor.getString(cursor.getColumnIndexOrThrow(CallDao.Properties.Number
                    .columnName));

            count = cursor.getInt(cursor.getColumnIndexOrThrow("count(" + CallDao.Properties
                    .Number.columnName + ")"));


            Log.d(TAG, "Number: " + number + " count: " + count);


            topContact = new Pair<String, Integer>(number, count);


        }
        return topContact;
    }


    /**
     * Return the contact with the most sms received and sent.
     *
     * @return Pair that first element contains the name of the contact and the second the number of sms
     */
    public Pair<String, Integer> getMostSmsContact() {
        List<Map.Entry<String, SmsInterface.SmsCount>> topContacts = getTopContacts
                (1);

        if (topContacts.isEmpty()) {
            return null;
        }

        Map.Entry<String, SmsInterface.SmsCount> mostSmsContact = topContacts.get(0);

        int overallSms = mostSmsContact.getValue().getOverall();
        return new Pair<String, Integer>(mostSmsContact.getKey(), overallSms);

    }

    /**
     * Calculate and return top contacts based on the sum of send and received sms. Number of
     * contacts is set via the parameter.
     *
     * @param top Number of contacts that should be returned
     *
     * @return List of contacts with their corresponding sms count
     */
    public List<Map.Entry<String, SmsInterface.SmsCount>> getTopContacts( int top ) {


        Map<String, SmsInterface.SmsCount> topContacts = calcTop();

        Set<Map.Entry<String, SmsInterface.SmsCount>> set = topContacts.entrySet();


        List<Map.Entry<String, SmsInterface.SmsCount>> list = new ArrayList<Map.Entry<String,
                SmsInterface.SmsCount>>(set);


        Collections.sort(list, OVERALL_SMS_COMPARATOR);


        if ( top > list.size() ) {
            top = list.size();
        }

        return list.subList(0, top);

    }

    /**
     * Comparator to compare two top contacts. Ordering according to the overall sms of the
     * contact (sent + received)
     */
    public static final Comparator<Map.Entry<String, SmsInterface.SmsCount>>
            OVERALL_SMS_COMPARATOR = new Comparator<Map.Entry<String, SmsInterface.SmsCount>>() {
        @Override
        public int compare( Map.Entry<String, SmsInterface.SmsCount> lhs, Map.Entry<String,
                SmsInterface.SmsCount> rhs ) {

            SmsInterface.SmsCount lhsValue = lhs.getValue();
            SmsInterface.SmsCount rhsValue = rhs.getValue();

            int lhsSum = lhsValue.received + lhsValue.sent;
            int rhsSum = rhsValue.received + rhsValue.sent;

            return rhsSum - lhsSum;
        }
    };

    /**
     * Comparator to compare two top words.
     */
    public static final Comparator<Map.Entry<String, Integer>> WORD_COUNT_COMPARATOR = new
            Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare( Map.Entry<String, Integer> lhs, Map.Entry<String,
                        Integer> rhs ) {

                    Integer lhsCount = lhs.getValue();
                    Integer rhsCount = rhs.getValue();


                    return rhsCount - lhsCount;
                }
            };


    /**
     * Helper method that calculates the top contacts.
     *
     * @return Map with contacts and corresponding sms counts.
     */
    private Map<String, SmsInterface.SmsCount> calcTop() {

        String number;
        int count;
        boolean isSentEntry;

        Map<String, SmsInterface.SmsCount> topContacts = new TreeMap<String,
                SmsInterface.SmsCount>();


        String queryString = "SELECT " + SmsDao.Properties.Number.columnName + ", " +
                "" + SmsDao.Properties.Sent.columnName + ", count(" + SmsDao.Properties.Number
                .columnName + ") FROM " + mSmsDao.getTablename() + " GROUP BY " + SmsDao
                .Properties.Number.columnName + ", " + SmsDao.Properties.Sent.columnName + " " +
                "ORDER BY count(" + SmsDao.Properties.Message.columnName + ") DESC";

        Cursor cursor = mDatabase.rawQuery(queryString, null);

        while ( cursor.moveToNext()) {

            // SELECT NUMBER, SENT, count(NUMBER) FROM SMS GROUP BY NUMBER,
            // SENT ORDER BY count(MESSAGE) DESC
            number = cursor.getString(cursor.getColumnIndexOrThrow(SmsDao.Properties.Number
                                                                           .columnName));

            isSentEntry = Utils.SQLiteBoolToBool(cursor.getInt(cursor.getColumnIndex(SmsDao.Properties.Sent.columnName)));

            count = cursor.getInt(cursor.getColumnIndexOrThrow("count(" + SmsDao.Properties
                    .Number.columnName + ")"));


            SmsInterface.SmsCount sms = topContacts.get(number);

            Log.d(TAG, "Number: " + number + (isSentEntry ? " Send: " : " Received: ") + count);


            // fill sms counter
            if ( sms != null ) {

                if ( isSentEntry ) {
                    sms.sent = count;
                } else {
                    sms.received = count;
                }

                topContacts.put(number, sms);

            } else {

                if ( isSentEntry ) {
                    sms = new SmsInterface.SmsCount(count, 0);
                } else {
                    sms = new SmsInterface.SmsCount(0, count);
                }

                topContacts.put(number, sms);

            }

        }

        return topContacts;

    }


    /**
     * Returns the first message (sent or received) of this number or null if no message found.
     *
     * @param number Number of which the first message should be returned
     *
     * @return the first message (sent or received) of this contact or null if no message found
     */
    public Sms getFirstMessage( String number ) {

        return mSmsDao.queryBuilder().orderAsc(SmsDao.Properties.Time).where(SmsDao.Properties.Number.eq(number)).limit(1).unique();

    }

    /**
     * Returns the first message that has been sent or received or null if no message found.
     *
     * @return the first message (sent or received) or null if no message found
     */
    public Sms getFirstMessage() {

        return mSmsDao.queryBuilder().orderAsc(SmsDao.Properties.Time).limit(1)
                .unique();

    }

    /**
     * Returns the last message (sent or received) of this number or null if no message found.
     *
     * @param number Number of which the last message should be returned
     *
     * @return the last message (sent or received) of this contact or null if no message found
     */
    public Sms getLastMessage( String number ) {

        return mSmsDao.queryBuilder().orderDesc(SmsDao.Properties.Time).where(SmsDao.Properties.Number.eq(number)).limit(1).unique();

    }

    /**
     * Returns the last message that has been sent or received or null if no message found.
     *
     * @return the last message (sent or received) or null if no message found
     */
    public Sms getLastMessage() {

        return mSmsDao.queryBuilder().orderDesc(SmsDao.Properties.Time).limit(1)
                .unique();


    }

    /**
     * Returns the number of sent message to the specified number.
     *
     * @param number telephone number of which the number of send messages should return
     *
     * @return number of sent message to this telephone number
     */
    public long getSumSent( String number ) {
        QueryBuilder qb = mSmsDao.queryBuilder();

        // Count elements which are sent messages AND from the specified number
        return qb.where(SmsDao.Properties.Sent.eq(true), SmsDao.Properties.Number.eq(number))
                .count();

    }

    /**
     * Returns the number of overall sent mesages.
     *
     * @return number of overall sent messages
     */
    public long getSumSent() {

        // Count elements which are sent messages
        return mSmsDao.queryBuilder().where(SmsDao.Properties.Sent.eq(true)).count();


    }

    /**
     * Returns the number of received message of the specified number.
     *
     * @param number telephone number of which the number of received messages should return
     *
     * @return number of received message of this telephone number
     */
    public long getSumReceived( String number ) {

        QueryBuilder qb = mSmsDao.queryBuilder();

        // Count elements which are not sent messages AND from the specified number
        return qb.where(SmsDao.Properties.Sent.eq(false), SmsDao.Properties.Number.eq(number))
                .count();

    }


    /**
     * Returns the number of overall received mesages.
     *
     * @return number of overall received messages
     */
    public long getSumReceived() {

        QueryBuilder qb = mSmsDao.queryBuilder();

        // Count elements which are not sent messages
        return qb.where(SmsDao.Properties.Sent.eq(false)).count();

    }

    /**
     * Returns the number of overall sms (sent and received).
     *
     * @return number of overall sms
     */
    public long getSum() {

        return getSumReceived() + getSumSent();

    }

    /**
     * Returns the number of overall sms (sent and received) for the specified number.
     *
     * @param number telephone number of which the overall number of messages should return
     *
     * @return number of overall sms for the specified number
     */
    public long getSum( String number ) {
        return getSumReceived(number) + getSumSent(number);
    }


    /**
     * Returns the top words of the specified telephone number up to the given limit.
     * Returned list are map entries containing the top word and the occurence of this word in the
     * messages to and from
     * the specified number.
     *
     * @param number telephone number of which the top words should be returned
     * @param limit  number of top words that should be returned
     *
     * @return List of top words for the given telephone number.
     */
    public List<Map.Entry<String, Integer>> getTopWordsToNumber( String number, int limit ) {


        CloseableListIterator<Sms> iter = mSmsDao.queryBuilder().where(SmsDao.Properties.Number
                                                                               .eq(number))
                .listIterator();

        return getTopWords(iter, limit);

    }

    /**
     * Returns the top words of all numbers up to the given limit.
     * Returned list are map entries containing the top word and the occurence of this word in all
     * messages.
     *
     * @param limit number of top words that should be returned
     *
     * @return List of top words
     */

    public List<Map.Entry<String, Integer>> getTopWords( SmsInterface.SmsType type, int limit ) {

        QueryBuilder<Sms> query = mSmsDao.queryBuilder();

        // build query to get only the sms types we want
        switch ( type ) {

            case SENT:
                query.where(SmsDao.Properties.Sent.eq(true));
                break;
            case RECEIVED:
                query.where(SmsDao.Properties.Sent.notEq(true));
                break;
            case BOTH:
                // no need to constrain query
                break;
        }

        CloseableListIterator<Sms> iter = query.listIterator();


        return getTopWords(iter, limit);

    }


    /**
     * Helper method that calculate and return the top words for the given iterator which is
     * returned from a greendao
     * db-query.
     * Iterates over each message, checks if the word is valid (no stop word, not too short) and
     * adds it then to the
     * returning map.
     *
     * @param iter ListIterator to get the words of the contact
     *
     * @return Map which contains the top words
     */
    private Map<String, Integer> calcTopWords( CloseableListIterator<Sms> iter ) {


        Map<String, Integer> topWords = new HashMap<String, Integer>();

        try {
            while ( iter.hasNext() ) {
                String message = iter.next().getMessage();
                String[] words = PATTERN.split(message);

                for ( String word : words ) {

                    word = word.toLowerCase(Locale.getDefault());

                    if ( isValid(word) ) {

                        int count = getCount(word, topWords) + 1;
                        topWords.put(word, count);
                    }
                }

            }
        } finally {
            try {
                iter.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }

        }

        cleanTopWords(topWords);


        return topWords;

    }

    /**
     * Helper method that returns the top words for the given iterator up to the given limit.
     * Returns a sorted list with map entries containing the top word and the occurence of this
     * word
     * in this iterator.
     * The list is ordered by descending word occurence.
     *
     * @param limit number of top words that should be returned
     *
     * @return List of top words
     */
    private List<Map.Entry<String, Integer>> getTopWords( CloseableListIterator<Sms> iter,
                                                          int limit ) {


        Map<String, Integer> topWords = calcTopWords(iter);

        Set<Map.Entry<String, Integer>> set = topWords.entrySet();


        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(set);


        Collections.sort(list, WORD_COUNT_COMPARATOR);

        int safeLimit = getSafeLimit(list, limit);


        return list.subList(0, safeLimit);


    }

    /**
     * Returns the maximal limit of the specified list.
     * If the list is shorter then the desired limit, the size of the list is returned else the
     * limit is returned.
     *
     * @param list  List of which the safe limit should be returned
     * @param limit the maximal limit
     *
     * @return the maximal limit for the list
     */
    private int getSafeLimit( List<?> list, int limit ) {

        int size = list.size();

        if ( size > limit ) {
            return limit;
        } else {
            return size;
        }

    }


    /**
     * Checks wether a certain word is valid (should be in the top words list) or not.
     * It has to have a certain length and must not appear in the stop word list.
     *
     * @param word word to check if it's valid
     *
     * @return true if the word is valid, false otherwise
     */
    private boolean isValid( String word ) {
        return word.length() >= MIN_WORD_LENGTH && !mStopWords.contains(word);
    }

    private void cleanTopWords( Map<String, Integer> topWords ) {

        Log.d(TAG, "Remove words with frequency < " + MIN_FREQUENCY);

        Iterator<Map.Entry<String, Integer>> iter = topWords.entrySet().iterator();

        while ( iter.hasNext() ) {
            Map.Entry<String, Integer> word = iter.next();

            Integer frequency = word.getValue();

            if ( (frequency <= MIN_FREQUENCY) ) {
                Log.d(TAG, "Removed: " + word.getKey() + "\tFrequency: " + frequency);
                iter.remove();
            }

        }

    }

    /**
     * Helper method that returns the occurence of the specified word in the top words map.
     *
     * @param word     word to check the occurence
     * @param topWords map of top words where the occurence of the word should be checked
     *
     * @return number of occurences of this word
     */
    private int getCount( String word, Map<String, Integer> topWords ) {
        if ( topWords.containsKey(word) ) {
            return topWords.get(word);
        } else {
            return 0;
        }
    }


}
