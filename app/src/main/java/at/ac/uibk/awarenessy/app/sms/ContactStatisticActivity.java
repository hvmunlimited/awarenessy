/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.app.sms.adapter.TopWordAdapter;
import at.ac.uibk.awarenessy.app.utils.Utils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Activity that shows the top word of a specific contact.
 */
public class ContactStatisticActivity extends Activity {


    private static final String TAG       = ContactStatisticActivity.class.getSimpleName();
    private static final int    TOP_WORDS = 20;


    private TextView     mFirstMessageView;
    private TextView     mLastMessageView;
    private SmsStatistic mStatistic;

    private String   mNumber;
    private TableRow mFirstMessageRowView;
    private TableRow mLastMessageRowView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.info_menu, menu);

        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_statistic_fragment);

        // show Up Button
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // get the number of the contact
        Intent intent = getIntent();
        mNumber = intent.getStringExtra(SmsStatisticFragment.EXTRA_NUMBER);
        String name = SmsInterface.getName(mNumber, this);


        // set title to the current contact name
        TextView title = (TextView) findViewById(android.R.id.title);
        title.setText(name);

        TextView overallView = (TextView) findViewById(R.id.overall);
        TextView overallSentView = (TextView) findViewById(R.id.overallSent);
        TextView overallReceivedView = (TextView) findViewById(R.id.overallReceived);

        mFirstMessageView = (TextView) findViewById(R.id.firstMessage);
        mLastMessageView = (TextView) findViewById(R.id.lastMessage);

        mFirstMessageRowView = (TableRow) findViewById(R.id.firstMessageRow);
        mLastMessageRowView = (TableRow) findViewById(R.id.lastMessageRow);


        ListView topWordsListView = (ListView) findViewById(android.R.id.list);
        TextView emptyView = (TextView) findViewById(android.R.id.empty);
        emptyView.setText(getString(R.string.empty_list_top_words, SmsStatistic.MIN_FREQUENCY));
        topWordsListView.setEmptyView(emptyView);

        mStatistic = new SmsStatistic(this);

        // get the top words of the number
        List<Map.Entry<String, Integer>> data = mStatistic.getTopWordsToNumber(mNumber, TOP_WORDS);

        TopWordAdapter adapter = new TopWordAdapter(this, data);
        topWordsListView.setAdapter(adapter);

        setFirstMessage();
        setLastMessage();

        // get amount of the sent and received messages for this contact
        long sent = mStatistic.getSumSent(mNumber);
        long received = mStatistic.getSumReceived(mNumber);


        overallView.setText(String.valueOf(sent + received));
        overallSentView.setText(String.valueOf(sent));
        overallReceivedView.setText(String.valueOf(received));

        SharedPreferences settings = getSharedPreferences(Preferences.PREF_FLAGS.PREF_NAME, MODE_PRIVATE);
        boolean showInfo = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_CONTACT_STATISTIC_INFO, true);

        if (showInfo)
            showInfoDialog();

    }

    private void showInfoDialog() {

        // Build a new Dialog to tell the user to restart the phone
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.title_activity_contact_statistic);

        builder.setMessage(R.string.contact_statistic_dialog_info);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Restore preferences
                SharedPreferences settings = getSharedPreferences(Preferences.PREF_FLAGS.PREF_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_CONTACT_STATISTIC_INFO, false);

                // Commit the edits!
                editor.commit();
            }
        });

        builder.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_info:

                showInfoDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    /**
     * Set the first sent or received message of the contact.
     */
    private void setFirstMessage() {

        Sms firstMessage = mStatistic.getFirstMessage(mNumber);
        Date date = new Date(firstMessage.getTime());

        if (firstMessage != null) {
            // Listener that opens a dialog and shows the respective Message
            mFirstMessageRowView.setOnClickListener(new MessageOnClickListener(this, firstMessage));

            mFirstMessageView.setText(Utils.DATE_FORMAT.format(date));
        }

    }

    /**
     * Set the last sent or received mesasge of the contact.
     */
    private void setLastMessage() {

        Sms lastMessage = mStatistic.getLastMessage(mNumber);
        Date date = new Date(lastMessage.getTime());

        if (lastMessage != null) {
            // Listener that opens a dialog and shows the respective Message
            mLastMessageRowView.setOnClickListener(new MessageOnClickListener(this, lastMessage));

            mLastMessageView.setText(Utils.DATE_FORMAT.format(date));
        }

    }
}
