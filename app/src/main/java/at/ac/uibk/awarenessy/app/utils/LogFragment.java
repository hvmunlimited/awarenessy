/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.utils;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spanned;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.List;

import at.ac.uibk.awarenessy.app.R;

/**
 * ListFragment that displays a simple Logfile.
 * <p/>
 * The name of the logfile has to be passed as an argument.
 */
public class LogFragment extends ListFragment {

    public static final  String            ARG_LOG_FILE_NAME  = LogFragment.class.getName()
            .concat(".ARG_LOG_FILE_NAME");
    private static final String            TAG                = LogFragment.class.getSimpleName();
    /**
     * Broadcast receiver that receives activity update intents. It checks to see if the ListView
     * contains items. If it doesn't, it pulls in history. This receiver is local only. It can't
     * read broadcast Intents from other apps.
     */
    private final        BroadcastReceiver updateListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*
             * When an Intent is received from the update listener IntentService, update
             * the displayed log.
             */
            updateActivityRecognitionHistory();

        }
    };
    private ArrayAdapter<Spanned> mStatusAdapter;
    // Instantiates a log file utility object, used to log status updates
    private LogFile               mLogFile;
    /*
 *  Intent filter for incoming broadcasts from the
 *  IntentService to refresh the list.
 */
    private IntentFilter          mBroadcastFilter;
    // Instance of a local broadcast manager
    private LocalBroadcastManager mBroadcastManager;
    private String                mFileName;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // tell activity that we need a options menu (delete and save logfile)
        setHasOptionsMenu(true);

        // check if a the name of a logfile has been passed as an argument
        if (!getArguments().containsKey(ARG_LOG_FILE_NAME)) {
            throw new IllegalArgumentException("No Logfile specified. Name of the Logfile must be" +
                                               " passed as an argument.");
        }

        mFileName = getArguments().getString(ARG_LOG_FILE_NAME);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.log_menu, menu);

        MenuItem mDeleteMenuItem = menu.findItem(R.id.action_delete_log);
        MenuItem mSaveMenuItem = menu.findItem(R.id.action_save_log);

        // if no log lines are stored, hide option menus
        if (mStatusAdapter != null && mStatusAdapter.isEmpty()) {

            mDeleteMenuItem.setVisible(false);
            mDeleteMenuItem.setEnabled(false);

            mSaveMenuItem.setVisible(false);
            mDeleteMenuItem.setEnabled(false);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_delete_log:
                showDeleteDialog();
                break;
            case R.id.action_save_log:
                saveLogFile();
                break;


        }

        return true;

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void saveLogFile() {

        // ensure that external storage is available and writable
        if (!Utils.isExternalStorageWritable()) {

            String msg = getString(R.string.save_file_error, mFileName) + " " + getString(R.string
                    .err_no_sd_card);

            Toast.makeText(getActivity(), msg
                    , Toast.LENGTH_LONG).show();
            return;
        }

        // Already existing file (current file that is opened)
        File logFile = new File(getActivity().getFilesDir(), mFileName);

        // if running on KitKat, store Logs into Documents folder instead of Downloads
        File parentDir;
        if (Utils.hasKitKat()) {
            parentDir = Environment.getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOCUMENTS);
        } else {
            parentDir = Environment.getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOWNLOADS);
        }

        // subfolder to store logs
        File path = new File(parentDir, "Awarenessy_Logs");

        path.mkdirs();

        // copied file on the external storage
        File savedFile = new File(path.getAbsolutePath(), mFileName);

        // try to copy file from internal storage (data/data/at.ac.uibk.awarenessy.app) to
        // external storage (Download/Awarenessy_Logs)
        try {
            copy(logFile, savedFile);

            String msg = getString(R.string.log_saved, mFileName, path.getAbsoluteFile());
            Log.d(TAG, msg);
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

            // send intent to start media scanner that makes the file shown in Explorers
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(savedFile));
            getActivity().sendBroadcast(intent);

        } catch (IOException e) {

            String msg = getString(R.string.save_file_error, mFileName);

            Log.e(TAG, msg);
            Log.e(TAG, e.getLocalizedMessage());
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }


    }

    public void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }


    /**
     * Show a dialog where the user can delete the opened logfile or cancel the process.
     */
    private void showDeleteDialog() {
        // Build a new Dialog to let the user confirm the deletion of the log file.
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(android.R.drawable.ic_dialog_alert);

        builder.setTitle(getString(R.string.delete_log_file_title));
        builder.setMessage(getString(R.string.delete_log_file_text));

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                boolean removed = LogFile.getInstance(getActivity()).removeLogFile(mFileName);
                mStatusAdapter.clear();
                mStatusAdapter.notifyDataSetChanged();
                String msg;

                // notify the user
                if (removed) {
                    msg = getString(R.string.log_file_deleted, mFileName);
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, msg);
                } else {
                    msg = getString(R.string.log_file_deletion_error, mFileName);
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, msg);
                }
            }
        });


        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onStart() {

        super.onStart();


        // Instantiate an adapter to store update data from the log
        mStatusAdapter = new ArrayAdapter<Spanned>(getActivity(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1) {

            // Override isEnabled to disable clicking animation on list elements
            @Override
            public boolean isEnabled(int position) {

                return false;
            }
        };


        getListView().setAdapter(mStatusAdapter);
        setEmptyText(getActivity().getString(R.string.empty_log));


        // Set the broadcast receiver to listen on broadcasts from {@link LogFile} when a new
        // line has been inserted
        mBroadcastManager = LocalBroadcastManager.getInstance(getActivity());

        // Create a new Intent filter for the broadcast receiver
        mBroadcastFilter = new IntentFilter(LogFile.ACTION_NEW_LOG);


        // Create a new LogFile object
        mLogFile = LogFile.getInstance(getActivity());


    }

    /**
     * Register the broadcast receiver and update the log of fragment updates
     */
    @Override
    public void onResume() {

        super.onResume();

        // Register the broadcast receiver
        mBroadcastManager.registerReceiver(updateListReceiver, mBroadcastFilter);

        // Load updated activity history
        updateActivityRecognitionHistory();


    }

    /**
     * Unregister the receiver during a pause
     */
    @Override
    public void onPause() {

        // Stop listening to broadcasts when the Fragment isn't visible.
        mBroadcastManager.unregisterReceiver(updateListReceiver);

        super.onPause();
    }

    /**
     * Display the activity detection history stored in the log file
     */
    private void updateActivityRecognitionHistory() {
        // Try to load data from the history file
        // Load log file records into the List
        List<Spanned> activityDetectionHistory = mLogFile.loadLogFile(mFileName);


        // Reverse log file to show newest message on top
        Collections.reverse(activityDetectionHistory);

        // Clear the adapter of existing data
        mStatusAdapter.clear();

        mStatusAdapter.addAll(activityDetectionHistory);

        // Trigger the adapter to update the display
        mStatusAdapter.notifyDataSetChanged();

        setListShown(true);

    }
}
