/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps.model;

import android.content.Context;
import android.content.pm.PermissionInfo;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

import at.ac.uibk.awarenessy.app.R;

/**
 * Class that represents only the necessary data of a {@link android.content.pm.PermissionInfo}.
 */
public class MyPermissionInfo implements Parcelable {

    private final String mName;
    private       String mDescription;
    private final int    mProtectionLevel;
    private       String mProtectionLevelName;
    private       int    mDangerPoints;

    // Resource String Identifier for the Info (NOT the String itself)
    private int mProtectionLevelInfo;

    // Resource Identifier for the Indicator Drawable (NOT the Drawable itself)
    private int mIndicatorRes;


    /**
     * Perform comparison between danger points of application entry objects.
     */
    public static final Comparator<MyPermissionInfo> DANGER_COMPARATOR_ASC = new
            Comparator<MyPermissionInfo>() {


                @Override
                public int compare( MyPermissionInfo myPermissionInfo,
                                    MyPermissionInfo myPermissionInfo2
                ) {
                    return myPermissionInfo.getDangerPoints() - myPermissionInfo2.getDangerPoints();
                }
            };

    /**
     * Perform comparison between danger points of application entry objects.
     */
    public static final Comparator<MyPermissionInfo> DANGER_COMPARATOR_DESC = new
            Comparator<MyPermissionInfo>() {


                @Override
                public int compare( MyPermissionInfo myPermissionInfo,
                                    MyPermissionInfo myPermissionInfo2
                ) {
                    return myPermissionInfo2.getDangerPoints() - myPermissionInfo.getDangerPoints();
                }
            };

    public MyPermissionInfo( Context context, PermissionInfo permInfo ) {

        CharSequence name = permInfo.loadLabel(context.getPackageManager());
        mName = name.toString();

        mDescription = "";

        // get description from the getPackageManager manager (already in system language)
        CharSequence descr = permInfo.loadDescription(context.getPackageManager());

        if ( descr != null ) {
            mDescription = descr.toString();
        }


        mProtectionLevel = permInfo.protectionLevel;
        mProtectionLevelName = "";

        // set additional information to get them later easier
        switch (mProtectionLevel) {
            case PermissionInfo.PROTECTION_DANGEROUS:

                mProtectionLevelName = context.getString(R.string.perm_danger);
                mProtectionLevelInfo = R.string.permission_info_dangerous;
                mIndicatorRes = R.drawable.ic_danger_iii;
                mDangerPoints = 5;
                break;

            case PermissionInfo.PROTECTION_NORMAL:

                mProtectionLevelName = context.getString(R.string.perm_normal);
                mProtectionLevelInfo = R.string.permission_info_normal;
                mIndicatorRes = R.drawable.ic_danger_i;
                mDangerPoints = 1;
                break;

            case PermissionInfo.PROTECTION_SIGNATURE:

                mProtectionLevelName = context.getString(R.string.perm_signature);
                mProtectionLevelInfo = R.string.permission_info_signature;
                mIndicatorRes = R.drawable.ic_danger_ii;
                mDangerPoints = 2;
                break;

            case PermissionInfo.PROTECTION_SIGNATURE_OR_SYSTEM:

                mProtectionLevelName = context.getString(R.string.perm_sign_or_system);
                mProtectionLevelInfo = R.string.permission_info_sig_or_system;
                mIndicatorRes = R.drawable.ic_danger_ii;
                mDangerPoints = 3;
                break;

            default:

                mIndicatorRes = 0;
                break;

        }

        // check if additional flags have been set
        boolean systemFlag = (mProtectionLevel & PermissionInfo.PROTECTION_FLAG_SYSTEM) != 0;
        boolean devFlag = (mProtectionLevel & PermissionInfo.PROTECTION_FLAG_DEVELOPMENT) != 0;

        if (systemFlag && devFlag) {
            mProtectionLevelName += context.getString(R.string.perm_flag_systemAndDev);
            mProtectionLevelInfo = R.string.permission_info_system_and_dev;
            if (mIndicatorRes == 0) {
                mIndicatorRes = R.drawable.ic_danger_ii;
            }
            mDangerPoints = 3;
        } else if (systemFlag) {
            mProtectionLevelName += context.getString(R.string.perm_flag_system);
            mProtectionLevelInfo = R.string.permission_info_system;
            if (mIndicatorRes == 0) {
                mIndicatorRes = R.drawable.ic_danger_ii;
            }
            mDangerPoints = 3;
        } else if (devFlag) {
            mProtectionLevelName += context.getString(R.string.perm_flag_dev);
            mProtectionLevelInfo = R.string.permission_info_dev;
        }


    }


    public int getProtectionLevel() {
        return mProtectionLevel;
    }

    public String getProtectionLevelName() {
        return mProtectionLevelName;
    }

    public String getName() {
        return mName;
    }


    public String getDescription() {
        return mDescription;
    }

    /**
     * Returns the Resource ID of the Indicator Drawable.
     * This is a defined Resource in {@link at.ac.uibk.awarenessy.app.R}.
     *
     * @return Resource ID for the Permission Indicator
     */
    public int getIndicatorRes() {
        return mIndicatorRes;
    }


    public int getDangerPoints() {

        return mDangerPoints;

    }

    /**
     * Returns the resource ID of the protection level name.
     * This is a defined Resource in {@link at.ac.uibk.awarenessy.app.R}.
     *
     * @return Resource ID for the protection level name.
     */
    public int getProtectionLevelInfo() {
        return mProtectionLevelInfo;
    }

    @Override
    public String toString() {
        return mName;
    }


    /*
     *   Parcelable Implementation
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int parcelableFlags) {

        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeInt(mProtectionLevel);
        dest.writeString(mProtectionLevelName);
        dest.writeInt(mDangerPoints);
        dest.writeInt(mProtectionLevelInfo);
        dest.writeInt(mIndicatorRes);


    }


    public static final Creator<MyPermissionInfo> CREATOR = new Creator<MyPermissionInfo>() {

        @Override
        public MyPermissionInfo createFromParcel(Parcel parcel) {
            return new MyPermissionInfo(parcel);
        }

        @Override
        public MyPermissionInfo[] newArray(int size) {
            return new MyPermissionInfo[size];
        }
    };

    private MyPermissionInfo(Parcel source) {

        mName = source.readString();
        mDescription = source.readString();
        mProtectionLevel = source.readInt();
        mProtectionLevelName = source.readString();
        mDangerPoints = source.readInt();
        mProtectionLevelInfo = source.readInt();
        mIndicatorRes = source.readInt();


    }


}

