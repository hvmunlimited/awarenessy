/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spanned;
import android.text.SpannedString;
import at.ac.uibk.awarenessy.app.utils.Log;

import at.ac.uibk.awarenessy.app.R;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Utility class that handles writing, reading and cleaning up files where we log the activities
 * that where detected by
 * the activity detection service.
 */
public class LogFile {


    /*
    * DIFFERENT LOGFILES
     */
    // logfile to store service changes and update intervals
    public static final  String   LOG_FILE_BACKGROUND_LOCATION_SERVICE         =
            "Background_Location_Service_Log.txt";
    // Name of the logfile where the service stores new recognized activities
    public static final  String   LOG_FILE_ACTIVITY_RECOGNITION_INTENT_SERVICE =
            "Activity_Recognition_Intent_Service_Log.txt";
    private static final String[] LOG_FILES_NO_PHONE                           = {LOG_FILE_ACTIVITY_RECOGNITION_INTENT_SERVICE,
            LOG_FILE_BACKGROUND_LOCATION_SERVICE};
    // name of the logfile where incoming and outcoming calls will be logged
    public static final  String   LOG_FILE_CALLS                               = "Calls_Log.txt";
    // name of the logfile where incoming sms will be logged
    public static final  String   LOG_FILE_SMS                                 = "Sms_Log.txt";
    private static final String[] LOG_FILES_ALL                                = {LOG_FILE_ACTIVITY_RECOGNITION_INTENT_SERVICE,
            LOG_FILE_BACKGROUND_LOCATION_SERVICE, LOG_FILE_CALLS, LOG_FILE_SMS};
    // Delimits the timestamp from the log info
    private static final String   LOG_DELIMITER                                = "||";
    private static final String   TAG                                          = LogFile.class.getSimpleName();
    public static final  String   ACTION_NEW_LOG                               = TAG + ".NEW_LOG";
    // Store an sLogFileInstance of the log file
    private static       LogFile  sLogFileInstance                             = null;
    // Store a context for handling files
    private final Context mContext;


    /**
     * Singleton that ensures that only one sLogFileInstance of the LogFile exists at any time
     *
     * @param context A Context for the current app
     */
    private LogFile(Context context) {

        // Get the context from the caller
        mContext = context;


    }

    /**
     * Create an sLogFileInstance of log file, or return the current sLogFileInstance
     *
     * @param context A Context for the current app
     * @return An sLogFileInstance of this class
     */
    public static LogFile getInstance(Context context) {

        if (sLogFileInstance == null) {
            sLogFileInstance = new LogFile(context);
        }
        return sLogFileInstance;
    }

    /**
     * Returns a Array of FileNames that should be used on the device.
     * <p/>
     * If the device is a tablet, which has no phone function (Messaging, Calling, ...) then some log files should not be used, so the function returns only possible files.
     *
     * @param context to determine if device is tablet or phone
     * @return an Array of Logfiles
     */
    public static String[] getLogNames(Context context) {

        if (!Utils.isTablet(context)) {
            return LOG_FILES_ALL;
        } else {
            return LOG_FILES_NO_PHONE;
        }

    }

    /**
     * Log a message with the current time to the log file
     *
     * @param fileName the name of the logfile
     * @param message  the message to log
     */
    public void log(String fileName, String message) {
        log(fileName, message, new Date());
    }

    /**
     * Log a message with the specified timeStamp to the log file
     *
     * @param fileName  the name of the logfile
     * @param message   the message to log
     * @param timeStamp the timestamp to use in the log
     */
    public void log(String fileName, String message, Date timeStamp) {
        String eol = System.getProperty("line.separator");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(mContext.openFileOutput(fileName,
                    Context.MODE_APPEND)));
            String timeStampString = Utils.DATE_FORMAT.format(timeStamp);

            message = mContext.getString(R.string.log_template, timeStampString, LOG_DELIMITER,
                    message);

            writer.write(message + eol);
            writer.flush();
            Log.d(TAG, "Logged: " + message);

            // notify all broadcast receiver that a new log has been stored
            Intent newLogIntent = new Intent(ACTION_NEW_LOG);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(newLogIntent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Loads data from the log file.
     */
    public List<Spanned> loadLogFile(String fileName) {

        // Get a new List of spanned strings
        List<Spanned> content = new ArrayList<Spanned>();

        BufferedReader input;

        // If no log file exists yet, return the empty List
        try {
            input = new BufferedReader(new InputStreamReader(mContext.openFileInput(fileName)));
        } catch (FileNotFoundException e) {
            return content;
        }

        try {
            String line;
            while ((line = input.readLine()) != null) {
                content.add(new SpannedString(line));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        // Return the data from the log file
        return content;
    }


    /**
     * Delete the specified log file.
     *
     * @param fileName Name of the log file
     * @return True if the file has been deleted, false if an error occur
     */
    public boolean removeLogFile(String fileName) {

        return mContext.deleteFile(fileName);

    }


}
