/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;

import at.ac.uibk.awarenessy.app.utils.Utils;


/**
 * @see <a href="http://pulse7.net/android/read-sms-message-inbox-sent-draft-android/">Read sms
 * message inbox sent draft android</a>
 */
public class SmsInterface {

    // All available column names in SMS table
    // [_id, thread_id, address, 
    // person, date, protocol, read,
    // status, type, reply_path_present,
    // subject, body, service_center,
    // locked, error_code, seen]
    public static final String ADDRESS = "address";
    public static final String DATE    = "date";
    public static final String BODY    = "body";


    /*
         * Since KitKat there was no official API for the sms content provider,
         * so we have to use the
         * 'unofficial' Uri.
         */
    @SuppressLint("NewApi")
    public static final Uri CONTENT_SMS_INBOX = Utils.hasKitKat() ? Telephony.Sms.Inbox
            .CONTENT_URI : Uri.parse("content://sms/inbox");
    @SuppressLint("NewApi")
    public static final Uri CONTENT_SMS_SENT  = Utils.hasKitKat() ? Telephony.Sms.Sent
            .CONTENT_URI : Uri.parse("content://sms/sent");


    public static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    /**
     * Returns the name of the specified number using the optimized lookup table PhoneLookup.
     * If no name matches to the given number, null will be returned. If the number is linked
     * with more
     * than one contact, then it's not determined which name is returned.
     *
     * @param number Telephone number to look up the name
     *
     * @return The name to the specified number, or the number if no name exists for this number.
     */
    public static String getName(String number, Context context) {


        if (context == null) {
            throw new IllegalArgumentException("ContentResolver must not be null");
        }

        if (number == null) {
            throw new IllegalArgumentException("Number must not be null");
        }

        String name = number;
        String[] projection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup
                .DISPLAY_NAME, ContactsContract.PhoneLookup.NUMBER};

        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));

        Cursor c = context.getContentResolver().query(lookupUri, projection, null, null, null);

        while (c.moveToNext()) {
            name = c.getString(c.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        c.close();

        return name;
    }

    /**
     * Returns the Contact id of the specified number using  the optimized lookup table
     * PhoneLookup or -1 if no contact has been found. If the number is linked with more
     * than one contact, then it's not determined which id is returned.
     *
     * @param number  Number to get the contact id from.
     * @param context Context to get ContentResolver
     *
     * @return the id of the contact linked with the specifed number or -1 if no contact found
     */
    public static int getContactId(String number, Context context) {


        if (context == null) {
            throw new IllegalArgumentException("ContentResolver must not be null");
        }

        int id = -1;
        String[] projection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup
                .NUMBER};

        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));

        Cursor c = context.getContentResolver().query(lookupUri, projection, null, null, null);

        while (c.moveToNext()) {
            id = c.getInt(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
        }

        c.close();

        return id;
    }

    public enum SmsType {
        SENT, RECEIVED, BOTH

    }

    public static class SmsCount {
        public int sent;
        public int received;

        public SmsCount(int sent, int received) {
            this.sent = sent;
            this.received = received;
        }

        @Override
        public String toString() {
            return "SmsCount{" +
                   "sent=" + sent +
                   ", received=" + received +
                   '}';
        }

        public int getOverall() {
            return sent + received;
        }

    }


}
