/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import at.ac.uibk.awarenessy.app.utils.Log;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.sms.SmsStatistic;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.SimpleLocation;

/**
 * Activity that gives a overview of information the phone provider has about the user and the
 * device.
 * <p/>
 * Because it is not possible to get the information directly from the provider, the app tries to
 * get as much information as possible:
 * <p/>
 * <ul><li>The <b>name</b> of the user - can be read out from the contacts (has to be done at first
 * setup of the Google Play Store)</li>
 * <li>The <b>phone number</b> of the SIM card- can be read out from {@link
 * android.telephony.TelephonyManager}</li>
 * <li>The <b>IMEI number</b> of the device - can be read out from {@link
 * android.telephony.TelephonyManager}</li>
 * <li>The <b>IP address</b> of the current/last mobile connection (only from mobile connection,
 * cause the provider didn't know the wifi's ip) - done by a http request to a website that returns
 * the used ip address (<a href="http://checkip.amazonaws.com/">URL</a></li>
 * <li>The current <b>location</b> - can be simulated with gps (the phone provider uses cell tower
 * triangulation)</li>
 * <li>The user's most <b>contacted persons</b> - can be read from the apps SmsStatistic and Call
 * Database<br>
 * Shows the person with the most messages and the person with the most calls</li>
 * <li>The <br>complete sms statistic/history</br> - link to {@link at.ac.uibk.awarenessy
 * .app.sms.SmsStatisticFragment}</li>
 * </ul>
 */
public class ProviderFragment extends ModuleFragment implements GooglePlayServicesClient
        .ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    public static final  String FRAGMENT_TAG = ProviderFragment.class.getName();
    private static final String TAG          = ProviderFragment.class.getSimpleName();
    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;

    //    UI Reference
    private View rootView;

    private TextView mIpView;
    private TextView mLocationView;
    private TextView mAddressView;


    private ProgressBar mIpProgress;
    private ProgressBar mLocationProgress;
    private ProgressBar mAddressProgress;


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        rootView = inflater.inflate(R.layout.provider_fragment, container, false);

        // textviews that will be calculated and display some information about the user
        TextView nameView = ( TextView ) rootView.findViewById(R.id.name);
        TextView phoneNumberView = ( TextView ) rootView.findViewById(R.id.phone_number);
        TextView IMEIView = ( TextView ) rootView.findViewById(R.id.imei);
        mIpView = ( TextView ) rootView.findViewById(R.id.curr_ip_address);
        mLocationView = ( TextView ) rootView.findViewById(R.id.curr_location);
        mAddressView = ( TextView ) rootView.findViewById(R.id.curr_address);

        SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                .PREF_FLAGS
                                                                                .PREF_NAME,
                                                                        Context.MODE_PRIVATE
        );
        boolean showInfo = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_PROVIDER_INFO, true);

        if ( showInfo ) { showInfoDialog(); }


        mIpProgress = ( ProgressBar ) rootView.findViewById(R.id.progress_ip);
        mLocationProgress = ( ProgressBar ) rootView.findViewById(R.id.progress_location);
        mAddressProgress = ( ProgressBar ) rootView.findViewById(R.id.progress_address);

        mLocationClient = new LocationClient(getActivity(), this, this);


        // get TelephonyManager to get IMEI and telephone number
        TelephonyManager telephonyManager = ( TelephonyManager ) getActivity().getSystemService
                (Context.TELEPHONY_SERVICE);

        // get ConnectivityManager to check if user is connected to wifi
        ConnectivityManager connManager = ( ConnectivityManager ) getActivity().getSystemService
                (Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        // get and display user name
        String userName = getUserName();
        nameView.setText(userName);

        // get and display number
        String number = telephonyManager.getLine1Number();
        if ( number == null || number.isEmpty() ) {
            number = getString(R.string.no_phone_number_found);
        }
        phoneNumberView.setText(number);

        // get and display IMEI
        String deviceId = telephonyManager.getDeviceId();
        if ( deviceId == null || deviceId.isEmpty() ) {
            deviceId = getString(R.string.no_imei_found);
        }
        IMEIView.setText(deviceId);


        GetFavouriteContactTask contactTask = new GetFavouriteContactTask(getActivity());
        contactTask.execute();

        /*
         * only check ip address if device is connected to mobile data, cause if not,
         * provider didn't know your ip
          */
        if ( mobileInfo != null && mobileInfo.isConnected() ) {
            DetectIpAddressTask detectIpAddressTask = new DetectIpAddressTask(getActivity());
            // get ip address
            detectIpAddressTask.execute();
        } else {
            // if no wifi has found (which device has no wifi module?) show false to display
            // normal toast message
            showLastIpAddress(wifiInfo != null && wifiInfo.isConnected());
        }
        return rootView;

    }


    /**
     * Returns the user name that has been stored on the first set up of the device.
     * If no name has been configured, 'Not found' will be returned.
     *
     * @return the user name if available, 'Not found' else
     */
    private String getUserName() {

        ContentResolver cr = getActivity().getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);

        String name = getString(R.string.no_name_found);
        while ( cursor.moveToNext() ) {
            name = cursor.getString(cursor.getColumnIndex(ContactsContract.Profile
                                                                  .DISPLAY_NAME_PRIMARY));
            Log.d(TAG, "Name primary: " + name);
            Log.d(TAG, "Name: " + cursor.getString(cursor.getColumnIndex(ContactsContract
                                                                                 .Profile
                                                                                 .DISPLAY_NAME)));
            Log.d(TAG, "Name alt: " + cursor.getString(cursor.getColumnIndex(ContactsContract
                                                                                     .Profile
                                                                                     .DISPLAY_NAME_ALTERNATIVE)));
        }
        return name;
    }

    @Override
    /**
     * Show an info dialog, what the usage and purpose of this Fragment/Module is.
     * <p/>
     * Consists of a simple title, string and an ok button.
     */
    public void showInfoDialog() {

        // Build a new Dialog to tell the user to restart the phone
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.phone_provider);

        builder.setMessage(R.string.provider_dialog_info);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                // Restore preferences
                SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                        .PREF_FLAGS.PREF_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_PROVIDER_INFO, false);


                // Commit the edits!
                editor.commit();
            }
        });

        builder.show();
    }


    @Override
    public void onStart() {

        super.onStart();
        // Connect the client.
        mLocationClient.connect();

    }

    @Override
    public void onStop() {

        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();


        super.onStop();
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully.
     */
    @Override
    public void onConnected( Bundle bundle ) {

        Log.d(TAG, "Connected");

        Location lastLocation = mLocationClient.getLastLocation();

        // no location found, show errors
        if ( lastLocation == null ) {
            showLocationError();
            return;
        }

        SimpleLocation simpleLocation = new SimpleLocation(lastLocation);

        LatLng latLng = simpleLocation.getLatLng();

        String msg = LocationUtils.getLatLngString(getActivity(), latLng);

        mLocationView.setText(msg);

        mLocationView.setVisibility(View.VISIBLE);
        mLocationProgress.setVisibility(View.INVISIBLE);

        GetAddressTask getAddressTask = new GetAddressTask(getActivity());
        getAddressTask.execute(simpleLocation);
    }

    /**
     * Hide progress and show error instead.
     */
    private void showLocationError() {
        mLocationView.setText(getString(R.string.curr_location_error));
        mAddressView.setText(getString(R.string.curr_address_error));

        mLocationView.setVisibility(View.VISIBLE);
        mAddressView.setVisibility(View.VISIBLE);

        mLocationProgress.setVisibility(View.INVISIBLE);
        mAddressProgress.setVisibility(View.INVISIBLE);
    }


    /*
         * Called by Location Services if the connection to the
         * location client drops because of an error.
         */
    @Override
    public void onDisconnected() {

        Log.d(TAG, "Disconnected");

    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed( ConnectionResult connectionResult ) {

        Log.d(TAG, "Connection Failed");
        showLocationError();

          /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if ( connectionResult.hasResolution() ) {
            try {

                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        getActivity(),
                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */

            } catch ( IntentSender.SendIntentException e ) {

//                Log the error
                e.printStackTrace();
            }
        } else {

            // If no resolution is available, display a dialog to the user with the error.
            GooglePlayServicesUtil.getErrorDialog(
                    connectionResult.getErrorCode(),
                    getActivity(),
                    LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST).show();
        }

    }


    /*
   * Handle results returned to this Activity by other Activities started with
   * startActivityForResult(). In particular, the method onConnectionFailed() in
   * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
   * start an Activity that handles Google Play services problems. The result of this
   * call returns here, to onActivityResult.
   */
    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent intent ) {

        // Choose what to do based on the request code
        switch ( requestCode ) {

            // If the request code matches the code sent in onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST:

                switch ( resultCode ) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:

                        // Log the result
                        Log.d(TAG, "Resolved");

                        break;

                    // If any other result was returned by Google Play services
                    default:
                        // Log the result
                        Log.d(TAG, "Google Play services: unable to resolve connection error.");


                        break;
                }

                // If any other request code was received
            default:
                // Report that this Activity received an unknown requestCode
                Log.d(TAG,
                      "Received an unknown activity request code %1$d in onActivityResult.");

                break;
        }
    }


    /**
     * Show the last known ip address in the provided view.
     * <p/>
     * Check the {@link android.content.SharedPreferences} if an old ip address has been stored and
     * then display it. If the specified boolean is {@code true}, which means the device is
     * connected to a
     * wifi network, a toast explaining this will be shown. If {@code false}, it's likely the
     * device
     * has no internet connection at all, so a toast will explaining this too. In the worst case,
     * where no ip address has been stored and no mobile connection is available a error message is
     * shown.
     *
     * @param isWifi true if the wifi toast should be shown, else false
     */
    private void showLastIpAddress( boolean isWifi ) {

        SharedPreferences preferences = getActivity().getSharedPreferences(Preferences
                                                                                   .APP_DATA
                                                                                   .PREF_NAME,
                                                                           Context.MODE_PRIVATE);
        // have we a last known ip?
        if ( preferences.contains(Preferences.APP_DATA.KEY_LAST_KNOWN_IP_ADDRESS) ) {

            // show last known ip address and tell user that it's not the current
            String lastIpAddress = preferences.getString(Preferences.APP_DATA
                                                                 .KEY_LAST_KNOWN_IP_ADDRESS,
                                                         getString(R.string.no_ip_address_found)
            );


            if ( isWifi ) {
                // show toast that the user knows, that it's not the current ip address
                Toast.makeText(getActivity(), R.string.last_ip_address_found_wifi,
                               Toast.LENGTH_LONG).show();
            } else {
                // show toast that the user knows, that it's not the current ip address
                Toast.makeText(getActivity(), R.string.last_ip_address_found_no_internet,
                               Toast.LENGTH_LONG).show();
            }

            // change 'Your current IP address' to 'Your last known IP address'
            TextView descIp = ( TextView ) rootView.findViewById(R.id.desc_curr_ip_address);
            descIp.setText(getString(R.string.last_ip_address));

            mIpView.setText(lastIpAddress);
        } else {
            // we have no current and no last ip address, just show error
            mIpView.setText(R.string.no_ip_address_found);
        }

        mIpView.setVisibility(View.VISIBLE);
        mIpProgress.setVisibility(View.INVISIBLE);

    }

    /**
     * A subclass of AsyncTask that creates a SmsStatistic Object and requests two top contacts.
     * One with the most calls (incoming + outgoing) and the other with the most sms (sent +
     * received).
     * <p/>
     * doInBackground returns a list with two {@link android.util.Pair}s. One pair contains the
     * contact with the most calls and the other contains the most sms. Each pair stores the
     * name of the contact ({@link android.util.Pair#first}) and the number of messages/calls
     * ({@link
     * android.util.Pair#second}). After completing the little progressbar
     * will be invisible and the text will be visible.
     */
    private class GetFavouriteContactTask extends AsyncTask<Void, Void, List<Pair<String,
            Integer>>> {

        private final Context mContext;

        private GetFavouriteContactTask( Context context ) {
            super();
            mContext = context;
        }


        @Override
        protected List<Pair<String, Integer>> doInBackground( Void... params ) {

            Log.d(TAG, "GetFavouriteContactTask started");


            long start = System.nanoTime();
            long stop;

            SmsStatistic statistic = new SmsStatistic(mContext);

            Pair<String, Integer> mostCallContact = statistic.getMostPhonedContact();

            Pair<String, Integer> mostSmsContact = statistic.getMostSmsContact();

            stop = System.nanoTime();

            Log.d(TAG, "GetFavouriteContactTask finished in " + Utils.getElapsedTime(start, stop));

            List<Pair<String, Integer>> list = new ArrayList<Pair<String, Integer>>(2);
            list.add(mostSmsContact);
            list.add(mostCallContact);

            return list;
        }

        @Override
        protected void onPostExecute( List<Pair<String, Integer>> mostContacts ) {

            // get both contacts
            Pair<String, Integer> mostSmsContact = mostContacts.get(0);
            Pair<String, Integer> mostCallContact = mostContacts.get(1);

            // get views
            TextView smsContact = ( TextView ) rootView.findViewById(R.id.sms_contact);
            TextView callContact = ( TextView ) rootView.findViewById(R.id.call_contact);


            // set sms text
            if ( mostSmsContact != null ) {

                String name = SmsInterface.getName(mostSmsContact.first, mContext);

                int numSms = mostSmsContact.second;

                smsContact.setText(numSms + " x " + name);

            } else {

                smsContact.setText(getString(R.string.no_messages_found));
            }


            // set call text
            if ( mostCallContact != null ) {

                String name = SmsInterface.getName(mostCallContact.first, mContext);


                int numCalls = mostCallContact.second;
                callContact.setText(numCalls + " x " + name);


            } else {

                callContact.setText(getString(R.string.no_calls_found));

            }


            smsContact.setVisibility(View.VISIBLE);
            callContact.setVisibility(View.VISIBLE);

            // hide progress bar and show result
            rootView.findViewById(R.id.progress_fav_contact).setVisibility(View.INVISIBLE);

        }
    }


    /**
     * A subclass of AsyncTask that calls getFromLocation() in the background.
     * If an address is found (doInBackground returns not null), this address will be stored into
     * passed SimpleLocation.
     * <p/>
     * The class definition has these generic types:
     * <ul>
     * <li>SimpleLocation - A SimpleLocation object containing the current location.</li>
     * <li>Void - indicates that progress units are not used</li>
     * <li>String - An address passed to onPostExecute()</li>
     * </ul>
     */
    private class GetAddressTask extends
            AsyncTask<SimpleLocation, Void, String> {

        final Context mContext;

        public GetAddressTask( Context context ) {

            super();
            mContext = context;
        }


        @Override
        protected String doInBackground( SimpleLocation... params ) {


            Log.d(TAG, "GetAddressTask started");

            long start = System.nanoTime();
            long stop;


            String address = LocationUtils.calcLocationAddress(mContext, params[0]);


            stop = System.nanoTime();
            Log.d(TAG, "GetAddressTask finished in " + Utils.getElapsedTime(start, stop));

            return address;

        }

        /**
         * A method that's called once doInBackground() completes. Show address in UI.
         */
        @Override
        protected void onPostExecute( String address ) {

            //TODO: save/check last known location

            TextView addressView = ( TextView ) rootView.findViewById(R.id.curr_address);


            if ( address == null ) {
                addressView.setText(mContext.getString(R.string.curr_address_error));
            } else {
                // Display the results of the lookup.
                addressView.setText(address);
            }

            mAddressProgress.setVisibility(View.INVISIBLE);
            addressView.setVisibility(View.VISIBLE);

        }

    }

    private class DetectIpAddressTask extends AsyncTask<Void, Void, String> {

        private static final String IPADDRESS_PATTERN  =
                "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        private static final String URL_IP_ADDRESS_API = "http://checkip.amazonaws.com/";

        private final Context mContext;

        public DetectIpAddressTask( Context context ) {

            super();
            mContext = context;
        }

        /**
         * Make a HttpGet to a website that return the IP address of the requesting device (or
         * router, NAT, ...), parse it and return it as a String. If an error occur, return null.
         * <p/>
         * Possible errors are:
         * <ul>
         * <li>No internet connection</li>
         * <li>{@link
         * at.ac.uibk.awarenessy.app .phoneprovider.ProviderFragment
         * .DetectIpAddressTask#URL_IP_ADDRESS_API}
         * is obsolete</li> <li>Other Error or Exception occure</li>
         * </ul>
         *
         * @param params void
         *
         * @return the ip address or null if an error occured
         */
        @Override
        protected String doInBackground( Void... params ) {


            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(URL_IP_ADDRESS_API);

                HttpResponse response;

                response = httpclient.execute(httpget);


                HttpEntity entity = response.getEntity();
                if ( entity != null ) {
                    String content = EntityUtils.toString(entity).trim();

                    Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
                    Matcher matcher = pattern.matcher(content);

                    return matcher.matches() ? content : null;



                } else {
                    Log.w(TAG, response.getStatusLine().toString());
                    return null;
                }

            } catch ( Exception e ) {
                Log.w(TAG, e.getLocalizedMessage());
                return null;
            }


        }

        /**
         * doInBackground finished. Either we have an ip address or we got null. Obtaining null can
         * have multiple reasons:
         * <p/>
         * <ul>
         * <li>No internet connection</li>
         * <li>{@link at.ac.uibk.awarenessy.app
         * .phoneprovider.ProviderFragment.DetectIpAddressTask#URL_IP_ADDRESS_API} is obsolete</li>
         * <li>Other Error or Exception occure</li> </ul>
         * <p/>
         * If ipAddress is valid, the UI is updated and the address is stored in SharedPreferences.
         * If not, check if a previous address has been stored and display this one or if no ip
         * address is available at all, update the UI with a 'Not found' hint.
         *
         * @param ipAddress the IP address that has been found, or null if an error occurred
         */
        @Override
        protected void onPostExecute( String ipAddress ) {

            SharedPreferences preferences = mContext.getSharedPreferences(Preferences
                                                                                  .APP_DATA
                                                                                  .PREF_NAME,
                                                                          Context.MODE_PRIVATE);
            /*
            detecting ip address wasn't successful. Either no internet connection,
            or URL_IP_ADDRESS_API is obsolete, or some unknown exception happens.
             */
            if ( ipAddress == null ) {
                showLastIpAddress(false);

                // have a current ip address, store and show it
            } else {
                preferences.edit().putString(Preferences.APP_DATA.KEY_LAST_KNOWN_IP_ADDRESS,
                                             ipAddress).commit();

                mIpView.setText(ipAddress);
            }

            mIpProgress.setVisibility(View.INVISIBLE);
            mIpView.setVisibility(View.VISIBLE);
        }


    }

}
