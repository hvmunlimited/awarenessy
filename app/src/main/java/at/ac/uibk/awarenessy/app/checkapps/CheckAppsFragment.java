/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.utils.Preferences;


/**
 * Entry Fragment to the Check Apps Feature.
 * <p/>
 * This fragment has different presentations for handset and tablet-size devices.
 * <p/>
 * On tablets and  large screen devices it presents two fragments ({@link
 * AppListFragment} and {@link
 * AppDetailFragment})
 * side-by-side using two vertical panes (List Apps and Display App Details).
 * On handsets, the fragment presents only
 * the {@link AppListFragment}, which will start a separte
 * {@link AppDetailActivity}
 * if the user click on an App.
 */
public class CheckAppsFragment extends ModuleFragment {

    public static final String FRAGMENT_TAG = CheckAppsFragment.class.getName();


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CheckAppsFragment() {
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        View rootView = inflater.inflate(R.layout.apps_check_apps_container, container, false);


        View detailView = rootView.findViewById(R.id.app_detail_container);

        boolean mTwoPane = detailView != null && detailView.getVisibility() == View.VISIBLE;

        FragmentManager fm = getFragmentManager();

        AppListFragment appListFragment = new AppListFragment();
        fm.beginTransaction().replace(R.id.app_list_container, appListFragment,
                                      AppListFragment.FRAGMENT_TAG).commit();


        // if second pane is available, start second fragment too.
        if ( mTwoPane ) {

            AppDetailFragment appDetailFragment = new AppDetailFragment();
            fm.beginTransaction().replace(R.id.app_detail_container, appDetailFragment,
                                          AppDetailFragment.FRAGMENT_TAG).commit();

        }

        // Restore preferences
        SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                .PREF_FLAGS
                                                                                .PREF_NAME,
                                                                        Context.MODE_PRIVATE);

        boolean firstStart = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_CHECK_APPS_INFO, true);

        if ( firstStart ) {
            showInfoDialog();
        }

        return rootView;

    }

    @Override
    /**
     * Shows a {@link android.app.AlertDialog} that displays information about the Check Apps
     * functionality.
     */
    public void showInfoDialog() {

        Activity activity = getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);


        // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
        // possible Buttons are uneffected.
        LayoutInflater inflater = activity.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.apps_info_dialog, null);


        // Set custom view
        builder.setView(view);

        builder.setIcon(android.R.drawable.ic_menu_info_details);
        builder.setTitle(R.string.check_dangerous_apps);


        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {

                Activity activity1 = getActivity();
                System.out.println(activity1 != null ? activity1.toString() : "null");

                // Restore preferences
                SharedPreferences settings = activity1.getSharedPreferences(Preferences
                                                                                    .PREF_FLAGS
                                                                                    .PREF_NAME,
                                                                            Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_CHECK_APPS_INFO, false);

                // Commit the edits!
                editor.commit();
            }
        });


        builder.show();

    }

}
