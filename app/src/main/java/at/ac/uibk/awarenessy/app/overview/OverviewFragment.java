/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.overview;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.widget.LoginButton;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.utils.FacebookUtils;
import at.ac.uibk.awarenessy.app.utils.Log;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.SimpleLocation;
import at.ac.uibk.awarenessy.dao.SimpleLocationDao;


/**
 * Fragment that gives a Overview of the Privacy.
 * <p/>
 * Shows the most common places, the covered distance and the value of Facebook.
 * <p/>
 * To obtain the common places and the value of Facebook, two AsyncTasks are used.
 * <p/>
 * {@link OverviewFragment.PlacesTask} is used to get the
 * four places (two day, two night) in the last two weeks.
 * {@link OverviewFragment.FacebookTask} is used to get
 * the
 * number of Postings and Likes of the last two weeks.
 */
public class OverviewFragment extends ModuleFragment {

    public static final String FRAGMENT_TAG = OverviewFragment.class.getName();

    //Milliseconds of the date two weeks ago
    public static final long TWO_WEEKS = System.currentTimeMillis() - TimeUnit
            .DAYS.toMillis(14);

    private static final String TAG = OverviewFragment.class
            .getSimpleName();

    // Begin and end of the day (used to get places for day and night)
    private static final int BEGIN_DAY = 8;
    private static final int END_DAY   = 20;

    // "size" of the cluster where common places will be calculated
    private static final float DIST_KM = 0.25f;

    // northern and southern boundary of the cluster
    private static final double LAT_DIFF = DIST_KM / 111f;
    private double mLongDiff; // eastern and western boundary has to be calculated (depends on
    // curr. latitude)


    private View mContentView;

    private float mDistance;

    // big progress that will be displayed as long as both task are working
    private View mProgressContainer;

    // progress bars
    private ProgressBar mProgressPlaces;
    private ProgressBar mProgressFb;


    private int         mNumPostings;
    private int         mNumLikes;
    // Facebook UI
    private TextView    mNumPostingsView;
    private TextView    mNumLikesView;
    private TextView    mDescFb;
    private LoginButton mLoginButton;
    private View        mFbContainer;
    private ImageView   mCoinsImg;


    // Common Places UI
    private TextView mAddressDay1View;
    private TextView mAddressDay2View;
    private TextView mAddressNight1View;
    private TextView mAddressNight2View;
    private TextView mNoInternetView;
    private View     mPlacesContainer;
    private View     mContainerDay;
    private View     mContainerNight;
    private View     mNoPlacesView;

    /**
     * Calculate the eastern and western boundary for the given latitude.
     * <p/>
     * To calculate longitudes in meters, the latitude at this position is needed.
     *
     * @param latitude the latitude to calculate the longitude
     *
     * @return the eastern and western difference
     */
    private double calcLongitudeDifference( double latitude ) {
        return DIST_KM / (Math.cos(Math.toRadians(latitude)) * 111);
    }


    /**
     * Returns the last known Location stored in the database.
     *
     * @return the last stored location
     */
    private double getLastLatitude() {

        SimpleLocationDao dao = Utils.getDaoSession(getActivity()).getSimpleLocationDao();

        // location with the highest time ( = newest location)
        List<SimpleLocation> list = dao.queryBuilder().orderDesc(SimpleLocationDao
                                                                         .Properties
                                                                         .Time)
                .limit(1).list();


        if ( list.isEmpty() ) {
            return 0;
        }

        return list.get(0).getLatitude();
    }

    /**
     * Start a new {@link OverviewFragment.FacebookTask}
     */
    public void startFBTask() {
        Log.d(TAG, "STart FBTask");
        FacebookTask facebookTask = new FacebookTask();
        facebookTask.execute(getPostingsV2(), getLikes());
    }

    @Override
    public void onStart() {
        super.onStart();

        // only start tasks if we have internet
        if ( Utils.isNetworkAvailable(getActivity()) ) {

            if ( FacebookUtils.isLoggedIn() ) {
                startFBTask();
            } else {
                updateUI(State.FB_LOGGED_OUT);
            }

            PlacesTask placesTask = new PlacesTask();
            placesTask.execute();

        } else {
            // no internet
            updateUI(State.NO_INTERNET);
        }

    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);


        SharedPreferences pref = getActivity().getSharedPreferences(Preferences.APP_DATA.PREF_NAME,
                                                                    Context.MODE_PRIVATE);

        mDistance = pref.getFloat(Preferences.APP_DATA.KEY_DISTANCE_TRAVELLED, 0);
        mLongDiff = calcLongitudeDifference(getLastLatitude());

    }

    private Request getLikes() {

        List<String> permissions = Session.getActiveSession().getPermissions();

        Log.d(TAG, "Permissions: " + permissions);
        new Request(
                Session.getActiveSession(),
                "/me/likes",
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {
            /* handle the result */
                        Log.d(TAG, "Got results new: " + response.toString());
                    }
                }
        ).executeAsync();


        // Callback that is called, when the request is executed
        final Request.Callback likeCallback = new Request.Callback() {
            @Override
            public void onCompleted( Response response ) {
                Log.d(TAG, "Got results likes: " + response.toString());
                GraphObject graphObject = response.getGraphObject();


                if ( graphObject != null ) {

                    Object data = graphObject.getProperty("data");

                    if ( data != null ) {


                        JSONArray data1 = ( JSONArray ) data;

                        Log.d(TAG, "Num of Likes: " + data1.length());


                        Date twoWeeks = new Date(TWO_WEEKS);

                        // iterate through the returned Likes as long as their given during the
                        // last 2 week
                        for ( int i = 0 ; i < data1.length() ; i++ ) {
                            try {
                                JSONObject like = data1.getJSONObject(i);
                                Log.d(TAG, i + " " + like.toString());

                                String created_time = like.getString("created_time");

                                Date date = FacebookUtils.SIMPLE_DATE_FORMAT.parse(created_time);

                                // check if Likes has been given in the last 2 weeks
                                if ( date.before(twoWeeks) ) {
                                    return;
                                } else {
                                    mNumLikes++;
                                }
                            } catch ( JSONException e ) {
                                e.printStackTrace();
                            } catch ( ParseException e ) {
                                e.printStackTrace();
                            }
                        }

                        // get next page
                        Request requestForPagedResults = response.getRequestForPagedResults
                                (Response.PagingDirection.NEXT);

                        if ( requestForPagedResults != null ) {

                            // call this callback again
                            requestForPagedResults.setCallback(this);

                            // synchronized call, because this is executed in a AsyncTask
                            requestForPagedResults.executeAndWait();

                        } else {
                            Log.d(TAG, "End of like pagination");
                            Log.d(TAG, "Likes: " + mNumLikes);

                        }


                    }


                }

            }
        };

        Bundle bundle = new Bundle();
        bundle.putString("fields", "created_time");

        // Request to get a bunch of likes that will be counted. likeCallback is defined above.
        return new Request(Session.getActiveSession(), "/me/likes", bundle, null,
                           likeCallback);
    }

    private Request getPostingsV2(){

        List<String> permissions = Session.getActiveSession().getPermissions();

        Log.d(TAG, "Permissions Posts: " + permissions);

      /* make the API call */
        new Request(
                Session.getActiveSession(),
                "/me/statuses",
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {
            /* handle the result */
                        Log.d(TAG, "Got posts new: " + response.toString());
                    }
                }
        ).executeAsync();


        // Callback that is called, when the request is executed
        final Request.Callback likeCallback = new Request.Callback() {
            @Override
            public void onCompleted( Response response ) {
                Log.d(TAG, "Got results posts v2: " + response.toString());
                GraphObject graphObject = response.getGraphObject();


                if ( graphObject != null ) {

                    Object data = graphObject.getProperty("data");

                    if ( data != null ) {


                        JSONArray data1 = ( JSONArray ) data;

                        Log.d(TAG, "Num of Posts: " + data1.length());


                        Date twoWeeks = new Date(TWO_WEEKS);

                        // iterate through the returned Likes as long as their given during the
                        // last 2 week
                        for ( int i = 0 ; i < data1.length() ; i++ ) {
                            try {
                                JSONObject like = data1.getJSONObject(i);
                                Log.d(TAG, i + " " + like.toString());

                                String created_time = like.getString("updated_time");

                                Date date = FacebookUtils.SIMPLE_DATE_FORMAT.parse(created_time);

                                // check if Likes has been given in the last 2 weeks
                                if ( date.before(twoWeeks) ) {
                                    return;
                                } else {
                                    mNumPostings++;
                                }
                            } catch ( JSONException e ) {
                                e.printStackTrace();
                            } catch ( ParseException e ) {
                                e.printStackTrace();
                            }
                        }

                        // get next page
                        Request requestForPagedResults = response.getRequestForPagedResults
                                (Response.PagingDirection.NEXT);

                        if ( requestForPagedResults != null ) {

                            // call this callback again
                            requestForPagedResults.setCallback(this);

                            // synchronized call, because this is executed in a AsyncTask
                            requestForPagedResults.executeAndWait();

                        } else {
                            Log.d(TAG, "End of posts pagination");
                            Log.d(TAG, "Posts: " + mNumPostings);

                        }


                    }


                }

            }
        };

//        Bundle bundle = new Bundle();
//        bundle.putString("fields", "created_time");

        // Request to get a bunch of likes that will be counted. likeCallback is defined above.
        return new Request(Session.getActiveSession(), "/me/statuses", null, null,
                           likeCallback);
    }

    private Request getPostings() {

        mNumPostings = 0;

        // convert millis to seconds (Facebook time is in seconds)
        long twoWeeksFb = TWO_WEEKS / 1000;

        // FQLQuery to get the satus posts
        String fql = "SELECT time FROM status WHERE uid=me() AND time > " + twoWeeksFb;

        Log.d(TAG, "FQL: " + fql);

        Bundle params = new Bundle();
        params.putString("q", fql);

        // Callback that is called, when the request is executed
        Request.Callback postingsCallback = new Request.Callback() {
            @Override
            public void onCompleted( Response response ) {
                Log.d(TAG, "Got results posts: " + response.toString());

                GraphObject graphObject = response.getGraphObject();
                FacebookRequestError error = response.getError();


                if ( graphObject != null ) {

                    Object data = graphObject.getProperty("data");

                    if ( data != null ) {


                        JSONArray data1 = ( JSONArray ) data;

                        Log.d(TAG, "Num of Posts: " + data1.length());

                        // count status posts in the returned graph object (no iterating needed
                        // because it is already set in the WHERE of the FQL)
                        mNumPostings += data1.length();


                        String next;
                        try {
                            next = graphObject.getInnerJSONObject().getJSONObject
                                    ("paging").getString("next");


                            Request.newGraphPathRequest(Session.getActiveSession(), next,
                                                        this);


                        } catch ( JSONException e ) {
                            Log.d(TAG, "End of posting pagination");
                            Log.d(TAG, "Postings: " + mNumPostings);


                        }


                    }

                }

            }
        };
        return new Request(Session.getActiveSession(), "/fql", params, null,
                           postingsCallback
        );
    }

    @Override
    public void showInfoDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.overview);

        // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
        // possible Buttons are uneffected.
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.overview_info_dialog, null);


        // Set custom view
        builder.setView(view);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                // Restore preferences
                SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                        .PREF_FLAGS.PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_DATA_USAGE_INFO, false);


                // Commit the edits!
                editor.commit();
            }
        });

        builder.show();

    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        View rootView = inflater.inflate(R.layout.overview_fragment, container
                , false);

        mContentView = rootView.findViewById(R.id.container);
        mProgressContainer = rootView.findViewById(R.id.progress_container);

        mPlacesContainer = rootView.findViewById(R.id.container_places);
        mProgressPlaces = ( ProgressBar ) rootView.findViewById(R.id.progress_places);

        mContainerDay = rootView.findViewById(R.id.container_day);
        mContainerNight = rootView.findViewById(R.id.container_night);

        mAddressDay1View = ( TextView ) rootView.findViewById(R.id.place_day_1);
        mAddressDay2View = ( TextView ) rootView.findViewById(R.id.place_day_2);
        mAddressNight1View = ( TextView ) rootView.findViewById(R.id.place_night_1);
        mAddressNight2View = ( TextView ) rootView.findViewById(R.id.place_night_2);

        mNoPlacesView = rootView.findViewById(R.id.no_places);

        mNoInternetView = ( TextView ) rootView.findViewById(R.id.no_internet);

        String unit = "m";


        // Distance section
        final TextView distance = ( TextView ) rootView.findViewById(R.id.dist_travelled);

        // format distance to fit in every locale (decimal point, thousands-separator, ...)
        NumberFormat distanceFormat = NumberFormat.getInstance();
        distanceFormat.setMaximumFractionDigits(0);

        if ( mDistance > 1000 ) {
            mDistance /= 1000;
            unit = "km";
        }

        String formattedDistance = distanceFormat.format(mDistance);

        // set distance
        distance.setText(formattedDistance + " " + unit);

        // Facebook section
        mFbContainer = rootView.findViewById(R.id.fb_container);
        mProgressFb = ( ProgressBar ) rootView.findViewById(R.id.progress_fb);

        mDescFb = ( TextView ) rootView.findViewById(R.id.desc_fb);
        mNumPostingsView = ( TextView ) rootView.findViewById(R.id.num_postings_two_weeks);
        mNumLikesView = ( TextView ) rootView.findViewById(R.id.num_likes);
        mLoginButton = ( LoginButton ) rootView.findViewById(R.id.fb_login_button);
        mLoginButton.setReadPermissions(FacebookUtils.PERMS);

        mCoinsImg = ( ImageView ) rootView.findViewById(R.id.coins);

        // show dialog how the value of a post/like is calculated
        mFbContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setIcon(R.drawable.ic_dialog_info);

                builder.setTitle(R.string.obolus_fb_desc);


                // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
                // possible Buttons are uneffected.
                LayoutInflater inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View view = inflater.inflate(R.layout.fb_info_dialog, null);

                TextView valuePostLikeTextView = ( TextView ) view.findViewById(R.id.fb_value_post_like);

                valuePostLikeTextView.setText(getString(R.string.desc_value_post_like,
                                                        FacebookUtils.FB_POST_LIKE_VALUE,
                                                        Utils.UsdToEur(FacebookUtils
                                                                               .FB_POST_LIKE_VALUE)
                ));

                TextView descView = ( TextView ) view.findViewById(R.id.fb_desc_value_2);
                descView.setText(getString(R.string.fb_explain_value_2,
                                           FacebookUtils.FB_POST_LIKE_VALUE));

                // set link that opens the browser with the given link
                view.findViewById(R.id.link_fb_investor).setOnClickListener(new View
                        .OnClickListener() {
                    @Override
                    public void onClick( View v ) {
                        Uri uri = Uri.parse("http://investor.fb.com/releasedetail" +
                                                    ".cfm?ReleaseID=842071");
                        Intent openBrowser = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(openBrowser);
                    }
                });

                // set link that opens the browser with the given link
                view.findViewById(R.id.link_fb).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick( View v ) {
                        Uri uri = Uri.parse("https://www.facebook.com/photo" +
                                                    ".php?fbid=10151908376831729&set=a" +
                                                    ".10151908376636729.1073741825" +
                                                    ".20531316728&type=1&theater");
                        Intent openBrowser = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(openBrowser);
                    }
                });

                // set link that opens the browser with the given link
                view.findViewById(R.id.link_socialbarrel).setOnClickListener(new View
                        .OnClickListener() {
                    @Override
                    public void onClick( View v ) {
                        Uri uri = Uri.parse("http://socialbarrel" +
                                                    ".com/facebook-photo-library-now-250-billion-user-photos/53315/");
                        Intent openBrowser = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(openBrowser);
                    }
                });
                // Set custom view
                builder.setView(view);


                builder.setPositiveButton(android.R.string.ok, null);

                builder.show();

            }
        });


        return rootView;
    }

    /**
     * Update the UI according to the given {@link at.ac.uibk.awarenessy.app.overview
     * .OverviewFragment.State}.
     *
     * @param state State in which the UI should be
     */
    private void updateUI( State state ) {

        switch ( state ) {

            // have no internet connection (Places and Fb will not work)
            case NO_INTERNET:

                // info toast
                Toast.makeText(getActivity(), getString(R.string.no_internet),
                               Toast.LENGTH_LONG).show();

                // update places
                mNoInternetView.setVisibility(View.VISIBLE);
                mNoInternetView.setText(getString(R.string.err_no_internet_for_address));
                mProgressPlaces.setVisibility(View.GONE);


                // update facebook
                mDescFb.setText(getString(R.string.err_no_internet_fb));
                showFbMoneyContainer(false);

                // make container visible
                mProgressFb.setVisibility(View.GONE);
                mFbContainer.setVisibility(View.VISIBLE);

                break;

            // User is logged out
            case FB_LOGGED_OUT:

                updateFbUI(false);

                break;

            // User is logged in
            case FB_LOGGED_IN:
                updateFbUI(true);
                break;

            case PLACES_FINISH:
                mPlacesContainer.setVisibility(View.VISIBLE);
                mProgressPlaces.setVisibility(View.GONE);
                break;

            case OK:
                // OK, do nothing
                break;


        }

        // hide big Progress and make the overall view visible
        mProgressContainer.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);

    }

    /**
     * Update date Facebook Container.
     * <p/>
     * Layout differs if the user is logged in (show info) or logged out (ask to log in).
     *
     * @param loggedIn if the "Logged In UI" or the "Logged Out UI" should be displayed
     */
    private void updateFbUI( boolean loggedIn ) {

        if ( loggedIn ) {
            mDescFb.setText(R.string.fb_money_logged_in);
            mLoginButton.setVisibility(View.GONE);

        } else {
            mDescFb.setText(R.string.fb_money_logged_out);
            mLoginButton.setVisibility(View.VISIBLE);
        }

        showFbMoneyContainer(loggedIn);


        // make container visible
        mProgressFb.setVisibility(View.GONE);
        mFbContainer.setVisibility(View.VISIBLE);

    }

    private void showFbMoneyContainer( boolean show ) {

        int visibility = show ? View.VISIBLE : View.GONE;

        mNumLikesView.setVisibility(visibility);
        mNumPostingsView.setVisibility(visibility);
        mCoinsImg.setVisibility(visibility);
    }

    /**
     * State in which the UI can be.
     */
    private enum State {

        OK, FB_LOGGED_IN, PLACES_FINISH, FB_LOGGED_OUT, NO_INTERNET

    }

    /**
     * Subclass of AsyncTask that executes given {@link com.facebook.Request} and their {@link
     * com.facebook.Response}s.
     * <p/>
     * At the beginning, check if the user is logged in. If not, try it up to 5 times each 100 ms
     * again. This is necessary, cause at app start, there is a latency where the app connects to
     * facebook. In this time {@link at.ac.uibk.awarenessy.app.utils
     * .FacebookUtils#isLoggedIn()} returns false, though the user is logged in. If the user is
     * logged in, the batch will be executed synchronously on the Task Thread. If all 5 tries fail,
     * the batch will executed too, but it gets a Error in the Response, so OnPostExecute handles
     * this.
     * <p/>
     * Note: Most logic is handled in the {@link com.facebook.Request.Callback}s of the Requests.
     */
    private class FacebookTask extends AsyncTask<Request, Void, List<Response>> {

        @Override
        protected List<Response> doInBackground( Request... params ) {

            mNumLikes = 0;
            mNumPostings = 0;

            RequestBatch batch = new RequestBatch();

            batch.addAll(Arrays.asList(params));

            return batch.executeAndWait();
        }

        @Override
        protected void onPostExecute( List<Response> responses ) {

            if ( !isAdded() ) {
                return;
            }

            for ( Response response : responses ) {
                FacebookRequestError error = response.getError();
                if ( error != null ) {


                    // Not logged in
                    if ( error.getErrorCode() == -1 ) {
                        Log.d(TAG, "Error: " + error.getErrorMessage());
                        updateUI(State.FB_LOGGED_OUT);
                        return;

                    }
                }
            }


            // calc values
            float value_likes_USD = FacebookUtils.FB_POST_LIKE_VALUE * mNumLikes;
            float value_postings_USD = FacebookUtils.FB_POST_LIKE_VALUE * mNumPostings;

            // convert to euros
            float value_likes_EUR = Utils.UsdToEur(value_likes_USD);
            float value_postings_EUR = Utils.UsdToEur(value_postings_USD);


            String postings;
            if ( mNumPostings == 0 ) {
                // no posts in the last 14 days
                postings = getString(R.string.no_value_posts);
            } else {
                postings = getResources().getQuantityString(R.plurals.value_posts, mNumPostings,
                                                            value_postings_USD,
                                                            value_postings_EUR, mNumPostings);
            }


            String likes;
            if ( mNumLikes == 0 ) {
                // no likes in the last 14 days
                likes = getString(R.string.no_value_likes);
            } else {
                likes = getResources().getQuantityString(R.plurals.value_likes, mNumLikes,
                                                         value_likes_USD, value_likes_EUR,
                                                         mNumLikes);
            }


            mNumPostingsView.setText(postings);

            mNumLikesView.setText(likes);

            // update ui to hide progress and make results visible
            updateUI(State.FB_LOGGED_IN);


        }
    }


    /**
     * Subclass of AsyncTask that calculates the 4 most common places of residence in the last 2
     * weeks.
     * <p/>
     * At the beginning, two queries are constructed. One for places at daytime, the other for
     * places at night. Each query is executed and for each the two places which are further away
     * from each other than the 2 x {@link OverviewFragment#DIST_KM}.
     */
    private class PlacesTask extends AsyncTask<Void, Void, Void> {

        private LatLng mFirstPlaceDay;
        private LatLng mSecondPlaceDay;
        private LatLng mFirstPlaceNight;
        private LatLng mSecondPlaceNight;

        private String mAddrDay1;
        private String mAddrDay2;
        private String mAddrNight1;
        private String mAddrNight2;

        private PlacesTask() {
        }

        @Override
        protected Void doInBackground( Void... params ) {


            Log.d(TAG, "Lat: " + LAT_DIFF + "\tLong: " + mLongDiff);


            DaoSession daoSession = Utils.getDaoSession(getActivity());
            SQLiteDatabase db = daoSession.getDatabase();

            // num of locations in db
            long entries = daoSession.getSimpleLocationDao().queryBuilder().count();
            long count = entries / 10;

            Log.d(TAG, entries + " entries\tonly showing the locations with at least " + count +
                    " " + "other location in a radius of " + DIST_KM + " km");

            // construct queries
            String queryNight = getQuery(false);
            String queryDay = getQuery(true);

            Log.d(TAG, queryDay);
            Log.d(TAG, queryNight);

            // execute both queries
            Cursor cursorDay = db.rawQuery(queryDay, null);
            Cursor cursorNight = db.rawQuery(queryNight, null);

            Log.d(TAG, "Count: " + cursorDay.getCount());

            // get two Day-Places
            while ( cursorDay.moveToNext() && (mSecondPlaceDay == null) ) {

                double lat = cursorDay.getDouble(0);
                double longitude = cursorDay.getDouble(1);


                // insert very first place
                if ( mFirstPlaceDay == null ) {
                    mFirstPlaceDay = new LatLng(lat, longitude);
                    mAddrDay1 = cursorDay.getString(2);
                } else {

                    // check if distance between the first found place and the current is far enough
                    float distance = LocationUtils.distanceBetween(mFirstPlaceDay.latitude,
                                                                   mFirstPlaceDay.longitude, lat,
                                                                   longitude);

                    // double distance cause "search circle" has a perimeter of 2*DIST_KM
                    if ( distance > DIST_KM * 2 * 1000 ) {
                        mSecondPlaceDay = new LatLng(lat, longitude);
                        mAddrDay2 = cursorDay.getString(2);
                    }
                }

            }

            // get two Night-Places
            while ( cursorNight.moveToNext() && (mSecondPlaceNight == null) ) {

                double lat = cursorNight.getDouble(0);
                double longitude = cursorNight.getDouble(1);

                // insert very first place
                if ( mFirstPlaceNight == null ) {
                    mFirstPlaceNight = new LatLng(lat, longitude);
                    mAddrNight1 = cursorNight.getString(2);
                } else {

                    // check if distance between the first found place and the current is far enough
                    float distance = LocationUtils.distanceBetween(mFirstPlaceNight.latitude,
                                                                   mFirstPlaceNight.longitude, lat,
                                                                   longitude);

                    // double distance cause "search circle" has a perimeter of 2*DIST_KM
                    if ( distance > DIST_KM * 2 * 1000 ) {
                        mSecondPlaceNight = new LatLng(lat, longitude);
                        mAddrNight2 = cursorNight.getString(2);
                    }
                }

            }


            // calculate addresses
            if ( mAddrDay1 == null ) { mAddrDay1 = getAddress(mFirstPlaceDay); }
            if ( mAddrDay2 == null ) { mAddrDay2 = getAddress(mSecondPlaceDay); }
            if ( mAddrNight1 == null ) { mAddrNight1 = getAddress(mFirstPlaceNight); }
            if ( mAddrNight2 == null ) { mAddrNight2 = getAddress(mSecondPlaceNight); }

            Log.d(TAG, "D1: " + mAddrDay1 + " D2: " + mAddrDay2 + " N1: " + mAddrNight1 + " N2: "
                    + mAddrNight2);

            return null;

        }

        @Override
        protected void onPostExecute( Void aVoid ) {

            updateUI(State.PLACES_FINISH);

            // no Places found, show only one message
            if ( mAddrDay1 == null && mAddrNight1 == null ) {

                mContainerDay.setVisibility(View.GONE);
                mContainerNight.setVisibility(View.GONE);

                mNoPlacesView.setVisibility(View.VISIBLE);

                return;

            }

            // at least one place has been found
            if ( mAddrDay1 != null ) {
                mAddressDay1View.setText(mAddrDay1);
                mAddressDay1View.setTypeface(Typeface.DEFAULT_BOLD);

                // only set day2 when a day1 exists, have both places found
                if ( mAddrDay2 != null ) {
                    mAddressDay2View.setText(mAddrDay2);
                } else {
                    mAddressDay2View.setVisibility(View.GONE);
                }

            } else if ( mAddrDay2 != null ) {
                // day1 is empty, so use day2 as day1
                mAddressDay1View.setText(mAddrDay2);
                mAddressDay1View.setTypeface(Typeface.DEFAULT_BOLD);

                // have no day2, hide
                mAddressDay2View.setVisibility(View.GONE);

            } else {
                // no place has been found
                mAddressDay1View.setText(R.string.no_place_found);

                // have no day2
                mAddressDay2View.setVisibility(View.GONE);

            }

            // same as for day
            if ( mAddrNight1 != null ) {
                mAddressNight1View.setText(mAddrNight1);
                mAddressNight1View.setTypeface(Typeface.DEFAULT_BOLD);

                if ( mAddrNight2 != null ) {
                    mAddressNight2View.setText(mAddrNight2);
                } else {
                    mAddressNight2View.setVisibility(View.GONE);
                }
            } else if ( mAddrNight2 != null ) {
                // night1 is empty, so use night2
                mAddressNight1View.setText(mAddrNight2);
                mAddressNight1View.setTypeface(Typeface.DEFAULT_BOLD);

                // have no night2
                mAddressNight2View.setVisibility(View.GONE);
            } else {

                mAddressNight1View.setText(R.string.no_place_found);

                // have no night2
                mAddressNight2View.setVisibility(View.GONE);
            }


        }

        private String getAddress( LatLng position ) {

            return position != null ? LocationUtils.calcLocationAddress(getActivity(),
                                                                        position.latitude,
                                                                        position.longitude) : null;
        }
    }

    /**
     * Returns the SQLite query to get the clusters for the defined boundaries ({@link
     * OverviewFragment#LAT_DIFF} and {@link at.ac.uibk.awarenessy.app.overview
     * .OverviewFragment#mLongDiff} in the last 2 weeks ({@link OverviewFragment#TWO_WEEKS}
     *
     * @param day true if only places between {@link OverviewFragment#BEGIN_DAY} and
     *            {@link OverviewFragment#END_DAY} should be returned, else false
     *
     * @return the query to get the clusters for day or night
     */
    private String getQuery( boolean day ) {

        return "SELECT s1.LATITUDE, s1.LONGITUDE, s1.ADDRESS" +
                " FROM SIMPLE_LOCATION s1" +
                " LEFT JOIN SIMPLE_LOCATION s2" +
                " ON s2.LATITUDE < s1.LATITUDE + " + LAT_DIFF + " AND " +
                " s2.LATITUDE > s1.LATITUDE - " + LAT_DIFF + " AND" +
                " s2.LONGITUDE < s1.LONGITUDE + " + mLongDiff + " AND" +
                " s2.LONGITUDE > s1.LONGITUDE - " + mLongDiff +
                " WHERE s1.TIME > " + TWO_WEEKS + " AND" +
                " s2.TIME > " + TWO_WEEKS + " AND" +
                " s1._id != s2._id AND" +
                " (" + getWhereForTimeLimits(day, "s1") + ") AND" +
                " (" + getWhereForTimeLimits(day, "s2") + ") " +
                " GROUP BY s1._id\n" +
                " ORDER BY count(*) DESC";
    }

    /**
     * Get complicated WHERE clause to constrain results only for places at day or night.
     *
     * @param day            true if only places between {@link OverviewFragment#BEGIN_DAY} and
     *                       {@link OverviewFragment#END_DAY} should be returned, else false
     * @param simpleLocation the name of the table where the places should come from
     *
     * @return the WHERE clause to contrain the query
     */
    private String getWhereForTimeLimits( boolean day, String simpleLocation ) {


        String s = "CAST(strftime('%H', datetime(" + simpleLocation + ".TIME/1000, 'unixepoch', " +
                "'localtime')) AS INTEGER) " + (day ? ">" : "<") + " " + BEGIN_DAY + " " + (day ?
                "AND" : "OR") + " CAST(strftime" +
                "('%H',datetime(" + simpleLocation + ".TIME / 1000, 'unixepoch'," +
                "'localtime')) AS INTEGER) " + (day ? "<" : ">")
                + " " + END_DAY + " ";

        Log.d(TAG, s);

        return s;

    }

}
