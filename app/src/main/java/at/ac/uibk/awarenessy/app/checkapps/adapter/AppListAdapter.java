/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.checkapps.model.AppEntry;
import at.ac.uibk.awarenessy.app.checkapps.model.Danger;

/**
 * Own ArrayAdapter implementation
 * <p/>
 * Defines a own Layout for a List.
 * Uses apps_app_list_item.xml as the layout, which displays the AppIcon, the AppName and the Danger points.
 * <p/>
 */
public class AppListAdapter extends ArrayAdapter<AppEntry> {

    static class ViewHolder {
        public ImageView image;
        public TextView packageName;
        public TextView dangerPoints;
        public ImageView danger;

    }

    public AppListAdapter(Context context) {
        super(context, R.layout.apps_app_list_item);
    }

    /**
     * Set the specified list as the data for the adapter.
     * Possible prior data will be removed.
     *
     * @param data List of {@link at.ac.uibk.awarenessy.app.checkapps.model.AppEntry}s to add to the adapter
     */
    public void setData(List<AppEntry> data) {
        clear();

        if (data != null) {
            addAll(data);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View appView = convertView;

        if (appView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            appView = inflater.inflate(R.layout.apps_app_list_item, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.packageName = (TextView) appView.findViewById(R.id.package_name);
            viewHolder.image = (ImageView) appView.findViewById(R.id.icon_list);
            viewHolder.dangerPoints = (TextView) appView.findViewById(R.id.danger_points);
            viewHolder.danger = (ImageView) appView.findViewById(R.id.danger);

            appView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) appView.getTag();


        // Package at the current displayed position
        AppEntry app = getItem(position);

        Danger danger = app.getDanger();
        holder.dangerPoints.setText(String.valueOf(danger.getDangerPoints()));

        // set correct danger icons (Danger Triangle with exlcamation marks) according to the danger.
        switch (danger.getDanger()) {

            case HARMFUL:
                holder.danger.setImageResource(R.drawable.ic_harmful_danger);
                break;
            case AVERAGE:
                holder.danger.setImageResource(R.drawable.ic_average_danger);
                break;
            case HARMLESS:
                holder.danger.setImageResource(R.drawable.ic_harmless_danger);
                break;
            case ABSOLUTE_NO_DANGER:
                holder.danger.setImageResource(R.drawable.ic_harmless_danger);
                break;
        }

        holder.packageName.setText(app.getName());
        holder.image.setImageDrawable(app.getIcon());


        return appView;
    }


}
