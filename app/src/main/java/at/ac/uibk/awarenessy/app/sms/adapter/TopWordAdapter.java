/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import at.ac.uibk.awarenessy.app.R;

import java.util.List;
import java.util.Map;

/**
 * Own ArrayAdapter implementation to show a Ranking of SMS in a list.
 * <p/>
 * Defines a own Layout for a List.
 * Uses sms_word_list_itemitem.xml as the layout, which displays the contact photo (if existing), the name (or number if no name exists), number of received and sent SMS.
 */
public class TopWordAdapter extends ArrayAdapter<Map.Entry<String, Integer>> {

    private final Context mContext;
    private static float mOriginalSize;


    static class ViewHolder {
        TextView rank;
        TextView word;
        TextView count;
    }


    public TopWordAdapter(Context context, List<Map.Entry<String, Integer>> data) {

        super(context, R.layout.sms_word_list_item, data);
        mContext = context;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View wordView = convertView;

        Map.Entry<String, Integer> contact = getItem(position);


        String word = contact.getKey();
        Integer count = contact.getValue();


        if (wordView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            wordView = inflater.inflate(R.layout.sms_word_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.rank = (TextView) wordView.findViewById(R.id.rank);
            viewHolder.word = (TextView) wordView.findViewById(R.id.word);
            viewHolder.count = (TextView) wordView.findViewById(R.id.count);

            mOriginalSize = viewHolder.rank.getTextSize();

            wordView.setTag(viewHolder);

        }

        ViewHolder holder = (ViewHolder) wordView.getTag();

        int rank = position + 1;
        holder.rank.setText(String.valueOf(rank));

        float newSize = newSize(mOriginalSize, position);
        holder.rank.setTextSize(TypedValue.COMPLEX_UNIT_PX, newSize);

        holder.word.setText(String.valueOf(word));
        holder.count.setText(String.valueOf(count));

        // Returns the item layout view
        return wordView;
    }

    private float newSize(float oldSize, int position) {

        float d = oldSize / 30;


        if (position > 10) {
            position = 10;
        }

        return oldSize - d * position;


    }

}
