/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.checkapps.adapter.PermissionListAdapter;
import at.ac.uibk.awarenessy.app.checkapps.model.AppEntry;
import at.ac.uibk.awarenessy.app.checkapps.model.MyPermissionInfo;


/**
 * Fragment that Display the Details (Icon, Name, Permissions, ...) of an App.
 * <p/>
 * It gets the displaying App by three different ways:<p/>
 * - call updateAppView(PackageInfo): Standard way in two pane view (only on Large Screens)<p/>
 * - passed Argument before creating this Fragment: Standard way in one pane view<p/>
 * - mCurrentApp has been set onCreateView: after reloading view (e.g. Screen Rotationn
 */
public class AppDetailFragment extends Fragment implements AdapterView.OnItemClickListener,
        View.OnClickListener {

    public static final  String FRAGMENT_TAG = AppDetailFragment.class.getName();
    private final static String TAG          = AppDetailFragment.class.getSimpleName();

    public final static String   ARG_PACKAGE = "package";
    private             AppEntry mCurrentApp = null;

    private View mHeaderView;

    private List<MyPermissionInfo> mPermList;


    private MyPermissionInfo mSelectedPermission;
    private ImageView        mAppIconView;
    private TextView         mAppNameView;
    private ListView         mListView;
    private View             mProgressContainer;
    private TextView         mUsageHint;
    private View             mListContainer;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AppDetailFragment() {

    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.apps_detail_fragment, container, false);


        mHeaderView = rootView.findViewById(R.id.app_header);
        mAppIconView = ( ImageView ) rootView.findViewById(R.id.icon);
        mAppNameView = ( TextView ) rootView.findViewById(R.id.label);

        mProgressContainer = rootView.findViewById(R.id.progress_container);
        mListView = ( ListView ) rootView.findViewById(android.R.id.list);
        mUsageHint = ( TextView ) rootView.findViewById(R.id.app_detail_usage_hint);

        mListContainer = rootView.findViewById(R.id.listContainer);
        mListView.setEmptyView(rootView.findViewById(android.R.id.empty));

        // hide list container (the list itself and empty view) and let progressContainer displayed
        showList(false);
        showProgressContainer(true);

        return rootView;
    }


    @Override
    public void onActivityCreated( Bundle savedInstanceState ) {

        super.onActivityCreated(savedInstanceState);

        // If activity recreated (such as from screen rotate), restore
        // the previous article selection set by onSaveInstanceState().
        // This is primarily necessary when in the two-pane layout.
        if ( savedInstanceState != null ) {
            mCurrentApp = savedInstanceState.getParcelable(ARG_PACKAGE);
        }

    }


    @Override
    public void onStart() {

        super.onStart();


        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
        Bundle args = getArguments();
        if ( args != null ) {
            AppEntry appEntry = args.getParcelable(ARG_PACKAGE);

            // Set permission based on argument passed in
            updateAppView(appEntry);
        } else if ( mCurrentApp != null ) {
            // Set permission based on saved instance state defined during onCreateView
            updateAppView(mCurrentApp);
        }


        // Behaviour when clicking on a Permission (show details)
        mListView.setOnItemClickListener(this);


    }


    /**
     * Update permission view.
     * <p/>
     * Show Permissions of the specified app.
     *
     * @param app App to show in the view
     */
    public void updateAppView( AppEntry app ) {

        //Set Image on the top
        if ( app.getPackageInfo().applicationInfo != null ) {
            PackageManager pm = getActivity().getPackageManager();
            mAppIconView.setImageDrawable(app.getPackageInfo().applicationInfo.loadIcon(pm));
        }

        mAppNameView.setText(app.getName());

        String[] perms = app.getPackageInfo().requestedPermissions;


        if ( perms != null ) {
            mPermList = app.getPermissions();
            Collections.sort(mPermList, MyPermissionInfo.DANGER_COMPARATOR_DESC);
        } else {
            // needed to let empty view displayed
            mPermList = new ArrayList<MyPermissionInfo>();
        }

        PermissionListAdapter adapter = new PermissionListAdapter(getActivity(), mPermList);
        mListView.setAdapter(adapter);

        showList(true);
        showHeader(true);
        showUsageHint(false);
        showProgressContainer(false);


        // save current package in the case we need it later again (e.g. after screen rotation)
        mCurrentApp = app;
    }

    /**
     * Show/hide the header  (App icon and app name) of the detail view.
     *
     * @param show True if the header should be shown, false otherwise
     */
    public void showHeader( boolean show ) {

        mHeaderView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /**
     * Show/hide the progress indicator (loading animation) of the detail view.
     * This view is only needed in the two pane view.
     *
     * @param show True if the indicator should be shown, false otherwise
     */
    public void showProgressContainer( boolean show ) {

        mProgressContainer.setVisibility(show ? View.VISIBLE : View.GONE);

    }

    /**
     * Show/hide the List of the detail view.
     * Shows or hide the entire list container, which contains the list itself and the empty view.
     *
     * @param show True if the list and possible empty view should be shown, false otherwise
     */
    public void showList( boolean show ) {

        mListContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /**
     * Show/hide the usage hint of the detail view.
     * This view is only needed in the two pane view.
     *
     * @param show True if the usage hint should be shown, false otherwise.
     */
    public void showUsageHint( boolean show ) {

        mUsageHint.setVisibility(show ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onSaveInstanceState( Bundle outState ) {

        super.onSaveInstanceState(outState);

        // Save the current package selection in case we need to recreate the fragment
        outState.putParcelable(ARG_PACKAGE, mCurrentApp);
    }


    @Override
    public void onItemClick( AdapterView<?> adapterView, View v, int position, long id ) {

        // Create a Dialog that displays infos about the permission.


        Activity activity = getActivity();

        mSelectedPermission = mPermList.get(position);

        // Build a new Dialog to show details about the clicked permission.
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
        // possible Buttons are uneffected.
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.apps_permission_dialog, null);

        // Set custom view
        builder.setView(view);


        // UI components
        TextView title = ( TextView ) view.findViewById(R.id.title);
        TextView descrView = ( TextView ) view.findViewById(R.id.description);
        View divider = view.findViewById(R.id.divider);
        TextView protectionLevelView = ( TextView ) view.findViewById(R.id.protection_level);
        ImageView indicatorView = ( ImageView ) view.findViewById(R.id.indicator_dialog);
        ImageButton permInfoButton = ( ImageButton ) view.findViewById(R.id.ibtn_perm_info);


        title.setText(mSelectedPermission.getName());

        if ( mSelectedPermission.getDescription().isEmpty() ) {
            divider.setVisibility(View.INVISIBLE);
        }

        CharSequence descr = mSelectedPermission.getDescription();

        // Some Signature Permissions have no Description
        if ( descr == null | descr.length() == 0 ) {
            descrView.setVisibility(View.GONE);
        } else {
            descrView.setText(descr);
        }

        // Action when the info button is pressed.
        permInfoButton.setOnClickListener(this);


        indicatorView.setImageResource(mSelectedPermission.getIndicatorRes());
        protectionLevelView.setText(mSelectedPermission.getProtectionLevelName());

        builder.setCancelable(true);


        final AlertDialog dialog = builder.show();

        // let dialog close if clicking on in
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {

                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);


    }

    @Override
    public void onClick( View view ) {

        // Build new dialog when the info button in the permission dialog has been clicked.

        Activity activity = getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // Use a custom layout, so get LayoutInflater and inflate the view.
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.simple_dialog, null);

        // Set custom view (contains header and simple message)
        builder.setView(dialogView);

        // UI components
        TextView title = ( TextView ) dialogView.findViewById(R.id.title);
        TextView message = ( TextView ) dialogView.findViewById(R.id.message);

        title.setText(mSelectedPermission.getProtectionLevelName());
        message.setText(getString(mSelectedPermission.getProtectionLevelInfo()));

        builder.setCancelable(true);

        final AlertDialog dialog = builder.show();

        // let dialog close if clicking on in
        dialogView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ) {

                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);


    }
}
