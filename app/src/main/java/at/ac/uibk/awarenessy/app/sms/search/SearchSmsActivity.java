/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms.search;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.widget.ListView;
import android.widget.TextView;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.sms.adapter.SmsAdapter;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.dao.SmsDao;

import java.util.List;
import java.util.Locale;


/**
 * Activity that will be started when the user searches for messages.
 * <p/>
 * Some parts of the code taken from the Android Developer Guide <a href="http://developer
 * .android.com/guide/topics/search/search-dialog.html">Search</a>
 */
public class SearchSmsActivity extends Activity {


    private TextView mTextView;
    private ListView mListView;

    private SmsDao smsDao;

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sms_search_sms_activity);

        mTextView = ( TextView ) findViewById(R.id.text);
        mListView = ( ListView ) findViewById(R.id.list);


        smsDao = Utils.getDaoSession(this).getSmsDao();

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if ( Intent.ACTION_SEARCH.equals(intent.getAction()) ) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            query = query.trim();

            // save recent query
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                                                                              SmsSuggestionProvider.AUTHORITY, SmsSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);

            // search in database and display results
            doSearch(query);
        }
    }

    private void doSearch( String query ) {

        // add Wildcards to the query to find query within a string
        String sqliteQuery = "%" + query.toLowerCase(Locale.getDefault()) + "%";

        List<Sms> results = smsDao.queryBuilder().where(SmsDao.Properties.Message.like
                (sqliteQuery)).orderDesc(SmsDao.Properties.Time).list();


        if ( results.isEmpty() ) {
            // There are no results
            mTextView.setText(getString(R.string.no_results, query));
        } else {
            // Display the number of results
            int count = results.size();
            String countString = getResources().getQuantityString(R.plurals.search_results,
                                                                  count, count, query);
            mTextView.setText(countString);


            SmsAdapter adapter = new SmsAdapter(this, results);

            mListView.setAdapter(adapter);

        }
    }

}
