/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider.adapter;

/**
 * A {@code DataUnit} represents data amounts at a given unit and provides utility methods to
 * convert across units. A {@code DataUnit} does not maintain data information, but only helps
 * organize data representations.
 * <p/>
 * The measurement system (Decimal or Binary) on which the conversions should be based on can be
 * set with {@code setMeasurementSystem}. Standard is
 * {@link DataUnit.MeasurementSystem#DECIMAL}
 * ( = 1000)
 */

public enum DataUnit {


    BYTES {
        public double toBytes( double b ) {return b;}

        public double toKilobytes( double b ) {return b / B1;}

        public double toMegabytes( double b ) {return b / B2;}

        public double toGigabytes( double b ) {return b / B3;}

        public double toTerabytes( double b ) {return b / B4;}

        public double convert( double b, DataUnit u ) {return u.toBytes(b);}

    }, KILOBYTES {
        public double toBytes( double b ) {return b * B1;}

        public double toKilobytes( double b ) {return b;}

        public double toMegabytes( double b ) {return b / B1;}

        public double toGigabytes( double b ) {return b / B2;}

        public double toTerabytes( double b ) {return b / B3;}

        public double convert( double b, DataUnit u ) {return u.toKilobytes(b);}


    }, MEGABYTES {
        public double toBytes( double b ) {return b * B2;}

        public double toKilobytes( double b ) {return b * B1;}

        public double toMegabytes( double b ) {return b;}

        public double toGigabytes( double b ) {return b / B1;}

        public double toTerabytes( double b ) {return b / B2;}

        public double convert( double b, DataUnit u ) {return u.toMegabytes(b);}

    }, GIGABYTES {
        public double toBytes( double b ) {return b * B3;}

        public double toKilobytes( double b ) {return b * B2;}

        public double toMegabytes( double b ) {return b * B1;}

        public double toGigabytes( double b ) {return b;}

        public double toTerabytes( double b ) {return b / B1;}

        public double convert( double b, DataUnit u ) {return u.toGigabytes(b);}

    }, TERABYTES {
        public double toBytes( double b ) {return b * B4;}

        public double toKilobytes( double b ) {return b * B3;}

        public double toMegabytes( double b ) {return b * B2;}

        public double toGigabytes( double b ) {return b * B1;}

        public double toTerabytes( double b ) {return b;}

        public double convert( double b, DataUnit u ) {return u.toGigabytes(b);}


    };


    @Override
    public String toString() {
        switch ( this ) {

            case BYTES:
                return "Bytes";
            case KILOBYTES:
                return "Kilobytes";
            case MEGABYTES:
                return "Megabytes";
            case GIGABYTES:
                return "Gigabytes";
            case TERABYTES:
                return "Terabytes";
        }

        return "";
    }


    public String toStringShort() {
        switch ( this ) {

            case BYTES:
                return "B";
            case KILOBYTES:
                return "KB";
            case MEGABYTES:
                return "MB";
            case GIGABYTES:
                return "GB";
            case TERABYTES:
                return "TB";
        }

        return "";
    }

    enum MeasurementSystem {
        DECIMAL(1000), BINARY(1024);

        private final int value;

        MeasurementSystem( int value ) {
            this.value = value;
        }
    }

    // constant to set measurement system for conversion (1000 = Decimal, 1024 = Binary)
    private static        MeasurementSystem SYSTEM = MeasurementSystem.DECIMAL;
    // Handy constants for conversion methods
    private static final long              B1     = SYSTEM.value;
    private static final long              B2     = B1 * B1;
    private static final long              B3     = B2 * B1;
    private static final long              B4     = B3 * B1;

    /**
     * Set the {@link DataUnit
     * .MeasurementSystem}
     * for all unit conversions.
     * A measurement system has either the decimal basis (1 Kilobyte = 1000 Bytes) or decimal (1
     * Kilobyte = 1024 Bytes).
     *
     * @param system the measurement system on which the calculations should be based on
     *
     * @see <a href="http://en.wikipedia.org/wiki/Binary_prefix">Binary prefix</a>
     */
    public static void setMeasurementSystem( MeasurementSystem system ) {
        if ( system == null ) {
            throw new IllegalArgumentException("System has to be MeasurementSystem.DECIMAL or " +
                                                       "MeasurementSystem.BINARY.");
        }

        SYSTEM = system;


    }

    /**
     * Convert the given data amount in the given unit to this unit using the defined measurement
     * system (decimal or binary).
     * <p/>
     * For example, to convert 10 Megabytes to Bytes, use:
     * {@code DataUnit.BYTES.convert(10, DataUnit.MEGABYTES)}
     *
     * @param sourceAmount the data amount in the given {@code sourceUnit}
     * @param sourceUnit   the unit of the {@code sourceAmount} argument
     *
     * @return the converted data in this unit.
     *
     * @see DataUnit#setMeasurementSystem
     * (DataUnit.MeasurementSystem)
     */
    public abstract double convert( double sourceAmount, DataUnit sourceUnit );

    /**
     * Convert the given data amount into bytes using the defined measurement system (decimal
     * or binary).
     *
     * @param amount the data amount in a any unit
     *
     * @return the converted data in bytes
     *
     * @see DataUnit#setMeasurementSystem
     * (DataUnit.MeasurementSystem)
     */
    public abstract double toBytes( double amount );

    /**
     * Convert the given data amount into kilobytes using the defined measurement system (decimal
     * or binary).
     *
     * @param amount the data amount in a any unit
     *
     * @return the converted data in kilobytes
     *
     * @see DataUnit#setMeasurementSystem
     * (DataUnit.MeasurementSystem)
     */
    public abstract double toKilobytes( double amount );

    /**
     * Convert the given data amount into megabytes using the defined measurement system (decimal
     * or binary).
     *
     * @param amount the data amount in a any unit
     *
     * @return the converted data in megabytes
     *
     * @see DataUnit#setMeasurementSystem
     * (DataUnit.MeasurementSystem)
     */
    public abstract double toMegabytes( double amount );

    /**
     * Convert the given data amount into gigabytes using the defined measurement system (decimal
     * or  binary).
     *
     * @param amount the data amount in a any unit
     *
     * @return the converted data in gigabytes
     */
    public abstract double toGigabytes( double amount );

    /**
     * Convert the given data amount into terabytes using the defined measurement system (decimal
     * or binary).
     *
     * @param amount the data amount in a any unit
     *
     * @return the converted data in terabytes
     */
    public abstract double toTerabytes( double amount );


}
