/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import at.ac.uibk.awarenessy.app.BuildConfig;
import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.checkapps.adapter.AppListAdapter;
import at.ac.uibk.awarenessy.app.checkapps.model.AppEntry;
import at.ac.uibk.awarenessy.app.main.MySearchView;

/**
 * Fragment to display a List of installed Packages/Apps.
 * <p/>
 * Deliver messages (clicked App) to the containing Activity via the IAppListCallback Interface.
 * Container Activity (CheckAppsActivity) must implement this Interface.
 */
public class AppListFragment extends ListFragment implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener, LoaderManager.LoaderCallbacks<List<AppEntry>> {

    public static final  String FRAGMENT_TAG              = AppListFragment.class.getName();
    private final        String TAG                       = this.getClass().getName();
    // The SearchView for doing filtering.
    private SearchView       mSearchView;
    private IAppListCallback mCallback;
    // This is the Adapter being used to display the list's data.
    private AppListAdapter   mAdapter;
    private long             start;
    private boolean mFinished  = false;
    private boolean mTwoPane;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AppListFragment() {
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        // We have a menu item to show in action bar.
        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState ) {
        super.onActivityCreated(savedInstanceState);


        setEmptyText(getString(R.string.no_app_installed));


        // Create an apps_empty_permissions adapter we will use to display the loaded data.
        mAdapter = new AppListAdapter(getActivity());
        setListAdapter(mAdapter);


        // Start out with a progress indicator.
        setListShown(false);


        View detailView = getActivity().findViewById(R.id.app_detail_container);
        mTwoPane = detailView != null && detailView.getVisibility() == View.VISIBLE;

        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);


        // When in two-pane layout, set the listview to highlight the selected list item
        if ( mTwoPane ) {

            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        }


    }


    @Override
    public void onAttach( Activity activity ) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = ( IAppListCallback ) activity;
        } catch ( ClassCastException e ) {
            throw new ClassCastException(activity.toString()
                                                 + " must implement IAppListCallback");
        }
    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {

        // Place an action bar item for searching.
        inflater.inflate(R.menu.apps_list_app_actions, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        mSearchView = new MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        mSearchView.setIconifiedByDefault(true);

        mSearchView.setQueryHint(getString(R.string.hint_search_app));

        item.setActionView(mSearchView);

        int searchPlateId = mSearchView.getContext().getResources().getIdentifier
                ("android:id/search_plate", null, null);
        View searchPlate = mSearchView.findViewById(searchPlateId);
        if ( searchPlate != null ) {
            int searchTextId = searchPlate.getContext().getResources().getIdentifier
                    ("android:id/search_src_text", null, null);
            TextView searchText = ( TextView ) searchPlate.findViewById(searchTextId);
            if ( searchText != null ) {
                searchText.setTextColor(Color.LTGRAY);
                searchText.setHintTextColor(Color.GRAY);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {

        switch ( item.getItemId() ) {

            case R.id.action_search:

                // No need to handle something here... will be handled in onCreateOptionsMenu
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick( ListView l, View v, int position, long id ) {

        showApp(position);


    }

    private void showApp( int position ) {
        // Package at the current displayed position
        AppEntry app = mAdapter.getItem(position);

        // Notify the parent activity of selected item
        mCallback.onAppSelected(app);

        // Set the item as checked to be highlighted when in two-pane layout
        getListView().setItemChecked(position, true);
    }

    public boolean isInTwoPane() {
        return mTwoPane;
    }

    @Override
    public Loader<List<AppEntry>> onCreateLoader( int i, Bundle bundle ) {


        Log.d(TAG, "Start AppListLoader");

        start = System.currentTimeMillis();

        return new AppListLoader(getActivity());

    }

    @Override
    public void onLoadFinished( Loader<List<AppEntry>> listLoader, List<AppEntry> appEntries ) {


        // Set the new data in the adapter.
        mAdapter.setData(appEntries);

        if ( BuildConfig.DEBUG ) {
            long end = System.currentTimeMillis();
            Log.d(TAG, "Finished   " + String.valueOf((end - start) / 1000.0));
        }

        if ( isResumed() ) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }

        mCallback.onAppLoaderFinished();

        mFinished = true;

    }

    @Override
    public void onLoaderReset( Loader<List<AppEntry>> listLoader ) {
        // Clear the data in the adapter.
        mAdapter.clear();
    }

    // Searchview implementation
    @Override
    public boolean onQueryTextSubmit( String s ) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onQueryTextChange( String newText ) {


        // Called when the action bar search text has changed.  Since this
        // is a simple array adapter, we can just have it do the filtering.
        String curFilter = !TextUtils.isEmpty(newText) ? newText : null;
        if ( mFinished ) { mAdapter.getFilter().filter(curFilter); }
        return true;
    }

    @Override
    public boolean onClose() {
        if ( !TextUtils.isEmpty(mSearchView.getQuery()) ) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }


    // The container Activity must implement this interface so the frag can deliver messages
    public interface IAppListCallback {

        /**
         * Called by AppListFragment when a list item is selected
         */
        public void onAppSelected( AppEntry app );

        public void onAppLoaderFinished();

    }


}
