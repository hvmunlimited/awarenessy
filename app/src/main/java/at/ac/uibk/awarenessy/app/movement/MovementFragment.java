/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import at.ac.uibk.awarenessy.app.utils.Log;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.phoneprovider.LatLngWrapper;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.utils.FacebookUtils;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.Call;
import at.ac.uibk.awarenessy.dao.CallDao;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.SimpleLocation;
import at.ac.uibk.awarenessy.dao.SimpleLocationDao;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.dao.SmsDao;

/**
 * Main Entry point for the movement feature.
 * <p/>
 * Shows a movement profile of the user.
 */
public class MovementFragment extends ModuleFragment implements SeekBar.OnSeekBarChangeListener,
        AdapterView.OnItemSelectedListener, GoogleMap.OnMyLocationChangeListener, GoogleMap.OnMyLocationButtonClickListener {

    public static final  String       FRAGMENT_TAG     = MovementFragment.class.getName();
    private static final String       TAG              = MovementFragment.class.getSimpleName();
    private final        List<Marker> mLocationMarkers = new ArrayList<Marker>();


    // ------------------------------ FIELDS ------------------------------
    private final List<Marker> mCallMarkers = new ArrayList<Marker>();
    private final List<Marker> mSmsMarkers  = new ArrayList<Marker>();
    private final List<Marker> mFbMarkers   = new ArrayList<Marker>();
    private GoogleMap         mMap;
    private CallDao           mCallDao;
    private SmsDao            mSmsDao;
    private SimpleLocationDao mSimpleLocationDao;
    private Marker            mLastMarker;
    // UI Elements
    private View              mTimeBarContainer;
    private SeekBar           mTimeBar;
    private ToggleButton      mSmsToggle;
    private ToggleButton      mCallToggle;
    private ToggleButton      mFbToggle;
    private int mTaskCounter = 0;

    private int mCurrentLeft   = 0;
    private int mCurrentTop    = 180;
    private int mCurrentRight  = 0;
    private int mCurrentBottom = 0;
    private ProgressBar   mProgressBar;
    private LatLngWrapper mLatLngWrapper;
    private View          mMapHeader;
    private MapView       mMapView;

    // ------------------------ INTERFACE METHODS ------------------------


// --------------------- Interface OnItemSelectedListener ---------------------

    @Override
    public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {

        if ( !checkReady() ) {
            return;
        }

        mMap.clear();

        // map is empty, so remove old references to possible markers
        mLastMarker = null;
        mLocationMarkers.clear();

        mLatLngWrapper = new LatLngWrapper();

        // time which defines the earliest info on the map
        long time = getTimeForPeriod(( String ) parent.getItemAtPosition(position));

        new FetchLocationsTask(getActivity()).execute(time);
        new FetchCallsTask().execute(time);
        new FetchSmsTask().execute(time);
        new FetchFbPlacesTask().execute(time);

    }

    @Override
    public void onNothingSelected( AdapterView<?> parent ) {

        // Do nothing.

    }

// --------------------- Interface OnSeekBarChangeListener ---------------------

    @Override
    public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser ) {

        if ( fromUser ) {
            highlightMarker(progress);
        }

    }

    @Override
    public void onStartTrackingTouch( SeekBar seekBar ) {

        // Do nothing.

    }

    @Override
    public void onStopTrackingTouch( SeekBar seekBar ) {

        // Do nothing.

    }

    @Override
    /**
     * Show an info dialog, what the usage and purpose of this Fragment/Module is.
     * <p/>
     * Consists of a simple title, string and an ok button.
     */
    public void showInfoDialog() {

        // Build a new Dialog to tell the user to restart the phone
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.movement_profile);

        builder.setMessage(R.string.movement_dialog_info);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                // Restore preferences
                SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                        .PREF_FLAGS.PREF_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_MOVEMENT_INFO, false);


                // Commit the edits!
                editor.apply();
            }
        });

        builder.show();
    }

// -------------------------- OTHER METHODS --------------------------

    /**
     * Draw locations of the given list of sms on the map. Every location get a marker and a info
     * window containing the sender or recipient and the time of the sms.
     *
     * @param smsList List of {@link at.ac.uibk.awarenessy.dao.Call}s to be drawn on the map
     */
    private void drawSms( List<Sms> smsList ) {

        if ( !checkReady() ) {
            return;
        }

        // should marker be visible
        boolean visible = mSmsToggle.isChecked();

        mSmsMarkers.clear();

        Log.d(TAG, "SmsList size " + smsList.size());


        // iterate through tracking points
        for ( Sms sms : smsList ) {

            // get each location associated with this tracking point
            SimpleLocation location = sms.getSimpleLocation();

            if ( location == null ) {
                continue;
            }

            MarkerOptions newMarker = new MarkerOptions();

            if ( sms.getSent() ) {
                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable
                                                                            .ic_map_pin_sms_sent));
            } else {
                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_sms_rec));
            }


            newMarker.anchor(0f, 1f);
            newMarker.infoWindowAnchor(0.5f, 0f);

            LatLng latLng = location.getLatLng();
            newMarker.position(latLng);


            // display name in title
            String name = SmsInterface.getName(sms.getNumber(), getActivity());
            newMarker.title(name);

            newMarker.visible(visible);

            newMarker.snippet(Utils.DATE_FORMAT.format(sms.getTime()));


            Marker marker = mMap.addMarker(newMarker);
            mSmsMarkers.add(marker);
            // only add to wrapper when visible
            if ( visible ) {
                mLatLngWrapper.add(marker.getPosition());
            }
        }


    }

    /**
     * Adjust the map padding to show the maps functions (Zoom Buttons, Google logo, ...) on the
     * correct place. If the timebar is invisible (no markers on map), the map functions can
     * consume the entire container. If not, show the map functions above the visible timebar.
     * <p/>
     * Same thing works on the upper padding where different screen sizes have different map
     * headers. So the top padding will also be adjusted.
     */
    private void adjustPadding() {

        int paddingBottom = 0;
        int paddingTop = 0;


        if ( mTimeBarContainer.getVisibility() == View.VISIBLE ) {
            paddingBottom += mTimeBarContainer.getHeight();
            paddingBottom += mTimeBarContainer.getPaddingTop();
            paddingBottom += mTimeBarContainer.getPaddingBottom();
        }

        if ( mMapHeader.getVisibility() == View.VISIBLE ) {
            paddingTop += mMapHeader.getHeight();
            paddingTop += mMapHeader.getPaddingTop();
            paddingTop += mMapHeader.getPaddingBottom();
        }


        animatePadding(mCurrentLeft, paddingTop, mCurrentRight, paddingBottom);

    }

    /**
     * Animate map from the current paddings (mCurrentLeft, mCurrentTop, mCurrentRight,
     * mCurrentBottom) to the specified to-positions. Use a {@link android.view.animation
     * .OvershootInterpolator} to get a overshooting animation.
     *
     * @param toLeft   left padding of the map after animation
     * @param toTop    top padding of the map after animation
     * @param toRight  right padding of the map after animation
     * @param toBottom bottom padding of the map after animation
     */
    private void animatePadding( final int toLeft, final int toTop, final int toRight,
                                 final int toBottom ) {

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1000;

        if ( !checkReady() ) {
            return;
        }

        final Interpolator interpolator = new OvershootInterpolator();

        final int startLeft = mCurrentLeft;
        final int startTop = mCurrentTop;
        final int startRight = mCurrentRight;
        final int startBottom = mCurrentBottom;

        Log.d(TAG, "left " + mCurrentLeft + " top " + mCurrentTop + " right " + mCurrentRight + "" +
                " bottom " + mCurrentBottom);


        mCurrentLeft = toLeft;
        mCurrentTop = toTop;
        mCurrentRight = toRight;
        mCurrentBottom = toBottom;

        Log.d(TAG, "left " + mCurrentLeft + " top " + mCurrentTop + " right " + mCurrentRight + "" +
                " bottom " + mCurrentBottom);

        handler.post(new Runnable() {

            @Override
            public void run() {

                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation(( float ) elapsed / duration);

                int left = ( int ) (startLeft + ((toLeft - startLeft) * t));
                int top = ( int ) (startTop + ((toTop - startTop) * t));
                int right = ( int ) (startRight + ((toRight - startRight) * t));
                int bottom = ( int ) (startBottom + ((toBottom - startBottom) * t));

                mMap.setPadding(left, top, right, bottom);

                if ( elapsed < duration ) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }

            }

        });

    }

    /**
     * Draw list of locations of calls on the map. Every location get a marker and a info window
     * containing the caller or callee and the start time of this call.
     *
     * @param calls List of {@link at.ac.uibk.awarenessy.dao.Call}s to be drawn on the map
     */
    private void drawCalls( List<Call> calls ) {

        if ( !checkReady() ) {
            return;
        }

        // should marker be visible
        boolean visible = mCallToggle.isChecked();

        mCallMarkers.clear();

        Log.d(TAG, "Calls size " + calls.size());

        // iterate through tracking points
        for ( Call call : calls ) {


            // get each location associated with this tracking point
            SimpleLocation location = call.getSimpleLocation();

            // if no location is associated, don't draw it on map
            if ( location == null ) {
                continue;
            }


            MarkerOptions newMarker = new MarkerOptions();

            if ( call.getOutgoing() ) {
                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable
                                                                            .ic_map_pin_call_out));

            } else {
                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_call_in));
            }


            newMarker.anchor(0f, 1f);
            newMarker.infoWindowAnchor(0.5f, 0f);


            LatLng latLng = location.getLatLng();
            newMarker.position(latLng);


            // display name in title
            String name = SmsInterface.getName(call.getNumber(), getActivity());
            newMarker.title(name);

            newMarker.visible(visible);


            long start = call.getStartTime();
            long end = call.getEndTime();
            String duration = Utils.getFormattedDurationString(end - start, TimeUnit.MILLISECONDS);

            // add start and duration of the call
            newMarker.snippet(Utils.DATE_FORMAT.format(start) + " - " + duration);

            Marker marker = mMap.addMarker(newMarker);
            mCallMarkers.add(marker);
            // only add to wrapper when visible
            if ( visible ) {
                mLatLngWrapper.add(marker.getPosition());
            }
        }


    }

    private void drawFbPlaces( List<FbPlace> fbPlaces ) {


        if ( !checkReady() ) {
            return;
        }

        Log.d(TAG, "drawing " + fbPlaces.size() + " places");

        // should marker be visible
        boolean visible = mFbToggle.isChecked();

        mFbMarkers.clear();

        // iterate through tracking points
        for ( FbPlace place : fbPlaces ) {


            MarkerOptions newMarker = new MarkerOptions();


            newMarker.anchor(0f, 1f);
            newMarker.infoWindowAnchor(0.5f, 0f);


            LatLng latLng = place.getLatLng();
            newMarker.position(latLng);


            // display name in title
            String name = place.getName();
            newMarker.title(name);

            newMarker.visible(visible);

            newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin_fb));

            String address = place.getAddress();

            if ( address != null && !address.isEmpty() ) {
                newMarker.snippet(address);
            }

            Marker marker = mMap.addMarker(newMarker);
            mFbMarkers.add(marker);
            // only add to wrapper when visible
            if ( visible ) {
                mLatLngWrapper.add(marker.getPosition());
            }
        }

    }

    /**
     * When the map is not ready the CameraUpdateFactory cannot be used. This should be called on
     * all entry points that call methods on the Google Maps API.
     */

    private boolean checkReady() {

        if ( mMap == null ) {
            Toast.makeText(getActivity(), R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    /**
     * Draw line from the first list element to the last element. Every tracking point will be a
     * vertex on the line.
     *
     * @param locations list of locations to draw a line through
     */
    private void drawLines( List<SimpleLocation> locations ) {

        if ( !checkReady() ) {
            return;
        }


        if ( locations.isEmpty() ) {
            return;
        }

        PolylineOptions options = new PolylineOptions();

        for ( SimpleLocation loc : locations ) {
            options.add(loc.getLatLng());
        }

        options.color(R.color.map_line);
        options.width(8);

        mMap.addPolyline(options);

    }

    private List<Call> calcCalls( long time ) {


        return mCallDao.queryBuilder().where(CallDao.Properties.StartTime.gt
                (time), CallDao.Properties.LocationId.isNotNull()).orderDesc(CallDao.Properties
                                                                                     .StartTime)
                .list();

    }

    private List<Sms> calcSmsList( long time ) {


        return mSmsDao.queryBuilder().where(SmsDao.Properties.Time.gt
                (time), SmsDao.Properties.LocationId.isNotNull()).orderDesc(SmsDao.Properties
                                                                                    .Time).list();
    }


    /**
     * Draw list of locations on the map. Every location get a marker and a info window containg
     * the
     * time of that location and, if found, the nearest address. Add every drawn marker in the
     * mLocationMarkers list to edit it later (select/unselect).
     *
     * @param locations List of {@link at.ac.uibk.awarenessy.dao.SimpleLocation}s to be drawn
     *                  on the map
     */
    private void drawLocations( List<SimpleLocation> locations ) {

        if ( !checkReady() ) {
            return;
        }

        if ( locations.isEmpty() ) {
            return;
        }

        mLocationMarkers.clear();

        // iterate through locations
        for ( SimpleLocation location : locations ) {


            MarkerOptions newMarker = new MarkerOptions();
            LatLng latLng = location.getLatLng();
            newMarker.position(latLng);


            // add every latLng to LatLngBounds adjust the map later to get all markers in view
            mLatLngWrapper.add(latLng);

            newMarker.title(Utils.DATE_FORMAT.format(new Date(location.getTime())));
            String address = location.getAddress();

            if ( address != null && !address.isEmpty() ) {
                newMarker.snippet(address);
            }

            newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_ic_unselected));
            newMarker.anchor(0.5f, 0.5f);

            // fill list of drawn markers to edit them later
            mLocationMarkers.add(mMap.addMarker(newMarker));

        }


    }

    private long getTimeForPeriod( String period ) {


        if ( period.equals(getString(R.string.last_hour)) ) {
            return System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1);
        } else if ( period.equals(getString(R.string.last_24_hours)) ) {
            return System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1);
        } else if ( period.equals(getString(R.string.last_7_days)) ) {
            return System.currentTimeMillis() - TimeUnit.DAYS.toMillis(7);
        } else if ( period.equals(getString(R.string.last_30_days)) ) {
            return System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30);
        } else if ( period.equals(getString(R.string.last_year)) ) {
            return System.currentTimeMillis() - TimeUnit.DAYS.toMillis(365);
        } else if ( period.equals(getString(R.string.all)) ) {
            return 0;
        } else {
            Log.e(TAG, "Error setting period with name " + period);
            throw new IllegalStateException("Error setting period with name " + period);
        }


    }

    /**
     * Get all locations that where tracked after the given time stamp.
     *
     * @param time the timestamp for the earliest location
     *
     * @return a list of locations after the given time stamp
     */
    private List<SimpleLocation> calcLocations( long time ) {

        return mSimpleLocationDao.queryBuilder().where(SimpleLocationDao.Properties
                                                               .Time.gt(time),
                                                       SimpleLocationDao.Properties
                                                               .IsTrackingPoint.eq(true)
        ).orderAsc
                (SimpleLocationDao.Properties.Time).list();


    }

    /**
     * Highlight the marker at the specified position (change color and center camera) and show its
     * InfoWindow containing its time and address. The last highlighted marker will be reseted, so
     * that at any given time, only one marker will be highlighted.
     *
     * @param position position of the marker in the markers list
     */
    private void highlightMarker( int position ) {

        if ( !checkReady() ) {
            return;
        }

        // check legal position
        if ( position >= mLocationMarkers.size() || position < 0 ) {
            return;
        }

        // reset last visited marker
        if ( mLastMarker != null ) {
            mLastMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_ic_unselected));
            mLastMarker.hideInfoWindow();
        }


        // corresponding marker to timeline position
        Marker marker = mLocationMarkers.get(position);


        Log.d(TAG, marker.getPosition().toString());

        // highlight current marker
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_ic_selected));
        marker.showInfoWindow();

        // focus camera
        CameraUpdate newCamera = CameraUpdateFactory.newLatLng(marker.getPosition());
        mMap.animateCamera(newCamera);


        // set current marker as last marker to reset it when another marker is selected
        mLastMarker = marker;

    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);

        MapsInitializer.initialize(getActivity());

        setUpDb();

    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        View rootView = inflater.inflate(R.layout.movement_activity_map, container, false);

        mMapView = ( MapView ) rootView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);


        mMapHeader = rootView.findViewById(R.id.map_header);

        mProgressBar = ( ProgressBar ) rootView.findViewById(R.id.progressBar);

        mTimeBarContainer = rootView.findViewById(R.id.time_seek_bar_container);
        mTimeBar = ( SeekBar ) rootView.findViewById(R.id.time_seek_bar);
        mTimeBar.setOnSeekBarChangeListener(this);

        // control time period
        Spinner mPeriodSpinner = ( Spinner ) rootView.findViewById(R.id.period_spinner);

        mSmsToggle = ( ToggleButton ) rootView.findViewById(R.id.toggle_sms);
        mCallToggle = ( ToggleButton ) rootView.findViewById(R.id.toggle_calls);
        mFbToggle = ( ToggleButton ) rootView.findViewById(R.id.toggle_fb);

        // set unchecked (not disabled, still want to listen to clicks)
        if ( !FacebookUtils.isLoggedIn() ) {
            mFbToggle.setChecked(false);
        }

        boolean isTablet = Utils.isTablet(getActivity());

        // hide buttons if device has no phone function (tablet)
        if ( isTablet ) {
            mSmsToggle.setVisibility(View.GONE);
            mCallToggle.setVisibility(View.GONE);
        }

        MapToggleOnClickListener toggleListener = new MapToggleOnClickListener();
        mSmsToggle.setOnCheckedChangeListener(toggleListener);
        mCallToggle.setOnCheckedChangeListener(toggleListener);
        mFbToggle.setOnCheckedChangeListener(toggleListener);


        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                                                             R.array.periods_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mPeriodSpinner.setAdapter(adapter);

        mPeriodSpinner.setOnItemSelectedListener(this);

        // set 'last hour' time interval
        mPeriodSpinner.setSelection(0);

        SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                .PREF_FLAGS
                                                                                .PREF_NAME,
                                                                        Context.MODE_PRIVATE
        );
        boolean showInfo = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_MOVEMENT_INFO, true);

        if ( showInfo ) {
            showInfoDialog();
        }

        setUpMapIfNeeded();

        return rootView;

    }


    private void setUpDb() {

        DaoSession daoSession = Utils.getDaoSession(getActivity());
        mCallDao = daoSession.getCallDao();
        mSmsDao = daoSession.getSmsDao();
        mSimpleLocationDao = daoSession.getSimpleLocationDao();
    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {

        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.map_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {

        switch ( item.getItemId() ) {


            case R.id.action_change_map_layer:

                if ( checkReady() ) {
                    toggleMapType();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onMyLocationButtonClick() {

        // show dialog to activate location (same as Google Maps)
        if(!LocationUtils.checkIfLocationIsOn(getActivity())){
            LocationUtils.showLocationAlert(getActivity(), false);
        }

        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    /**
     * Toggle the map type between {@link com.google.android.gms.maps.GoogleMap#MAP_TYPE_NORMAL}
     * and
     * {@link com.google.android.gms.maps.GoogleMap#MAP_TYPE_HYBRID}.
     */
    private void toggleMapType() {

        int mapType = mMap.getMapType();


        if ( mapType == GoogleMap.MAP_TYPE_NORMAL ) {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if ( mapType == GoogleMap.MAP_TYPE_HYBRID ) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else {
            Log.e(TAG, "Error toggling map type. Map has illegal map type");
        }

    }

    @Override
    public void onResume() {

        super.onResume();
        mMapView.onResume();

        setUpMapIfNeeded();

    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only
     * ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     */
    private void setUpMapIfNeeded() {

        // Do a null check to confirm that we have not already instantiated the map.
        if ( mMap == null ) {
            // Try to obtain the map from the MovementFragment.
            mMap = mMapView.getMap();
            // Check if we were successful in obtaining the map.
            if ( mMap != null ) {
                setUpMap();
            } else {
                // Couldn't obtain the map, hide UI Elements
                mMapHeader.setVisibility(View.INVISIBLE);
                mTimeBarContainer.setVisibility(View.INVISIBLE);


            }
        }

    }

    /**
     * After calling this, it is save to manipulate the map. This should only be called once and
     * when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

        Log.d(TAG, "left " + mCurrentLeft + " top " + mCurrentTop + " right " + mCurrentRight + "" +
                " bottom " + mCurrentBottom);
        mMap.setPadding(mCurrentLeft, mCurrentTop, mCurrentRight, mCurrentBottom);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(this);
        mMap.setOnMyLocationButtonClickListener(this);

    }

    @Override
    public void onStart() {

        super.onStart();

        if ( !LocationUtils.checkIfLocationIsOn(getActivity()) ) {
            LocationUtils.showLocationAlert(getActivity(), false);
        }

    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState( Bundle outState ) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    /**
     * show or hide the time bar at the bottom of the screen
     *
     * @param show true if the time bar should be shown, else false
     */

    private void showTimeBar( boolean show ) {

        if ( show ) {
            mTimeBarContainer.setVisibility(View.VISIBLE);
        } else {
            mTimeBarContainer.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onMyLocationChange( Location lastKnownLocation ) {

        Log.d(TAG, "MyLocationChange");

        // do nothing when there are markers added
        if ( mLatLngWrapper != null && !mLatLngWrapper.isEmpty() ) {
            return;
        }
        CameraUpdate myLoc = CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder().target(new LatLng(lastKnownLocation.getLatitude(),
                                                               lastKnownLocation.getLongitude()))
                        .zoom(14).build()
        );
        mMap.animateCamera(myLoc);
        mMap.setOnMyLocationChangeListener(null);
    }

    /**
     * Center the map so that all markers can be seen.
     */
    private void adjustBounds() {

        // only move camera when at least 2 points are on the map
        if ( mLatLngWrapper == null || mLatLngWrapper.getSize() < 2 ) {
            return;
        }

        LatLngBounds bounds = mLatLngWrapper.build();

        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));


    }

    private class MapToggleOnClickListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged( CompoundButton button, boolean isChecked ) {


            switch ( button.getId() ) {

                case R.id.toggle_calls:
                    if ( isChecked ) {

                        for ( Marker callMarker : mCallMarkers ) {
                            callMarker.setVisible(true);
                            mLatLngWrapper.add(callMarker.getPosition());
                        }

                    } else {
                        for ( Marker callMarker : mCallMarkers ) {
                            callMarker.setVisible(false);
                            mLatLngWrapper.remove(callMarker.getPosition());

                        }
                    }
                    break;


                case R.id.toggle_sms:

                    if ( isChecked ) {

                        for ( Marker smsMarker : mSmsMarkers ) {
                            smsMarker.setVisible(true);
                            mLatLngWrapper.add(smsMarker.getPosition());
                        }

                    } else {
                        for ( Marker smsMarker : mSmsMarkers ) {
                            smsMarker.setVisible(false);
                            mLatLngWrapper.remove(smsMarker.getPosition());
                        }
                    }

                    break;

                case R.id.toggle_fb:

                    if ( FacebookUtils.isLoggedIn() ) {


                        if ( isChecked ) {

                            for ( Marker fbMarker : mFbMarkers ) {
                                fbMarker.setVisible(true);
                                mLatLngWrapper.add(fbMarker.getPosition());
                            }

                        } else {
                            for ( Marker fbMarker : mFbMarkers ) {
                                fbMarker.setVisible(false);
                                mLatLngWrapper.remove(fbMarker.getPosition());
                            }
                        }

                    } else {
                        // suppress activate button and show hint to login
                        button.setChecked(false);
                        Toast.makeText(getActivity(), getString(R.string.toast_map_fb_login),
                                       Toast.LENGTH_LONG).show();

                    }

                    break;
            }

            adjustBounds();


        }
    }

    /**
     * Subclass of AsyncTask that fetches Places that the user has been tagged since the given Long
     * UnixTime.
     */
    private class FetchFbPlacesTask extends AsyncTask<Long, Void, Void> {

        private final List<FbPlace> mFbPlaces = new ArrayList<FbPlace>();

        @Override
        protected void onPreExecute() {

            mProgressBar.setVisibility(View.VISIBLE);
            mTaskCounter++;

        }


        @Override
        protected Void doInBackground( Long... params ) {

            Log.d(TAG, "Start FetchFbPlacesTask" + Thread.currentThread().getName());


            long time = params[0];
            final Date earliest = new Date(time);

            Session activeSession = Session.getActiveSession();
            new Request(
                    activeSession,
                    "/me/locations",
                    null,
                    HttpMethod.GET,
                    new Request.Callback() {
                        public void onCompleted( Response response ) {
            /* handle the result */

                            Log.d(TAG, "Got results: " + response.toString());

                            GraphObject graphObject = response.getGraphObject();


                            if ( graphObject != null ) {

                                JSONArray data = ( JSONArray ) graphObject.getProperty("data");

                                if ( data != null ) {


                                    Log.d(TAG, "Num of Locations: " + data.length());

                                    for ( int i = 0 ; i < data.length() ; i++ ) {
                                        try {
                                            JSONObject object = data.getJSONObject(i);
                                            Log.d(TAG, i + " " + object.toString());

                                            String created_time = object.getString("created_time");

                                            Date date = FacebookUtils.SIMPLE_DATE_FORMAT.parse
                                                    (created_time);

                                            if ( date.before(earliest) ) {
                                                return;
                                            } else {
                                                JSONObject place = object.getJSONObject("place");

                                                FbPlace fbPlace = new FbPlace(place);

                                                mFbPlaces.add(fbPlace);


                                                Log.d(TAG, "Got place: " + fbPlace.toString());

                                            }
                                        } catch ( JSONException e ) {
                                            e.printStackTrace();
                                        } catch ( ParseException e ) {
                                            e.printStackTrace();
                                        }
                                    }

                                    // get next page
                                    Request requestForPagedResults = response
                                            .getRequestForPagedResults
                                                    (Response.PagingDirection.NEXT);

                                    if ( requestForPagedResults != null ) {

                                        requestForPagedResults.setCallback(this);

                                        requestForPagedResults.executeAndWait();

                                    } else {
                                        Log.d(TAG, "End of places pagination");

                                    }


                                }
                            }

                        }
                    }
            ).executeAndWait();

            return null;
        }

        @Override
        protected void onPostExecute( Void aVoid ) {


            Log.d(TAG, "OnPost FetchFbPlacesTask start" + Thread.currentThread().getName());

            drawFbPlaces(mFbPlaces);

            if ( --mTaskCounter == 0 ) {
                mProgressBar.setVisibility(View.INVISIBLE);
                adjustBounds();
            }

            Log.d(TAG, "OnPost FetchFbPlacesTask finish");

        }
    }

    private class FetchSmsTask extends AsyncTask<Long, Void, List<Sms>> {

        @Override
        protected void onPreExecute() {

            mProgressBar.setVisibility(View.VISIBLE);
            mTaskCounter++;


        }

        @Override
        protected List<Sms> doInBackground( Long... params ) {

            Log.d(TAG, "Start FetchSmsTask" + Thread.currentThread().getName());


            long time = params[0];

            List<Sms> smsList = calcSmsList(time);


            Log.d(TAG, "Finish FetchSmsTask");
            return smsList;
        }


        @Override
        protected void onPostExecute( List<Sms> smsList ) {


            Log.d(TAG, "OnPost FetchSmsTask start" + Thread.currentThread().getName());

            drawSms(smsList);

            if ( --mTaskCounter == 0 ) {
                mProgressBar.setVisibility(View.INVISIBLE);
                adjustBounds();
            }

            Log.d(TAG, "OnPost FetchSmsTask finish");

        }
    }

    private class FetchCallsTask extends AsyncTask<Long, Void, List<Call>> {


        @Override
        protected void onPreExecute() {

            mProgressBar.setVisibility(View.VISIBLE);
            mTaskCounter++;

        }

        @Override
        protected List<Call> doInBackground( Long... params ) {

            Log.d(TAG, "Start FetchCallsTask" + Thread.currentThread().getName());

            long time = params[0];

            List<Call> calls = calcCalls(time);

            Log.d(TAG, "Finish FetchCallsTask");

            return calls;
        }

        @Override
        protected void onPostExecute( List<Call> calls ) {

            Log.d(TAG, "OnPost FetchCallsTask start" + Thread.currentThread().getName());


            drawCalls(calls);


            if ( --mTaskCounter == 0 ) {
                mProgressBar.setVisibility(View.INVISIBLE);
                adjustBounds();
            }

            Log.d(TAG, "OnPost FetchCallsTask finish");

        }
    }

    private class FetchLocationsTask extends AsyncTask<Long, Void, List<SimpleLocation>> {

        private final Context mContext;

        public FetchLocationsTask( Context context ) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {

            mProgressBar.setVisibility(View.VISIBLE);
            mTaskCounter++;

        }

        @Override
        protected List<SimpleLocation> doInBackground( Long... params ) {

            Log.d(TAG, "Start FetchLocationsTask" + Thread.currentThread().getName());

            long time = params[0];

            // get all locations for the selected period
            List<SimpleLocation> locations = calcLocations(time);

            Log.d(TAG, "Finish FetchLocationsTask");

            return locations;
        }


        @Override
        protected void onPostExecute( List<SimpleLocation> locations ) {

            Log.d(TAG, "OnPost FetchLocationsTask start" + Thread.currentThread().getName());

            if ( !locations.isEmpty() ) {


                drawLocations(locations);
                drawLines(locations);

                mTimeBar.setMax(locations.size() - 1);
                mTimeBar.setProgress(mTimeBar.getMax());


                // if only one location is stored, don't show the bar
                showTimeBar(locations.size() > 1);
            } else {
                Toast.makeText(mContext, getString(R.string.empty_history),
                               Toast.LENGTH_SHORT).show();
                showTimeBar(false);


            }

            if ( --mTaskCounter == 0 ) {
                mProgressBar.setVisibility(View.INVISIBLE);
                adjustBounds();


            }


            Log.d(TAG, "OnPost FetchLocationsTask finish");

            adjustPadding();

        }
    }

}
