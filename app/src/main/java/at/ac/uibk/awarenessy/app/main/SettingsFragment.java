/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.main;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;

import at.ac.uibk.awarenessy.app.utils.Log;

import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.utils.LogActivity;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.CallDao;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.SimpleLocation;
import at.ac.uibk.awarenessy.dao.SimpleLocationDao;
import at.ac.uibk.awarenessy.dao.SmsDao;
import at.ac.uibk.awarenessy.dao.Table;
import au.com.bytecode.opencsv.CSVWriter;
import de.greenrobot.dao.query.QueryBuilder;

/**
 * {@link android.preference.PreferenceFragment} which gives the user the option to change data
 * stored by the app. E.g. stored Logs, stored locations, ...
 */
public class SettingsFragment extends android.preference.PreferenceFragment implements Preference
        .OnPreferenceClickListener {

    public static final  int    ERR_WRITE_FILE                       = -1;
    private static final String TAG                                  = SettingsFragment.class
            .getSimpleName();
    private static final String PREF_KEY_LOGS                        = "pref_key_logs";
    private static final String PREF_KEY_DB_EXPORT                   = "pref_key_db_export";
    private static final String PREF_KEY_DELETE_LOCATION_HISTORY_ALL =
            "pref_delete_location_history_all";
    private static final String PREF_KEY_DELETE_LOCATION_HISTORY_OLD =
            "pref_delete_location_history_old";
    private List<String> mSelectedItems;
    private File         mPath;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // if running on KitKat, store Logs into Documents folder instead of Downloads
        File parentDir;
        if (Utils.hasKitKat()) {
            parentDir = Environment.getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOCUMENTS);
        } else {
            parentDir = Environment.getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOWNLOADS);
        }

        // check if external storage is available and writable
        boolean storageMounted = Utils.isExternalStorageWritable();


        // subfolder to store logs
        mPath = new File(parentDir, "Awarenessy_Logs");


        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        Preference buttonLogs = findPreference(PREF_KEY_LOGS);
        buttonLogs.setOnPreferenceClickListener(this);

        Preference buttonExportDb = findPreference(PREF_KEY_DB_EXPORT);
        buttonExportDb.setOnPreferenceClickListener(this);


        // if no storage available, disable export function
        if (!storageMounted) {
            buttonExportDb.setEnabled(false);
            buttonExportDb.setSelectable(false);
            String msg = getString(R.string.err_no_export) + " " + getString(R.string
                    .err_no_sd_card);
            buttonExportDb.setSummary(msg);
        } else {
            // set correct path
            buttonExportDb.setSummary(getString(R.string.pref_description_export,
                    mPath.getAbsoluteFile()));

        }

        Preference buttonDeleteHistoryAll = findPreference(PREF_KEY_DELETE_LOCATION_HISTORY_ALL);
        buttonDeleteHistoryAll.setOnPreferenceClickListener(this);

        Preference buttonDeleteHistoryOld = findPreference(PREF_KEY_DELETE_LOCATION_HISTORY_OLD);
        buttonDeleteHistoryOld.setOnPreferenceClickListener(this);
    }


    @Override
    public boolean onPreferenceClick(Preference preference) {

        String s = preference.getKey();

        // execute action according to the clicked preference
        if (s.equals(PREF_KEY_LOGS)) {
            Intent logIntent = new Intent(getActivity(), LogActivity.class);
            startActivity(logIntent);
            return true;

        } else if (s.equals(PREF_KEY_DB_EXPORT)) {
            showDBExportDialog();
            return true;
        } else if (s.equals(PREF_KEY_DELETE_LOCATION_HISTORY_ALL)) {
            showDeleteDialog(true);
            return true;

        } else if (s.equals(PREF_KEY_DELETE_LOCATION_HISTORY_OLD)) {

            showDeleteDialog(false);
            return true;

        } else {
            return false;
        }
    }

    private void showDBExportDialog() {

        // String array containing table which can be exported
        final String[] tables;

        // don't allow exporting non existing SMS and Call tables
        if (!Utils.isTablet(getActivity())) {
            tables = getResources().getStringArray(R.array.db_tables);
        } else {
            tables = getResources().getStringArray(R.array.db_tables_no_phone);
        }

        // item which should be exported when clicking OK in dialog
        mSelectedItems = new ArrayList<String>();


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Set the dialog title
        builder.setTitle(getString(R.string.dialog_title_export_db));

        // Set the action buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                Log.d(TAG, "Ok, exporting " + mSelectedItems.toString());


                ExportDbToCSVTask<Table> task = new ExportDbToCSVTask<Table>(getActivity());
                task.execute(mSelectedItems.toArray(new String[mSelectedItems.size()]));


            }
        });


        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                mSelectedItems = null;

            }
        });

        // Specify the list array, the tables to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
        builder.setMultiChoiceItems(tables, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    // If the user checked the item, add it to the selected items
                    mSelectedItems.add(tables[which]);
                } else if (mSelectedItems.contains(tables[which])) {
                    // Else, if the item is already in the array, remove it
                    mSelectedItems.remove(tables[which]);

                }
            }
        });


        builder.show();


    }


    private void showDeleteDialog(final boolean deleteAll) {
        // Build a new Dialog to let the user confirm the deletion of the search history.
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(android.R.drawable.ic_dialog_alert);

        SimpleLocationDao locationDao = Utils.getDaoSession(getActivity())
                .getSimpleLocationDao();


        String title;
        String msg;
        long numDelete;
        QueryBuilder<SimpleLocation> where = locationDao.queryBuilder().where(SimpleLocationDao
                .Properties.IsTrackingPoint.eq(true));
        final long last30d = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30);

        // calc number of deleting locations
        if (deleteAll) {
            numDelete = where.count();
            title = getString(R.string.pref_title_delete_locations_all);
            msg = getString(R.string.pref_dialog_delete_locations_all, numDelete);
        } else {
            numDelete = where.where(SimpleLocationDao.Properties.Time.le(last30d)).count();
            title = getString(R.string.pref_title_delete_locations_old);
            msg = getString(R.string.pref_dialog_delete_locations_old, numDelete);
        }



        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SimpleLocationDao locationDao = Utils.getDaoSession(getActivity())
                        .getSimpleLocationDao();

                // delete rows
                if (deleteAll) {
                    locationDao.deleteAll();
                } else {
                    locationDao.queryBuilder().where(SimpleLocationDao.Properties.Time.le
                            (last30d)).buildDelete().executeDeleteWithoutDetachingEntities();
                }


                Toast.makeText(getActivity(), getString(R.string.location_history_deleted),
                        Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Location history deleted");
            }
        });

        // do nothing on cancel
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.show();
    }

    /**
     * Lock screen configuration until {@link at.ac.uibk.awarenessy.app.main
     * .SettingsFragment#unlockScreenOrientation()}
     * is called.
     */
    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;

        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else  {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    /**
     * Unlock screen configuration.
     */
    private void unlockScreenOrientation() {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    /**
     * Task that exports the database tables which are given as a String Array to {@link
     * SettingsFragment.ExportDbToCSVTask#execute(Object[])}.
     * {@link SettingsFragment.ProgressUpdate} will be passed
     * to {@link android.os.AsyncTask#onProgressUpdate(Object[])} and contains the current task
     * (export SMS, Calls or Positions) and the progress in percent. Return value is the number of
     * exported rows or -1 if an error occured.
     */
    private class ExportDbToCSVTask<T extends Table> extends AsyncTask<String, ProgressUpdate,
            Integer> {

        private final Context           mContext;
        private final ProgressDialog    mDialog;
        private       SmsDao            mSmsDao;
        private       CallDao           mCallDao;
        private       SimpleLocationDao mSimpleLocationDao;
        private       DaoSession        mDaoSession;

        private ExportDbToCSVTask(Context context) {
            super();
            mContext = context;
            mDialog = new ProgressDialog(mContext);
            mDialog.setTitle(context.getString(R.string.export_database_dialog_title));
            mDialog.setMessage(context.getString(R.string.take_a_while_message));
            mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mDialog.setCancelable(false);
            mDialog.show();

        }

        @Override
        protected void onPreExecute() {

            lockScreenOrientation();
            mDaoSession = Utils.getDaoSession(mContext);


        }

        @Override
        protected Integer doInBackground(String... params) {


            mPath.mkdirs();

            int exportedRows = 0;

            for (String tables : params) {


                String fileName = tables + ".csv";


                // copied file on the external storage
                File file = new File(mPath.getAbsolutePath(), fileName);

                try {

                    file.createNewFile();
                    CSVWriter csvWrite = new CSVWriter(new FileWriter(file), ';');

                    String task;

                    // Header of CSV file
                    String[] columns;
                    List<T> list;

                    if (tables.equals(getString(R.string.calls))) {

                        mCallDao = mDaoSession.getCallDao();
                        columns = mCallDao.getAllColumns();

                        task = mContext.getString(R.string.exp_calls);

                        // cast allowed, cause Dao-Entities has to extend Table and T extends Table
                        list = (List<T>) mCallDao.loadAll();

                    } else if (tables.equals(getString(R.string.sms))) {

                        mSmsDao = mDaoSession.getSmsDao();
                        columns = mSmsDao.getAllColumns();

                        task = mContext.getString(R.string.exp_sms);

                        // cast allowed, cause Dao-Entities has to extend Table and T extends Table
                        list = (List<T>) mSmsDao.loadAll();


                    } else if (tables.equals(getString(R.string.positions))) {

                        mSimpleLocationDao = mDaoSession.getSimpleLocationDao();
                        columns = mSimpleLocationDao.getAllColumns();

                        task = mContext.getString(R.string.exp_locations);

                        // cast allowed, cause Dao-Entities has to extend Table and T extends Table
                        list = (List<T>) mSimpleLocationDao.loadAll();


                    } else {
                        Log.e(TAG, "Illegal Checkbox checked: " + tables);
                        continue;
                    }


                    // write header
                    csvWrite.writeNext(columns);

                    int size = list.size();

                    // write empty message
                    if (list.isEmpty()) {
                        csvWrite.writeNext(new String[]{mContext.getString(R.string.empty_table)});
                    }

                    int i;
                    for (i = 0; i < list.size(); i++) {
                        T tuple = list.get(i);
                        String[] entry = tuple.toCSV();
                        csvWrite.writeNext(entry);

                        float progress = (i + 1) / (float) size * 100;
                        publishProgress(new ProgressUpdate(task, (int) progress));

                    }

                    exportedRows += i;

                    csvWrite.close();

                    // send intent to start media scanner that makes the file shown in Explorers
                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    intent.setData(Uri.fromFile(file));
                    mContext.sendBroadcast(intent);

                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                    return ERR_WRITE_FILE;
                }
            }

            return exportedRows;
        }

        @Override
        protected void onProgressUpdate(ProgressUpdate... values) {
            mDialog.setMessage(values[0].mTask);
            mDialog.setProgress(values[0].mProgress);
        }

        @Override
        protected void onPostExecute(Integer exportedRows) {


            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }

            if (exportedRows >= 0) {
                String msg = getResources().getQuantityString(R.plurals.exported_records,
                        exportedRows, exportedRows);
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            } else {

                int error = exportedRows;

                switch (error) {


                    case ERR_WRITE_FILE:
                        Toast.makeText(mContext, mContext.getString(R.string.err_export_failed)
                                , Toast.LENGTH_SHORT).show();
                        break;

                }

            }

            unlockScreenOrientation();

        }
    }

    private class ProgressUpdate {
        String mTask;
        int    mProgress;

        private ProgressUpdate(String mTask, int mProgress) {
            this.mTask = mTask;
            this.mProgress = mProgress;
        }
    }
}
