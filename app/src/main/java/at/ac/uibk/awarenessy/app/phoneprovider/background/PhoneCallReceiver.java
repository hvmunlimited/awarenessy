/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider.background;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import at.ac.uibk.awarenessy.app.utils.Log;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.Module;
import at.ac.uibk.awarenessy.app.phoneprovider.PhoneUtils;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.utils.LogFile;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.Call;

public class PhoneCallReceiver extends BroadcastReceiver {

    private static final String TAG = PhoneCallReceiver.class.getSimpleName();
    //The receiver will be recreated whenever android feels like it.  We need a static variable
    // to remember data between instantiations
    private static MyPhoneCallListener listener;
    private        Context             mContext;

    @Override
    public void onReceive( Context context, Intent intent ) {

        if ( !Module.CALL.isChecked(context) ) {
            Log.d(TAG, "Call Module turned off. Ignoring call");
            return;
        }

        mContext = context;
        if ( listener == null ) {
            listener = new MyPhoneCallListener();
        }

        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We
        // use it to get the number.
        if ( "android.intent.action.NEW_OUTGOING_CALL".equals(intent.getAction()) ) {
            listener.setOutgoingNumber(intent.getExtras().getString("android.intent.extra" +
                                                                            ".PHONE_NUMBER"));
            return;
        }

        //The other intent tells us the phone state changed.  Here we set a listener to deal with it
        TelephonyManager telephony = ( TelephonyManager ) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private void onIncomingCallStarted( String number, long start ) {

        String name = SmsInterface.getName(number, mContext);


        String msg = mContext.getString(R.string.log_incoming_call_start, name, number,
                                        Utils.DATE_FORMAT.format(start));
        Log.d(TAG, msg);
        LogFile.getInstance(mContext).log(LogFile.LOG_FILE_CALLS, msg);

    }

    private void onOutgoingCallStarted( String number, long start ) {

        String name = SmsInterface.getName(number, mContext);

        String msg = mContext.getString(R.string.log_outgoing_call_start, name, number,
                                        Utils.DATE_FORMAT.format(start));
        Log.d(TAG, msg);
        LogFile.getInstance(mContext).log(LogFile.LOG_FILE_CALLS, msg);

    }

    private void onIncomingCallEnded( String number, long start, long end ) {

        String name = SmsInterface.getName(number, mContext);

        // create formatted duration string
        String duration = Utils.getFormattedDurationString(end - start, TimeUnit.MILLISECONDS);

        String msg = mContext.getString(R.string.log_incoming_call_end, name, number,
                                        Utils.DATE_FORMAT.format(new Date(start)),
                                        Utils.DATE_FORMAT.format(new Date(end)), duration);
        Log.d(TAG, msg);
        LogFile.getInstance(mContext).log(LogFile.LOG_FILE_CALLS, msg);

        startCallLocationService(number, start, end, false);


    }


    private void onOutgoingCallEnded( String number, long start, long end ) {

        String name = SmsInterface.getName(number, mContext);

        // create formatted duration string
        String duration = Utils.getFormattedDurationString(end - start, TimeUnit.MILLISECONDS);

        String msg = mContext.getString(R.string.log_outgoing_call_end, name, number,
                                        Utils.DATE_FORMAT.format(new Date(start)),
                                        Utils.DATE_FORMAT.format(new Date(end)), duration);
        Log.d(TAG, msg);
        LogFile.getInstance(mContext).log(LogFile.LOG_FILE_CALLS, msg);

        startCallLocationService(number, start, end, true);


    }

    private void onMissedCall( String number, long start ) {

        String name = SmsInterface.getName(number, mContext);

        String msg = mContext.getString(R.string.log_missed_call, name, number,
                                        Utils.DATE_FORMAT.format(new Date(start)));
        Log.d(TAG, msg);
        LogFile.getInstance(mContext).log(LogFile.LOG_FILE_CALLS, msg);


    }

    private void startCallLocationService( String number, long start, long end,
                                           boolean isOutgoing ) {

        Intent intent = new Intent(mContext, InsertService.class);

        intent.setAction(PhoneUtils.ACTION_INSERT_CALL);

        Call call = new Call(null, number, start, end, isOutgoing, null);

        intent.putExtra(PhoneUtils.EXTRA_CALL, call);

        mContext.startService(intent);
    }


    //Deals with actual events
    public class MyPhoneCallListener extends PhoneStateListener {

        int lastState = TelephonyManager.CALL_STATE_IDLE;
        long    callStartTime;
        boolean isIncoming;
        String  savedNumber;  //because the passed incoming is only valid in ringing

        public MyPhoneCallListener() {

        }

        //The outgoing number is only sent via a separate intent, so we need to store it out of band
        public void setOutgoingNumber( String number ) {

            savedNumber = number;
        }

        //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered,
        // to IDLE when its hung up
        //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
        @Override
        public void onCallStateChanged( int state, String incomingNumber ) {

            super.onCallStateChanged(state, incomingNumber);
            if ( lastState == state ) {
                //No change, debounce extras
                return;
            }


            switch ( state ) {
                case TelephonyManager.CALL_STATE_RINGING:
                    isIncoming = true;
                    callStartTime = System.currentTimeMillis();
                    savedNumber = incomingNumber;
                    onIncomingCallStarted(incomingNumber, callStartTime);
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    //Transition of ringing->offhook are pickups of incoming calls.  Nothing donw
                    // on them
                    if ( lastState != TelephonyManager.CALL_STATE_RINGING ) {
                        isIncoming = false;
                        callStartTime = System.currentTimeMillis();
                        onOutgoingCallStarted(savedNumber, callStartTime);
                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    //Went to idle-  this is the end of a call.  What type depends on previous
                    // state(s)
                    if ( lastState == TelephonyManager.CALL_STATE_RINGING ) {
                        //Ring but no pickup-  a miss
                        onMissedCall(savedNumber, callStartTime);
                    } else if ( isIncoming ) {
                        onIncomingCallEnded(savedNumber, callStartTime, System.currentTimeMillis());
                    } else {
                        onOutgoingCallEnded(savedNumber, callStartTime, System.currentTimeMillis());
                    }
                    break;
            }
            lastState = state;
        }

    }


}