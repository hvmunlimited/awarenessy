/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.nfc.Tag;
import android.provider.ContactsContract;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.i18n.phonenumbers.PhoneNumberUtil;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.utils.FilterableArrayAdapter;
import at.ac.uibk.awarenessy.app.utils.ImageLoader;

import java.util.Locale;
import java.util.Map;

/**
 * Own ArrayAdapter implementation to show a Ranking of SMS in a list.
 * <p/>
 * Defines a own Layout for a List.
 * Uses sms_top_contact_list_item.xmlitem.xml as the layout, which displays the contact photo (if existing), the name (or number if no name exists), number of received and sent SMS.
 * <p/>
 * Use the {@link at.ac.uibk.awarenessy.app.utils.ImageLoader}ImageLoader implementation to load the contact photo.
 */
public class TopContactsAdapter extends FilterableArrayAdapter<Map.Entry<String, SmsInterface.SmsCount>> {

    private final Context     mContext;
    private final ImageLoader mImageLoader;


    static class ViewHolder {
        TextView  number;
        TextView  smsSent;
        TextView  smsReceived;
        ImageView icon;
        TextView  name;
    }


    public TopContactsAdapter(Context context, ImageLoader imageLoader) {
        super(context, R.layout.sms_top_contact_list_item);

        mContext = context;
        mImageLoader = imageLoader;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View topContactView = convertView;

        Map.Entry<String, SmsInterface.SmsCount> contact = getItem(position);


        Uri photoUri = null;

        String number = contact.getKey();

        // stored Contact name or number
        String name = SmsInterface.getName(number, mContext);



        SmsInterface.SmsCount count = contact.getValue();
        int contactId = SmsInterface.getContactId(number, mContext);

        if (contactId != -1) {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI,
                    contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo
                    .CONTENT_DIRECTORY);

        }

        if (topContactView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            topContactView = inflater.inflate(R.layout.sms_top_contact_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) topContactView.findViewById(R.id.name);
            viewHolder.number = (TextView) topContactView.findViewById(R.id.number);
            viewHolder.smsSent = (TextView) topContactView.findViewById(R.id.smsSent);
            viewHolder.smsReceived = (TextView) topContactView.findViewById(R.id.smsReceived);
            viewHolder.icon = (ImageView) topContactView.findViewById(android.R.id.icon);
            topContactView.setTag(viewHolder);

        }

        ViewHolder holder = (ViewHolder) topContactView.getTag();


        if (!name.equals(number)) {
            holder.number.setText(number);
        } else {
            holder.number.setText("");
        }

        holder.name.setText(name);

        holder.smsReceived.setText(String.valueOf(count.received));
        holder.smsSent.setText(String.valueOf(count.sent));


        // Loads the thumbnail image pointed to by photoUri into the ImageView in a
        // background worker thread
        mImageLoader.loadImage(photoUri, holder.icon);


        // Returns the item layout view
        return topContactView;
    }

    @Override
    protected boolean filterObject(Map.Entry<String, SmsInterface.SmsCount> myObject,
                                   String constraint) {

        String name = SmsInterface.getName(myObject.getKey(), mContext).toLowerCase(Locale
                .getDefault());


        return name.contains(constraint);

    }
}
