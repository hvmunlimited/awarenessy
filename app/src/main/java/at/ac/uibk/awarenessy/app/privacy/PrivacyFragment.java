/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.privacy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.facebook.widget.LoginButton;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.Module;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.utils.FacebookUtils;
import at.ac.uibk.awarenessy.app.utils.Log;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;

/**
 * Module Fragment that gives the user the possibility to activate or deactivate tracking functions
 * of the app. Shows for each Tracking Function (Track SMS, track calls, ...) a description and a
 * {@link android.widget.ToggleButton} to turn function on/off. Uses
 * {@link at.ac.uibk.awarenessy.app.main.Module} to simplify reading and
 * setting flags in SharedPreferences
 * {@link at.ac.uibk.awarenessy.app.utils.Preferences.PREF_FLAGS}.
 */
public class PrivacyFragment extends ModuleFragment implements CompoundButton
        .OnCheckedChangeListener, View.OnClickListener {


    public static final String FRAGMENT_TAG = PrivacyFragment.class.getName();
    private static final String TAG = PrivacyFragment.class.getSimpleName();
    private Switch mSmsStatSwitch;
    private Switch mPositionSwitch;
    private Switch mCallSwitch;
    private LoginButton mLoginButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                        .PREF_FLAGS
                        .PREF_NAME,
                Context.MODE_PRIVATE
        );

        boolean showInfo = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_PRIVACY_INFO, true);

        if (showInfo) {
            showInfoDialog();
        }


        for (Module module : Module.values()) {

            Log.d(TAG, module.getPrefName() + " " + module.isChecked(getActivity()));

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.privacy_fragment, container, false);

        LinearLayout sms = (LinearLayout) rootView.findViewById(R.id.container_sms_stat);
        LinearLayout call = (LinearLayout) rootView.findViewById(R.id.container_calls);
        LinearLayout position = (LinearLayout) rootView.findViewById(R.id.container_position);
        LinearLayout fb = (LinearLayout) rootView.findViewById(R.id.container_fb);

        boolean isTablet = Utils.isTablet(getActivity());

        // hide on tablets
        if (isTablet) {

            // hide container
            sms.setVisibility(View.GONE);
            call.setVisibility(View.GONE);

            // hide dividers
            rootView.findViewById(R.id.div_sms_calls).setVisibility(View.GONE);
            rootView.findViewById(R.id.div_calls_position).setVisibility(View.GONE);
        }


        mSmsStatSwitch = (Switch) rootView.findViewById(R.id.switch_sms_stat);
        mCallSwitch = (Switch) rootView.findViewById(R.id.switch_calls);
        mPositionSwitch = (Switch) rootView.findViewById(R.id.switch_position);

        mLoginButton = (LoginButton) rootView.findViewById(R.id.fb_login_button);
        mLoginButton.setReadPermissions(FacebookUtils.PERMS);

        mSmsStatSwitch.setChecked(Module.SMS.isChecked(getActivity()));
        mCallSwitch.setChecked(Module.CALL.isChecked(getActivity()));
        mPositionSwitch.setChecked(Module.POSITION.isChecked(getActivity()));

        mSmsStatSwitch.setOnCheckedChangeListener(this);
        mCallSwitch.setOnCheckedChangeListener(this);
        mPositionSwitch.setOnCheckedChangeListener(this);

        sms.setOnClickListener(this);
        call.setOnClickListener(this);
        position.setOnClickListener(this);

        // no internet, disable fb login
        if (!Utils.isNetworkAvailable(getActivity())) {
            mLoginButton.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), R.string.err_no_internet_fb, Toast.LENGTH_SHORT).show();
        } else {
            // only react on click on container when internet and button is available
            fb.setOnClickListener(this);
        }

        return rootView;

    }


    @Override
    public void showInfoDialog() {

        // Build a new Dialog to tell the user to restart the phone
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.privacy);

        builder.setMessage(R.string.privacy_dialog_info);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Restore preferences
                SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                        .PREF_FLAGS.PREF_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_PRIVACY_INFO, false);


                // Commit the edits!
                editor.apply();
            }
        });

        builder.show();


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {

            case R.id.switch_sms_stat:

                onSmsStatClick(isChecked);
                break;

            case R.id.switch_calls:

                onCallClick(isChecked);
                break;
            case R.id.switch_position:

                onPositionClick(isChecked);
                break;


        }

    }

    private void onCallClick(boolean isChecked) {
        Log.d(TAG, Module.CALL.toString() + ": " + isChecked);

        String toastMsg;

        if (isChecked) {
            toastMsg = getString(R.string.toast_start_read_calls);
        } else {
            toastMsg = getString(R.string.toast_stop_read_calls);
        }

        Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();

        Module.CALL.setChecked(isChecked, getActivity());


    }

    private void onPositionClick(boolean isChecked) {

        Log.d(TAG, Module.POSITION.toString() + ": " + isChecked);

        String toastMsg;
        Intent backgroundLocationIntent;

        if (isChecked) {

            if (!Utils.servicesConnected(getActivity())) {
                mPositionSwitch.setChecked(false);
                return;
            }

            // Check if Location is enabled
            if (!LocationUtils.checkIfLocationIsOn(getActivity())) {

                // Show Settings to enable Location and let user reactivate position tracking
                LocationUtils.showLocationAlert(getActivity(), false);

                // can't tell if user activated location or not, so let the user reactivate awarenessys positioning again
                mPositionSwitch.setChecked(false);
                return;
            }
            toastMsg = getString(R.string.toast_start_loc_tracking);
            backgroundLocationIntent = new Intent(LocationUtils.ACTION_START_GPS_TRACKING);


        } else {
            toastMsg = getString(R.string.toast_stop_loc_tracking);
            backgroundLocationIntent = new Intent(LocationUtils.ACTION_STOP_GPS_TRACKING);
        }

        getActivity().sendBroadcast(backgroundLocationIntent);

        Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();

        Module.POSITION.setChecked(isChecked, getActivity());


    }

    private void onSmsStatClick(boolean isChecked) {

        Log.d(TAG, Module.SMS.toString() + ": " + isChecked);

        String toastMsg;

        if (isChecked) {


            toastMsg = getString(R.string.toast_start_save_sms);


            // put current time to import only sms since turning module on
            SharedPreferences prefData = getActivity().getSharedPreferences(Preferences
                            .APP_DATA
                            .PREF_NAME,
                    Context.MODE_PRIVATE
            );
            SharedPreferences.Editor editor = prefData.edit();
            editor.putLong(Preferences.APP_DATA.LAST_SENT_SMS_SYNC, System.currentTimeMillis());
            editor.apply();

        } else {

            toastMsg = getString(R.string.toast_stop_save_sms);


        }


        Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();

        Module.SMS.setChecked(isChecked, getActivity());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.container_sms_stat:

                mSmsStatSwitch.performClick();
                break;

            case R.id.container_calls:

                mCallSwitch.performClick();
                break;

            case R.id.container_position:

                mPositionSwitch.performClick();
                break;

            case R.id.container_fb:

                mLoginButton.performClick();
                break;

        }

    }
}
