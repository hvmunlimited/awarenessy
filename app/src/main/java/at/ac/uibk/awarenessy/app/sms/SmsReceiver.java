/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import at.ac.uibk.awarenessy.app.utils.Log;

import at.ac.uibk.awarenessy.app.main.Module;
import at.ac.uibk.awarenessy.app.phoneprovider.PhoneUtils;
import at.ac.uibk.awarenessy.app.phoneprovider.background.InsertService;

/**
 * Broadcast receiver that listens for incoming messages and insert them into the sms database.
 * <p/>
 * Note: Displays a Toast on incoming sms. This is not necessary for the app itself. Only a nice
 * feature.
 */
public class SmsReceiver extends BroadcastReceiver {

    private static final String SMS_EXTRA_NAME = "pdus";

    private static final String TAG = SmsReceiver.class.getSimpleName();


    @Override
    public void onReceive( Context context, Intent intent ) {

        if ( !Module.SMS.isChecked(context) ) {
            Log.d(TAG, "SMS Module turned off. Ignoring message");
            return;
        }

        // Get SMS map from Intent
        Bundle extras = intent.getExtras();

        // Get Action of this intent (Sent or Received SMS)
        String action = intent.getAction();


        if ( action == null || !action.equals(SmsInterface.ACTION_SMS_RECEIVED) ) {
            Log.d(TAG, "No Action for this Receiver found!");
            return;
        }


        if ( extras != null ) {

            // Get received/sent SMS array
            Object[] smsExtra = ( Object[] ) extras.get(SMS_EXTRA_NAME);


            // check if there is a sms
            if ( smsExtra.length > 0 ) {

                // start Service to insert Sms
                Intent startServiceIntent = new Intent(context, InsertService.class);
                startServiceIntent.setAction(PhoneUtils.ACTION_INSERT_SMS);
                startServiceIntent.putExtra(PhoneUtils.EXTRA_SMS, smsExtra);

                context.startService(startServiceIntent);


            }

        }


    }


}
