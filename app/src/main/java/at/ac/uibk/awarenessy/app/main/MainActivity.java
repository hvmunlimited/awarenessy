/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import at.ac.uibk.awarenessy.app.BuildConfig;
import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.checkapps.AppDetailActivity;
import at.ac.uibk.awarenessy.app.checkapps.AppDetailFragment;
import at.ac.uibk.awarenessy.app.checkapps.AppListFragment;
import at.ac.uibk.awarenessy.app.checkapps.CheckAppsFragment;
import at.ac.uibk.awarenessy.app.checkapps.model.AppEntry;
import at.ac.uibk.awarenessy.app.datausage.DataFragment;
import at.ac.uibk.awarenessy.app.main.adapter.NavDrawerListAdapter;
import at.ac.uibk.awarenessy.app.main.model.NavDrawerItem;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.movement.MovementFragment;
import at.ac.uibk.awarenessy.app.overview.OverviewFragment;
import at.ac.uibk.awarenessy.app.phoneprovider.ProviderFragment;
import at.ac.uibk.awarenessy.app.privacy.PrivacyFragment;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.sms.SmsLoader;
import at.ac.uibk.awarenessy.app.sms.SmsStatisticFragment;
import at.ac.uibk.awarenessy.app.utils.FacebookUtils;
import at.ac.uibk.awarenessy.app.utils.Log;
import at.ac.uibk.awarenessy.app.utils.LogFile;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.Call;
import at.ac.uibk.awarenessy.dao.CallDao;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.dao.SmsDao;

/**
 * Launcher activity of the app.
 * <p/>
 * Display a navigation drawer as the main navigation of the app.
 * <p/>
 * Load new sent messages on start and insert them into the app's database.
 */
public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<List<Sms>>,
        AppListFragment.IAppListCallback {

    public static final  int                    RECEIVED_LOADER      = 0;
    public static final  int                    SENT_LOADER          = 1;
    public static final  int                    SENT_REFRESH_LOADER  = 2;
    private static final int                    DELAY_MILLIS_TO_EXIT = 3000;
    private static final String                 TAG                  = MainActivity.class
            .getSimpleName();
    private static final String                 DRAWER_TITLE         = "drawer_title";
    private final        Session.StatusCallback callback             =
            new Session.StatusCallback() {
                @Override
                public void call( Session session,
                                  SessionState state, Exception exception ) {

                    onSessionStateChange(session);


                }
            };
    private DrawerLayout          mDrawerLayout;
    private ListView              mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    // nav drawer title
    private CharSequence          mDrawerTitle;
    // used to store app title
    private CharSequence          mTitle;
    // slide menu items
    private String[]              navMenuTitles;
    private boolean doubleBackToExitPressedOnce = false;
    private UiLifecycleHelper uiHelper;
    private boolean           mIsResumed;
    private ProgressDialog    mUpdateSmsDialog;

    /**
     * Called when the state of the facebook connection/session changes
     *
     * @param session the current session
     */
    private void onSessionStateChange( Session session ) {
        // Only make changes if the activity is visible
        if ( mIsResumed ) {


            SharedPreferences pref = getSharedPreferences(Preferences.APP_DATA
                                                                  .PREF_NAME, MODE_PRIVATE);
            final SharedPreferences.Editor editor = pref.edit();


            if ( session.isOpened() ) {
                // make request to the /me API and show welcome message
                Request.newMeRequest(session, new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted( GraphUser user, Response response ) {
                        if ( user != null ) {

                            FragmentManager fragmentManager = getFragmentManager();
                            Fragment fragment = fragmentManager.findFragmentById(R.id
                                                                                         .content_frame);

                            if ( fragment != null && fragment instanceof OverviewFragment ) {

                                Log.d(TAG, "Commit");
//
                                // we are logged in, so start task
                                (( OverviewFragment ) fragment).startFBTask();


                            }

                            String firstName = user.getFirstName();
                            String lastName = user.getLastName();

                            String welcome = getString(R.string.fb_welcome, firstName, lastName);


                            editor.putString(Preferences.APP_DATA.FB_FIRST_NAME, firstName);
                            editor.putString(Preferences.APP_DATA.FB_LAST_NAME, lastName);

                            editor.commit();

                            Toast.makeText(MainActivity.this, welcome, Toast.LENGTH_LONG).show();
                            Log.d(TAG, "Logged in as " + firstName + " " + lastName);

                        }
                    }
                }).executeAsync();


            } else if ( session.isClosed() ) {

                editor.remove(Preferences.APP_DATA.FB_FIRST_NAME);
                editor.remove(Preferences.APP_DATA.FB_LAST_NAME);
                editor.commit();
            }

        }
    }


    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);
        getActionBar().setIcon(R.drawable.ic_menu_launcher);

        setContentView(R.layout.main_layout);

        // Helper that manages Facebook login/sessions
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);


        // set Preferences defaults (this is only called on the very first start)
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);


        // Restore preferences
        final SharedPreferences prefFlags = this.getSharedPreferences(Preferences.PREF_FLAGS
                                                                              .PREF_NAME,
                                                                      MODE_PRIVATE
        );

        final boolean firstStart = prefFlags.getBoolean(Preferences.PREF_FLAGS.FIRST_START, true);
        final boolean drawerTeached = prefFlags.getBoolean(Preferences.PREF_FLAGS.DRAWER_TEACHED,
                                                           false);

        final boolean monitorSMS = Module.SMS.isChecked(this);

        final boolean isTablet = Utils.isTablet(this);
        final boolean phoneFunction = Utils.hasPhoneFunction(this);

        int versionUpdated = prefFlags.getInt(Preferences.PREF_FLAGS.VERSION_UPDATE, 0);

        // update version when the app is started for the first time
        if ( firstStart ) {
            versionUpdated = BuildConfig.VERSION_CODE;
            prefFlags.edit().putInt(Preferences.PREF_FLAGS.VERSION_UPDATE, versionUpdated).commit();
        }

        Log.d(TAG, "FirstStart: " + firstStart);
        Log.d(TAG, "drawerTeached: " + drawerTeached);
        Log.d(TAG, "monitorSMS " + monitorSMS);
        Log.d(TAG, "Tablet " + isTablet);
        Log.d(TAG, "PhoneFunction " + phoneFunction);


        mTitle = mDrawerTitle = getTitle();

        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        TypedArray navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = ( DrawerLayout ) findViewById(R.id.drawer_layout);
        mDrawerList = ( ListView ) findViewById(R.id.list_slidermenu);


        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        // Overview
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1),
                                             true));
        // Dangerous Apps
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1),
                                             true));
        // SMS Statistic
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1),
                                             !isTablet));
        // Movement Profile
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1),
                                             true));
        // Phone Provider Data
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1),
                                             !isTablet));
        // Data Usage
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1),
                                             true));
        // Privacy
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1),
                                             true));

        // show hint that some functions will not work on tablets
        if ( Utils.isTablet(this) ) {
            navDrawerItems.add(new NavDrawerItem(getString(R.string.nav_drawer_no_phone), -1,
                                                 false));
        }

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        NavDrawerListAdapter adapter = new NavDrawerListAdapter(getApplicationContext(),
                                                                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        if ( getActionBar() != null ) {
            // enabling action bar app icon and behaving it as toggle button
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                                                  //nav menu toggle icon
                                                  R.string.app_name,
                                                  // nav drawer open - description for accessibility
                                                  R.string.app_name // nav drawer close -
                                                  // description for accessibility
        ) {
            public void onDrawerClosed( View view ) {

                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();

                // fetch boolean again, to get current value
                boolean drawerTeached = prefFlags.getBoolean(Preferences.PREF_FLAGS.DRAWER_TEACHED,
                                                             false);

                // hide Tutorial Layout if the user closed the drawer (knows how to open it)
                if ( !drawerTeached ) {
                    prefFlags.edit().putBoolean(Preferences.PREF_FLAGS.DRAWER_TEACHED,
                                                true).commit();

                    Log.d(TAG, "Drawer teached, User closed it");

                }

            }

            public void onDrawerOpened( View drawerView ) {


                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if ( savedInstanceState == null ) {
            startAppModule(0);


            Log.d(TAG, "Device has Phone Function: " + phoneFunction);

            if ( phoneFunction ) {


                // check if the sms numbers has to be updated (new number format)
                if ( !firstStart && versionUpdated < 6 ) {

                    // delete old sms and insert every new
                    SmsDao smsDao = Utils.getDaoSession(this).getSmsDao();
                    smsDao.deleteAll();

                    // init loading
                    getLoaderManager().initLoader(RECEIVED_LOADER, null, this);
                    getLoaderManager().initLoader(SENT_LOADER, null, this);


                }


            }


        } else {
            String title = savedInstanceState.getString(DRAWER_TITLE);
            setTitle(title);
            Log.d(TAG, "Title: " + title);
        }

        if ( monitorSMS ) {

            boolean smsSentImported = prefFlags.getBoolean(Preferences.PREF_FLAGS
                                                                   .SMS_SENT_IMPORTED, false);
            boolean smsRecImported = prefFlags.getBoolean(Preferences.PREF_FLAGS
                                                                  .SMS_REC_IMPORTED, false);

            Log.d(TAG, "Sent: " + smsSentImported + " Rec: " + smsRecImported);

            // import only all SMS if not imported yet (will be set in OnLoaderFinished)
            if ( !smsSentImported || !smsRecImported ) {
                if ( !smsSentImported ) {

                    getLoaderManager().initLoader(SENT_LOADER, null, this);

                }

                if ( !smsRecImported ) {

                    getLoaderManager().initLoader(RECEIVED_LOADER, null, this);

                }
            } else {
                getLoaderManager().initLoader(SENT_REFRESH_LOADER, null, this);
            }
        }


        if ( firstStart ) {
            showStartDialog(isTablet);
        }


        // show how to open drawer in a overlay view
        if ( !drawerTeached ) {

            mDrawerLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }, 500);


        }


    }


    private void showStartDialog( final boolean tablet ) {

        // Restore preferences
        final SharedPreferences prefFlags = getSharedPreferences(Preferences.PREF_FLAGS.PREF_NAME,
                                                                 MODE_PRIVATE);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.start_aw_title);

        // Inflate layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View dialogView = getLayoutInflater().inflate(R.layout.start_exp_dialog, null);

        TextView changeText = ( TextView ) dialogView.findViewById(R.id.how_to_change_privacy);
        changeText.setText(getString(R.string.start_aw_change_data, getString(R.string.privacy)));


        builder.setView(dialogView);

        // modify look of the login button to fit better in the dialog
        LoginButton login = ( LoginButton ) dialogView.findViewById(R.id.fb_login_button);
        login.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        login.setPadding(login.getPaddingLeft(), 0, login.getPaddingRight(),
                         0); // remove top and bottom

        login.setReadPermissions(FacebookUtils.PERMS);

        // no internet, disable fb login
        if ( !Utils.isNetworkAvailable(this) ) {
            login.setVisibility(View.INVISIBLE);
        }


        builder.setPositiveButton(R.string.start_aw_ok,
                                  new DialogInterface.OnClickListener() {


                                      @Override
                                      public void onClick( DialogInterface dialog, int which ) {
                                          Log.d(TAG, "Start");

                                          // don't show dialog again
                                          prefFlags.edit().putBoolean(Preferences.PREF_FLAGS
                                                                              .FIRST_START,
                                                                      false
                                          ).commit();

                                          startPositionTracking();

                                          // don't change something on tablets
                                          if ( !tablet ) {
                                              startSmsMonitoring();
                                              startCallHistory();
                                          }


                                      }
                                  }
        );

        builder.setNegativeButton(getString(R.string.start_later),
                                  new DialogInterface.OnClickListener() {


                                      @Override
                                      public void onClick( DialogInterface dialog, int which ) {
                                          // don't show dialog again
                                          prefFlags.edit().putBoolean(Preferences.PREF_FLAGS
                                                                              .FIRST_START,
                                                                      false
                                          ).commit();
                                      }
                                  }
        );

        // user has to explicitly say yes or no
        builder.setCancelable(false);

        builder.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        mIsResumed = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
        mIsResumed = false;
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putString(DRAWER_TITLE, mTitle.toString());
        Log.d(TAG, "mTitle: " + mTitle + " mDrawerTitle" + mDrawerTitle);
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }


    private void startCallHistory() {

        // change only boolean in SharedPreferences to enable receiver
        Module.CALL.setChecked(true, this);

    }

    private void startSmsMonitoring() {

        Module.SMS.setChecked(true, this);

        // init loading
        getLoaderManager().initLoader(RECEIVED_LOADER, null, this);
        getLoaderManager().initLoader(SENT_LOADER, null, this);

    }

    public void startPositionTracking() {

        // Check if Location is enabled
        if ( !LocationUtils.checkIfLocationIsOn(this) ) {

            // Show Settings to enable Location and tell the user to reactivate positions in privacy
            LocationUtils.showLocationAlert(this, true);

            return;
        }

        // Check for Google Play services
        if ( !Utils.servicesConnected(this) ) {
            return;
        }

        Intent backgroundLocationIntent = new Intent(LocationUtils
                                                             .ACTION_START_GPS_TRACKING);

        sendBroadcast(backgroundLocationIntent);

        Module.POSITION.setChecked(true, this);

    }


    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        // toggle nav drawer on selecting action bar app icon/title
        if ( mDrawerToggle.onOptionsItemSelected(item) ) {
            return true;
        }
        // Handle action bar actions click
        switch ( item.getItemId() ) {
            case R.id.action_settings:

                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu( Menu menu ) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

        showMenuItems(menu, !drawerOpen);

        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Show/hide all menu items.
     *
     * @param menu     Menu which items should be shown/hided
     * @param visibile true if all menu items should be visible, false otherwise
     */
    private void showMenuItems( Menu menu, boolean visibile ) {

        for ( int i = 0 ; i < menu.size() ; i++ ) {
            MenuItem m = menu.getItem(i);
            m.setVisible(visibile);
        }
    }

    @Override
    public void setTitle( CharSequence title ) {

        mTitle = title;
        if ( getActionBar() != null ) {
            getActionBar().setTitle(mTitle);
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during onPostCreate() and
     * onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate( Bundle savedInstanceState ) {

        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged( Configuration newConfig ) {

        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public Loader<List<Sms>> onCreateLoader( int id, Bundle args ) {

        Log.d(TAG, "Start SmsLoader " + id);

        switch ( id ) {
            case RECEIVED_LOADER:
                return new SmsLoader(this, SmsInterface.SmsType.RECEIVED, 0);


            case SENT_LOADER:
                return new SmsLoader(this, SmsInterface.SmsType.SENT, 0);

            case SENT_REFRESH_LOADER:

                SharedPreferences prefData = this.getSharedPreferences(Preferences
                                                                               .APP_DATA.PREF_NAME,
                                                                       Context.MODE_PRIVATE
                );
                long lastSync = prefData.getLong(Preferences.APP_DATA.LAST_SENT_SMS_SYNC, 0);

                Log.d(TAG, "Last Sms sync: " + new Date(lastSync));

                return new SmsLoader(this, SmsInterface.SmsType.SENT, lastSync);

            default:
                throw new IllegalArgumentException("Illegal Loader ID: " + id);

        }


    }

    @Override
    public void onLoadFinished( Loader<List<Sms>> loader, List<Sms> data ) {

        Log.d(TAG, "loader " + loader.toString() + " finished: " + data.size());

        Collections.sort(data, Sms.TIME_COMPARATOR);


        DaoSession daoSession = Utils.getDaoSession(this);
        SmsDao smsDao = daoSession.getSmsDao();

        if ( data.size() > 0 ) {
            smsDao.insertOrReplaceInTx(data);
        }

        // only log sms since the installation and ignore importing sms ( = SENT_LOADER &
        // RECEIVED_LOADER)
        if ( loader.getId() == SENT_REFRESH_LOADER ) {
            for ( Sms sms : data ) {
                // use timestamp of the message
                LogFile.getInstance(this).log(LogFile.LOG_FILE_SMS, sms.toStringLog(this),
                                              new Date(sms.getTime()));
            }
        } else {

            // set flag that sms has been imported
            SharedPreferences prefFlags = getSharedPreferences(Preferences.PREF_FLAGS.PREF_NAME,
                                                               MODE_PRIVATE);
            SharedPreferences.Editor editor = prefFlags.edit();


            String msg;
            if ( loader.getId() == SENT_LOADER ) {
                Log.d(TAG, "Sent finished");
                msg = getString(R.string.sms_sent_imported, data.size());
                editor.putBoolean(Preferences.PREF_FLAGS.SMS_SENT_IMPORTED, true);
            } else {
                Log.d(TAG, "Rec finished");
                msg = getString(R.string.sms_rcv_imported, data.size());
                editor.putBoolean(Preferences.PREF_FLAGS.SMS_REC_IMPORTED, true);
            }

            editor.commit();


            LogFile.getInstance(this).log(LogFile.LOG_FILE_SMS, msg);
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }

        Log.d(TAG, "Inserted " + data.size() + " Sms into Db");

        // save current time into shared preferences
        SharedPreferences prefData = getSharedPreferences(Preferences.APP_DATA.PREF_NAME,
                                                          MODE_PRIVATE);
        SharedPreferences.Editor editor = prefData.edit();
        editor.putLong(Preferences.APP_DATA.LAST_SENT_SMS_SYNC, System.currentTimeMillis());
        editor.commit();



    }

    @Override
    public void onLoaderReset( Loader<List<Sms>> loader ) {
        Log.d(TAG, "Loader " + loader + " reset");
    }

    /**
     * Displaying fragment view for selected nav drawer list item
     */
    private void startAppModule( int position ) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        String tag = null;
        switch ( position ) {
            case 0:
                fragment = new OverviewFragment();
                tag = OverviewFragment.FRAGMENT_TAG;
                break;
            case 1:
                fragment = new CheckAppsFragment();
                tag = CheckAppsFragment.FRAGMENT_TAG;
                break;
            case 2:
                fragment = new SmsStatisticFragment();
                tag = SmsStatisticFragment.FRAGMENT_TAG;
                break;

            case 3:

                fragment = new MovementFragment();
                tag = MovementFragment.FRAGMENT_TAG;
                break;


            case 4:

                fragment = new ProviderFragment();
                tag = ProviderFragment.FRAGMENT_TAG;

                break;

            case 5:


                fragment = new DataFragment();
                tag = DataFragment.FRAGMENT_TAG;

                break;

            case 6:

                fragment = new PrivacyFragment();
                tag = PrivacyFragment.FRAGMENT_TAG;

                break;
            default:
                Log.wtf(TAG, "Unknown module clicked");
                break;
        }

        if ( fragment != null ) {
            FragmentManager fragmentManager = getFragmentManager();

            // we navigate to a new module, so clear eventually pushed fragments from the backstack
//            while (fragmentManager.getBackStackEntryCount() > 0) {
//                fragmentManager.popBackStack();
//            }

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.content_frame, fragment, tag);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.commit();


            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);

            // if home is selected show app name
            if ( position == 0 ) {
                setTitle(mDrawerTitle);
            } else {
                setTitle(navMenuTitles[position]);
            }
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    /**
     * Required Implementation of Callback Method for the {@link at.ac.uibk.awarenessy.app
     * .checkapps.AppListFragment.IAppListCallback}.
     * <p/>
     * If in two pane view (On Large Screen Devices) the {@link at.ac.uibk.awarenessy.app
     * .checkapps.AppDetailFragment} will be updated, else the {@link AppListFragment] will be
     * exchanged with the AppDetailFragment.
     *
     * @param app The App to display
     */
    @Override
    public void onAppSelected( AppEntry app ) {

        // The user selected an App from the AppListFragment

        FragmentManager fm = getFragmentManager();

        // Capture the AppDetailFragment from the activity layout
        AppListFragment appListFragment = ( AppListFragment ) fm.findFragmentById(R.id
                                                                                          .app_list_container);

        if ( appListFragment != null && appListFragment.isInTwoPane() ) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putParcelable(AppDetailFragment.ARG_PACKAGE, app);

            AppDetailFragment fragment = ( AppDetailFragment ) fm.findFragmentByTag
                    (AppDetailFragment.FRAGMENT_TAG);


            if ( fragment != null ) {
                fragment.updateAppView(app);
            }

        } else {

            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, AppDetailActivity.class);
            detailIntent.putExtra(AppDetailFragment.ARG_PACKAGE, app);
            startActivity(detailIntent);

        }


    }

    /**
     * Required Implementation of Callback Method for the {@link at.ac.uibk.awarenessy.app
     * .checkapps.AppListFragment.IAppListCallback}.
     * <p/>
     * Called when the all apps has been loaded.
     * <p/>
     * If in two pane view hide the progess indicator and show the usage hint instead
     */
    @Override
    public void onAppLoaderFinished() {

        AppDetailFragment appDetailFragment = ( AppDetailFragment ) getFragmentManager()
                .findFragmentByTag(AppDetailFragment.FRAGMENT_TAG);

        if ( appDetailFragment != null ) {
            appDetailFragment.showProgressContainer(false);
            appDetailFragment.showUsageHint(true);
        }
    }

    @Override
    public void onBackPressed() {

        /*
        * User has to click the back button twice to exit the app.
        * The second click has to occur within a specific delay.
         */
        if ( doubleBackToExitPressedOnce ) {
            super.onBackPressed();
            return;
        }

        // if we have opened another fragment, close fragment instead of showing exit msg
        FragmentManager fragmentManager = getFragmentManager();
        if ( fragmentManager.getBackStackEntryCount() > 0 ) {
            getFragmentManager().popBackStack();
            return;
        }

        // we are in the top most navigation layer and pressed for the first time
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.exit_hint), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                doubleBackToExitPressedOnce = false;

            }
        }, DELAY_MILLIS_TO_EXIT);
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements ListView.OnItemClickListener {

        private int oldPosition = -1;

        @Override
        public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {

            // display view for selected nav drawer item
            if ( position != oldPosition ) {
                oldPosition = position;
                startAppModule(position);
            }

            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }




}
