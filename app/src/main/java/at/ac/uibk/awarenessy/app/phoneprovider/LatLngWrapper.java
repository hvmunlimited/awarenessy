/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class that enriches the functionality of
 * {@link com.google.android.gms.maps.model.LatLngBounds.Builder} with remove, getSize and isEmpty
 * methods.
 */
public class LatLngWrapper {


    private final List<LatLng> mLatLngList = new ArrayList<LatLng>();

    /**
     * Add the given position to the boundaries.
     *
     * @param latLng LatLng to add
     */
    public void add(LatLng latLng) {
        mLatLngList.add(latLng);
    }

    /**
     * Remove the given position from the boundaries.
     *
     * @param latLng LatLng to remove
     */
    public void remove(LatLng latLng) {
        mLatLngList.remove(latLng);
    }

    /**
     * Build bounds.
     *
     * @return LatLngBounds that can be used with update camera methods
     */
    public LatLngBounds build() {

        LatLngBounds.Builder builder = LatLngBounds.builder();


        for (LatLng latLng : mLatLngList) {
            builder.include(latLng);
        }

        return builder.build();

    }

    /**
     * Return the amount of boundaries.
     *
     * @return size of the boundaries
     */
    public int getSize() {

        return mLatLngList.size();
    }

    /**
     * Check if bounds exists.
     *
     * @return true if there are boundaries, else false
     */
    public boolean isEmpty() {
        return mLatLngList.isEmpty();
    }
}
