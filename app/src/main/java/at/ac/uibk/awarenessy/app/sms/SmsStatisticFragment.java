/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.app.*;
import android.content.*;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.SearchRecentSuggestions;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;

import at.ac.uibk.awarenessy.app.BuildConfig;
import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.sms.adapter.TopContactsAdapter;
import at.ac.uibk.awarenessy.app.sms.search.SearchSmsActivity;
import at.ac.uibk.awarenessy.app.sms.search.SmsSuggestionProvider;
import at.ac.uibk.awarenessy.app.utils.ImageLoader;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.Sms;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SmsStatisticFragment extends ModuleFragment implements AdapterView
        .OnItemClickListener, SearchView.OnQueryTextListener {


    public static final  String FRAGMENT_TAG                      = SmsStatisticFragment.class
            .getName();
    public static final  String EXTRA_NUMBER                      = "at.ac.uibk.mobileflow.NUMBER";
    private static final int    DELAY_HIDE_CONTACTS_SEARCH_MILLIS = 1000;
    private static final String TAG                               = SmsStatisticFragment.class
            .getSimpleName();
    //TODO: Use in Settings Later
    private static final int    TOP_RECEIVED_CONTACTS             = 10;

    private TopContactsAdapter mAdapter;
    private ImageLoader        mImageLoader;

    private TextView mOverallView;
    private TextView mOverallSentView;
    private TextView mOverallReceivedView;
    private TextView mLastMessageView;
    private TextView mFirstMessageView;

    // SearchView to filter contact
    private SearchView mSearchView;

    private SmsStatistic                                   mStatistic;
    private List<Map.Entry<String, SmsInterface.SmsCount>> mTopContacts;
    private TableRow                                       mLastMessageRowView;
    private TableRow                                       mFirstMessageRowView;
    private ImageView                                      mFirstMessageInfoImage;
    private ImageView                                      mLastMessageInfoImage;

    @Override
    public void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);


        mStatistic = new SmsStatistic(getActivity());

                /*
         * An ImageLoader object loads and resizes an image in the background and binds it to the
         * QuickContactBadge in each item layout of the ListView. ImageLoader implements memory
         * caching for each image, which substantially improves refreshes of the ListView as the
         * user scrolls through it.
          */
        mImageLoader = new ImageLoader(getActivity(), getListPreferredItemHeight()) {
            @Override
            protected Bitmap processBitmap( Uri data ) {
                // This gets called in a background thread and passed the data from
                // ImageLoader.loadImage().
                Bitmap contactPhoto = loadContactPhotoThumbnail(data, getImageSize());

                // if image hasn't found, try workaround
                if(contactPhoto == null){
                    contactPhoto = loadContactPhotoThumbnailWorkaround(data);
                }

                return contactPhoto;
            }
        };

        // Set a placeholder loading image for the image loader
        mImageLoader.setLoadingImage(R.drawable.ic_contact_picture);
        // Add a cache to the image loader
        mImageLoader.addImageCache(getFragmentManager(), 0.1f);

        mAdapter = new TopContactsAdapter(getActivity(), mImageLoader);


    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        View rootView = inflater.inflate(R.layout.sms_statistic_fragment, container, false);


        mOverallView = ( TextView ) rootView.findViewById(R.id.overall);
        mOverallSentView = ( TextView ) rootView.findViewById(R.id.overallSent);
        mOverallReceivedView = ( TextView ) rootView.findViewById(R.id.overallReceived);

        mFirstMessageView = ( TextView ) rootView.findViewById(R.id.firstMessage);
        mLastMessageView = ( TextView ) rootView.findViewById(R.id.lastMessage);

        mFirstMessageRowView = ( TableRow ) rootView.findViewById(R.id.firstMessageRow);
        mLastMessageRowView = ( TableRow ) rootView.findViewById(R.id.lastMessageRow);

        mFirstMessageInfoImage = ( ImageView ) rootView.findViewById(R.id.info_image_first_message);
        mLastMessageInfoImage = ( ImageView ) rootView.findViewById(R.id.info_image_last_message);

        mSearchView = ( SearchView ) rootView.findViewById(R.id.searchView);
        mSearchView.setOnQueryTextListener(this);


        ListView topContactsListView = ( ListView ) rootView.findViewById(android.R.id.list);
        topContactsListView.setEmptyView(rootView.findViewById(android.R.id.empty));


        topContactsListView.setAdapter(mAdapter);

        topContactsListView.setOnItemClickListener(this);

        SharedPreferences settings = getActivity().getSharedPreferences(Preferences.PREF_FLAGS
                                                                                .PREF_NAME,
                                                                        Context.MODE_PRIVATE
        );
        boolean showInfo = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_SMS_STATISTIC_INFO,
                                               true);

        if ( showInfo ) { showInfoDialog(); }

        refreshView();


        return rootView;
    }

    @Override
    public void showInfoDialog() {

        // Build a new Dialog to tell the user to restart the phone
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.sms_statistic);

        builder.setMessage(R.string.sms_statistic_dialog_info);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                // Restore preferences
                SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                        .PREF_FLAGS.PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_SMS_STATISTIC_INFO, false);


                // Commit the edits!
                editor.commit();
            }
        });

        builder.create().show();
    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {


        super.onCreateOptionsMenu(menu, inflater);

        // Inflate the options menu from XML
        inflater.inflate(R.menu.sms_statistic_menu, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = ( SearchManager ) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if ( searchManager == null ) {
            throw new NullPointerException("Can't obtain Search Manager from system.");
        }

        // SMS search functionality
        MenuItem searchItem = menu.findItem(R.id.action_search);

        if ( searchItem != null ) {
            SearchView searchView = ( SearchView ) searchItem.getActionView();
            // Assumes current activity is the searchable activity
            searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName
                                                                                 (getActivity()
                                                                                          .getApplicationContext(), SearchSmsActivity.class)));
            searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by
            // default
        } else {
            Log.w(TAG, "SearchItem 'action_search' not found. Cannot set SearchableInfo.");
        }
    }


    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {

        switch ( item.getItemId() ) {
            case R.id.action_search:

                // No need to handle something here... will be handled in onCreateOptionsMenu
                break;

            case R.id.action_delete_search_history:
                showDeleteHistoryDialog();

                break;

            case R.id.action_filter_contacts:
                mSearchView.setVisibility(View.VISIBLE);
                break;


        }

        return super.onOptionsItemSelected(item);
    }

    private void showDeleteHistoryDialog() {

        // Build a new Dialog to let the user confirm the deletion of the search history.
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(android.R.drawable.ic_dialog_alert);


        builder.setTitle("Delete Search History");
        builder.setMessage("Are you sure to delete the recent search history. This can't be " +
                                   "undone!");

        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getActivity()
                                                                                          .getApplicationContext(),
                                                                                  SmsSuggestionProvider.AUTHORITY, SmsSuggestionProvider.MODE
                );
                suggestions.clearHistory();
            }
        });


        builder.setNegativeButton(android.R.string.cancel, null);
        Dialog dialog = builder.show();
        dialog.setCanceledOnTouchOutside(true);
    }

    private void loadView() {

        mOverallView.setText(String.valueOf(mStatistic.getSum()));
        mOverallSentView.setText(String.valueOf(mStatistic.getSumSent()));
        mOverallReceivedView.setText(String.valueOf(mStatistic.getSumReceived()));

        setFirstMessage();
        setLastMessage();

        mTopContacts = mStatistic.getTopContacts(TOP_RECEIVED_CONTACTS);

    }

    private void setLastMessage() {

        Sms lastMessage = mStatistic.getLastMessage();

        if ( lastMessage != null ) {

            Date date = new Date(lastMessage.getTime());

            // Listener that opens a dialog and shows the respective Message
            mLastMessageRowView.setOnClickListener(new MessageOnClickListener(getActivity(),
                                                                              lastMessage));

            mLastMessageView.setText(Utils.DATE_FORMAT.format(date));
        } else {
            mLastMessageInfoImage.setVisibility(View.GONE);
        }

    }

    private void setFirstMessage() {

        Sms firstMessage = mStatistic.getFirstMessage();

        if ( firstMessage != null ) {

            Date date = new Date(firstMessage.getTime());

            // Listener that opens a dialog and shows the respective Message
            mFirstMessageRowView.setOnClickListener(new MessageOnClickListener(getActivity(),
                                                                               firstMessage));

            mFirstMessageView.setText(Utils.DATE_FORMAT.format(date));
        } else {
            mFirstMessageInfoImage.setVisibility(View.GONE);
        }

    }

    private void refreshView() {


        loadView();

        mAdapter.clear();
        mAdapter.addAll(mTopContacts);
        mAdapter.notifyDataSetChanged();

    }


    @Override
    public void onPause() {
        super.onPause();

        // In the case onPause() is called during a fling the image loader is
        // un-paused to let any remaining background work complete.
        mImageLoader.setPauseWork(false);
    }


    /**
     * Gets the preferred height for each item in the ListView, in pixels, after accounting for
     * screen density. ImageLoader uses this value to resize thumbnail images to match the ListView
     * item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    private int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getActivity().getTheme().resolveAttribute(
                android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new DisplayMetrics();

        // Populate the DisplayMetrics
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return ( int ) typedValue.getDimension(metrics);
    }

    /**
     * Workaround for {@link SmsStatisticFragment#loadContactPhotoThumbnail(android.net.Uri, int)},
     * cause on some devices (Galaxy S3 with 4.4.2) no photo image were found. Similar to normal
     * method but uses a {@link java.io.InputStream} instead of {@link
     * android.content.res.AssetFileDescriptor}
     * <p/>
     * Found Solution at
     * <a href="http://stackoverflow
     * .com/questions/20475495/newfromfd-failed-in-nativedecodefiledescriptor-android-4-4
     * ">stackoverflow.com</a>.
     */
    private Bitmap loadContactPhotoThumbnailWorkaround( Uri photoData ) {


        InputStream inputStream = null;

        try {

            inputStream = getActivity().getContentResolver().openInputStream(photoData);
            if ( inputStream != null ) {
                return BitmapFactory.decodeStream(inputStream);

            }
        } catch ( FileNotFoundException e ) {
            Log.d(TAG, "Contact photo thumbnail not found for contact " + photoData
                        + ": " + e.toString());

        } finally {
            if ( inputStream != null ) {
                try {
                    inputStream.close();
                } catch ( IOException e ) {
                    // ignored
                }
            }
        }

        // If the decoding failed, returns null
        return null;
    }

    /**
     * Decodes and scales a contact's image from a file pointed to by a Uri in the contact's data,
     * and returns the result as a Bitmap. The column that contains the Uri varies according to the
     * platform version.
     *
     * @param photoData For platforms prior to Android 3.0, provide the Contact._ID column value.
     *                  For Android 3.0 and later, provide the Contact.PHOTO_THUMBNAIL_URI value.
     * @param imageSize The desired target width and height of the output image in pixels.
     *
     * @return A Bitmap containing the contact's image, resized to fit the provided image size. If
     * no thumbnail exists, returns null.
     */
    private Bitmap loadContactPhotoThumbnail( Uri photoData, int imageSize ) {


        // Instantiates an AssetFileDescriptor. Given a content Uri pointing to an image file, the
        // ContentResolver can return an AssetFileDescriptor for the file.
        AssetFileDescriptor afd = null;
        // This "try" block catches an Exception if the file descriptor returned from the Contacts
        // Provider doesn't point to an existing file.
        try {

            // Retrieves a file descriptor from the Contacts Provider. To learn more about this
            // feature, read the reference documentation for
            // ContentResolver#openAssetFileDescriptor.
            afd = getActivity().getContentResolver().openAssetFileDescriptor(photoData, "r");
            // Gets a FileDescriptor from the AssetFileDescriptor. A BitmapFactory object can
            // decode the contents of a file pointed to by a FileDescriptor into a Bitmap.
            FileDescriptor fileDescriptor = afd.getFileDescriptor();

            if ( afd != null ) {
                // Decodes a Bitmap from the image pointed to by the FileDescriptor, and scales it
                // to the specified width and height
                return ImageLoader.decodeSampledBitmapFromDescriptor(
                        fileDescriptor, imageSize, imageSize);

            }
        } catch ( FileNotFoundException e ) {
            // If the file pointed to by the thumbnail URI doesn't exist, or the file can't be
            // opened in "read" mode, ContentResolver.openAssetFileDescriptor throws a
            // FileNotFoundException.
            Log.d(TAG, "Contact photo thumbnail not found for contact " + photoData
                        + ": " + e.toString());

        } finally {
            // If an AssetFileDescriptor was returned, try to close it
            if ( afd != null ) {
                try {
                    afd.close();
                } catch ( IOException e ) {
                    // Closing a file descriptor might cause an IOException if the file is
                    // already closed. Nothing extra is needed to handle this.
                }
            }
        }

        // If the decoding failed, returns null
        return null;
    }


    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {

        final Map.Entry<String, SmsInterface.SmsCount> item = ( Map.Entry<String,
                SmsInterface.SmsCount> ) parent.getItemAtPosition(position);
        final String number = item.getKey();

        /*
         * Start a new Activity when clicking on a Top Contact.
         * Put number as an extra so the new Activity can display the right contact.
         */
        Intent intent = new Intent(getActivity(), ContactStatisticActivity.class);
        intent.putExtra(EXTRA_NUMBER, number);

        startActivity(intent);


    }

    // Searchview implementation
    @Override
    public boolean onQueryTextSubmit( String query ) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onQueryTextChange( String newText ) {


        // Called when the action bar search text has changed.  Since this
        // is a simple array adapter, we can just have it do the filtering.
        String curFilter;
        if ( TextUtils.isEmpty(newText) ) {
            curFilter = null;

            // start a handler that removes after a specific delay the Search view if the user
            // didn't change the query meanwhile
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSearchView.setVisibility(View.GONE);
                }
            }, DELAY_HIDE_CONTACTS_SEARCH_MILLIS);


        } else {
            curFilter = newText;
        }
        mAdapter.getFilter().filter(curFilter);
        return true;
    }

}
