/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.utils.Utils;

import java.util.Date;
import java.util.List;

/**
 * ArrayAdapter to display send/received message from/to a number. Displays the name of the contact
 * (or number), the message itself, the time of sent/receive and a send or received icon.
 *
 * @see {@link at.ac.uibk.awarenessy.dao.Sms}
 */
public class SmsAdapter extends ArrayAdapter<Sms> {

    private final Context mContext;

    public SmsAdapter(Context context, List<Sms> smsList) {
        super(context, android.R.layout.simple_list_item_2, smsList);
        mContext = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View smsView = convertView;

        Sms sms = getItem(position);

        // getName returns the name to the number, or the number if no name exists
        String name = SmsInterface.getName(sms.getNumber(), mContext);

        if (smsView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            smsView = inflater.inflate(R.layout.sms_sms_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) smsView.findViewById(R.id.name);
            viewHolder.inOutIcon = (ImageView) smsView.findViewById(R.id.inOutIcon);
            viewHolder.message = (TextView) smsView.findViewById(R.id.message);
            viewHolder.time = (TextView) smsView.findViewById(R.id.time);
            smsView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) smsView.getTag();

        Date date = new Date(sms.getTime());

        holder.name.setText(name);
        holder.message.setText(sms.getMessage());
        holder.time.setText(Utils.DATE_FORMAT.format(date));

        // set icon whether sms was sent or received
        if (sms.getSent()) {
            holder.inOutIcon.setImageResource(R.drawable.ic_sms_sent);
        } else {
            holder.inOutIcon.setImageResource(R.drawable.ic_sms_received);
        }

        // Returns the item layout view
        return smsView;


    }

    static class ViewHolder {
        TextView  name;
        ImageView inOutIcon;
        TextView  message;
        TextView  time;
    }
}
