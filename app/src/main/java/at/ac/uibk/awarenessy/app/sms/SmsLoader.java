/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.content.AsyncTaskLoader;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import at.ac.uibk.awarenessy.app.utils.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * AsyncTaskLoader that loads messages from the inbox or sent messages in the background.
 */
public class SmsLoader extends AsyncTaskLoader<List<Sms>> {


    private static final String TAG = SmsLoader.class.getSimpleName();
    private final long                 mTime;
    private       Uri                  mUri;
    private       SmsInterface.SmsType mLoadType;
    private       List<Sms>            mSms;


    public SmsLoader(Context context, SmsInterface.SmsType loadType, long time) {

        super(context);

        mTime = time;

        // decide if sent or received sms should be loaded
        switch (loadType) {

            case SENT:
                mUri = SmsInterface.CONTENT_SMS_SENT;
                mLoadType = loadType;
                break;
            case RECEIVED:
                mUri = SmsInterface.CONTENT_SMS_INBOX;
                mLoadType = loadType;
                break;

            default:
                throw new IllegalArgumentException("Invalid Uri. Only " + SmsInterface
                        .CONTENT_SMS_INBOX + " or " + SmsInterface.CONTENT_SMS_SENT + " allowed.");

        }


    }

    @Override
    public List<Sms> loadInBackground() {

        ContentResolver cr = getContext().getContentResolver();

        // Get all messages from the given Uri (Sent or Received) after the given Time
        Cursor cursor = cr.query(mUri, null, SmsInterface.DATE + " > ?", new String[]{
                String.valueOf(mTime)}, null);

        Locale locale = Locale.getDefault();
        String countryCode = locale.getCountry();

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();


        try {

            int indexMessage = cursor.getColumnIndexOrThrow(SmsInterface.BODY);
            int indexNumber = cursor.getColumnIndexOrThrow(SmsInterface.ADDRESS);
            int indexTime = cursor.getColumnIndexOrThrow(SmsInterface.DATE);

            List<Sms> smsList = new ArrayList<Sms>();

            if (indexMessage < 0 || !cursor.moveToFirst()) {
                return smsList;
            }


            do {
                String number = cursor.getString(indexNumber);
                String message = cursor.getString(indexMessage);
                long time = cursor.getLong(indexTime);


                // format number to international format (e.g. +43 664 12345678 for AT)
                try {
                    Phonenumber.PhoneNumber numberProto = phoneUtil.parse(number, countryCode);

                    number = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);

                } catch (NumberParseException e) {
//                    System.err.println("NumberParseException was thrown: " + e.toString());
                }




                Long locationId = null;

                // check only location if we insert sent messages, cause receiving ones will get
                // there location in {@link SmsReceiver}
                if (mLoadType == SmsInterface.SmsType.SENT) {
                    locationId = LocationUtils.getLocationForTime(getContext(), time);
                }

                Sms sms = new Sms(null, number, message, time, mLoadType == SmsInterface.SmsType
                        .SENT, locationId);
                smsList.add(sms);
            } while (cursor.moveToNext());


            return smsList;
        } finally {
            cursor.close();

        }
    }

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(List<Sms> apps) {

        if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (apps != null) {
                onReleaseResources(apps);
            }
        }
        List<Sms> oldApps = mSms;
        mSms = apps;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(apps);
        }

        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldApps != null) {
            onReleaseResources(oldApps);
        }
    }

    /**
     * Handles a request to start the Loader.
     */
    @Override
    protected void onStartLoading() {


        if (takeContentChanged() || mSms == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }

    /**
     * Handles a request to stop the Loader.
     */
    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(List<Sms> apps) {

        super.onCanceled(apps);

        // At this point we can release the resources associated with 'apps'
        // if needed.
        onReleaseResources(apps);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {

        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        // At this point we can release the resources associated with 'apps'
        // if needed.
        if (mSms != null) {
            onReleaseResources(mSms);
            mSms = null;
        }


    }

    /**
     * Helper function to take care of releasing resources associated
     * with an actively loaded data set.
     */
    protected void onReleaseResources(List<Sms> apps) {
        // For a simple List<> there is nothing to do.  For something
        // like a Cursor, we would close it here.
    }
}

