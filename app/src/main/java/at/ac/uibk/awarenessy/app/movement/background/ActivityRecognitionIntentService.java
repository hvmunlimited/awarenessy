/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import at.ac.uibk.awarenessy.app.utils.Log;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.utils.LogFile;
import at.ac.uibk.awarenessy.app.utils.Preferences;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

/**
 * Service that receives ActivityRecognition updates.
 * <p/>
 * Possible activitiy types are:
 * <ul>
 * <li>{@link com.google.android.gms.location.DetectedActivity#IN_VEHICLE}</li>
 * <li>{@link com.google.android.gms.location.DetectedActivity#ON_BICYCLE}</li>
 * <li>{@link com.google.android.gms.location.DetectedActivity#ON_FOOT}</li>
 * <li>{@link com.google.android.gms.location.DetectedActivity#STILL}</li>
 * <li>{@link com.google.android.gms.location.DetectedActivity#TILTING}</li>
 * <li>{@link com.google.android.gms.location.DetectedActivity#UNKNOWN}</li>
 * </ul>
 * Where the first three are so called <b>'Moving Activities'</b>, STILL a <b>'Still Activity'</b>
 * and the latter two
 * <b>'Meaningless Activities'</b>.
 * <p/>
 * The last known activity is stored in SharedPreferences in {@link at.ac.uibk.awarenessy
 * .app.utils.Preferences.APP_DATA}.
 * If a new activity is
 * recognized, the service checks with {@link at.ac.uibk.awarenessy.app.movement.background
 * .ActivityRecognitionIntentService#activityChanged}
 * if the new activity has changed in an interesting way.<p/>
 * There are two cases of interesting changes that can occur:
 * <ul>
 * <li>STILL to MOVING:</li> User starts to move (either on_foot, on_bicycle or in_vehicle), so
 * start tracking
 * location. The service sends a broadcast with the action
 * {@link at.ac.uibk.awarenessy.app.movement.LocationUtils#ACTION_START_LOCATION_UPDATES}.
 * <li>MOVING to STILL:</li> User stops moving, so stop tracking location. The service sends a
 * broadcast with the
 * action {@link at.ac.uibk.awarenessy.app.movement
 * .LocationUtils#ACTION_STOP_LOCATION_UPDATES}.
 * <p/>
 * </ul>
 * Every time a moving activity has been recognized, a broadcast will be send to ensure that the
 * service is restarted if Android has killed it.
 * <p/>
 * Both broadcasts will be received from {@link BackgroundMovementManager} that either starts or
 * stops the {@link
 * BackgroundLocationService}
 * <p/>
 * Taken and adapted from <a href="https://developer.android
 * .com/training/location/activity-recognition.html">Android
 * Developers</a>
 */

public class ActivityRecognitionIntentService extends IntentService {


    private static final String TAG = ActivityRecognitionIntentService.class.getSimpleName();

    // Store the app's shared preferences repository
    private SharedPreferences mPrefs;


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public ActivityRecognitionIntentService() {
        super(TAG);
    }

    /**
     * Determine if an activity means that the user is moving.
     *
     * @param type The type of activity the user is doing (see DetectedActivity constants)
     * @return true if the user seems to be moving from one location to another, otherwise false
     */
    public static boolean isMoving(int type) {
        switch (type) {
            // These types mean that the user is probably not moving
            case DetectedActivity.STILL:
            case DetectedActivity.TILTING:
            case DetectedActivity.UNKNOWN:
                return false;
            default:
                return true;
        }
    }

    /**
     * Map detected activity types to strings
     *
     * @param activityType The detected activity type
     * @return A user-readable name for the type
     */
    private String getNameFromType(int activityType) {
        switch (activityType) {
            case DetectedActivity.IN_VEHICLE:
                return getString(R.string.vehicle);
            case DetectedActivity.ON_BICYCLE:
                return getString(R.string.bicycle);
            case DetectedActivity.RUNNING:
                return getString(R.string.running);
            case DetectedActivity.WALKING:
                return getString(R.string.walking);
            case DetectedActivity.ON_FOOT:
                return getString(R.string.foot);
            case DetectedActivity.STILL:
                return getString(R.string.still);
            case DetectedActivity.UNKNOWN:
                return getString(R.string.unknown);
            case DetectedActivity.TILTING:
                return getString(R.string.tilting);
        }
        return getString(R.string.unknown);
    }

    /**
     * Called when a new activity detection update is available.
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d(TAG, "New Intent: " + (intent != null ? intent.toString() : "null"));

        // Get a handle to the repository
        mPrefs = getApplicationContext().getSharedPreferences(Preferences.PREF_FLAGS.PREF_NAME,
                Context.MODE_PRIVATE);


        // check if we should really track recognition (if request comes from BOOT_COMPLETED we
        // get updates till the next reboot, even if turned off)
        boolean isTrackingOn = mPrefs.getBoolean(Preferences.PREF_FLAGS.TRACK_POSITION, false);
        if (!isTrackingOn) {
            Log.d(TAG, "Mistaken intent received. Ignore intent.");
            return;
        }

        // If the incoming intent contains an update
        if (ActivityRecognitionResult.hasResult(intent)) {
            // Get the update
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            // Get the most probable activity
            DetectedActivity mostProbableActivity = result.getMostProbableActivity();

            /*
             * Get the probability that this activity is the
             * the user's actual activity
             */
            int confidence = mostProbableActivity.getConfidence();
            /*
             * Get an integer describing the type of activity
             */
            int activityType = mostProbableActivity.getType();
            String activityName = getNameFromType(activityType);

            /*
             * At this point, we have retrieved all the information
             * for the current update. We check if the activity is useful (moving or still and
             * enough confidence). In this case send an intent to the
              * BackgroundLocationService.
             */

            Log.d(TAG, "New Activity recognized: " + activityName + " with " + confidence + " % " +
                    "confidence");

            // store the new type
            mPrefs.edit().putInt(Preferences.APP_DATA.KEY_PREVIOUS_ACTIVITY_TYPE,
                    activityType).commit();


            // need reasonably sure recognitions
            if (confidence >= 50) {

                // first case: change movement
                if (isMoving(activityType)) {

                    Log.d(TAG, "New moving activity update. Send Broadcast to (re)START location " +
                            "update service");

                    Intent startIntent = new Intent(LocationUtils.ACTION_START_LOCATION_UPDATES);
                    startIntent.putExtra(LocationUtils.EXTRA_ACTIVITY_TYPE, activityType);

                    sendBroadcast(startIntent);

                    // Log the update
                    logActivityRecognitionResult(mostProbableActivity);


                    // second case: MOVING to STILL
                } else if (activityType == DetectedActivity.STILL && activityChanged
                        (activityType)) {
                    Log.d(TAG, "Still and changed. Send broadcast to STOP service.");
                    sendBroadcast(new Intent(LocationUtils.ACTION_STOP_LOCATION_UPDATES));

                    // Log the update
                    logActivityRecognitionResult(mostProbableActivity);


                }

            }


        } else {

            /*
             * This implementation ignores intents that don't contain
             * an activity update. If you wish, you can report them as
             * errors.
             */
            Log.d(TAG, "No Activity Update found");

        }

    }

    /**
     * Tests to see if the activity has changed from the previous.
     *
     * @param currentType The current activity type
     * @return true if the user's current activity is different from the previous most probable
     * activity; otherwise,
     * false.
     */
    private boolean activityChanged(int currentType) {

        // Get the previous type, otherwise return the "unknown" type
        int previousType = mPrefs.getInt(Preferences.APP_DATA.KEY_PREVIOUS_ACTIVITY_TYPE,
                DetectedActivity.UNKNOWN);

        Log.d(TAG, "Prev Activity: " + getNameFromType(previousType) + "\tCurr Activity: " +
                getNameFromType(currentType));


        // If the previous type isn't the same as the current type, the activity has changed
        if (previousType != currentType) {
            return true;

            // Otherwise, it hasn't.
        } else {
            return false;
        }
    }

    /**
     * Write the activity recognition update to the log file
     *
     * @param detectedActivity The result extracted from the incoming Intent
     */
    private void logActivityRecognitionResult(DetectedActivity detectedActivity) {

        // Get the activity type, confidence level, and human-readable name
        int activityType = detectedActivity.getType();
        int confidence = detectedActivity.getConfidence();
        String activityName = getNameFromType(activityType);


        // Get the current log file or create a new one, then log the activity
        LogFile.getInstance(getApplicationContext()).log(LogFile
                        .LOG_FILE_ACTIVITY_RECOGNITION_INTENT_SERVICE,
                getString(R.string.log_message_activity_recognition, activityName, confidence)
        );

        // send a local broadcast that the LogActivity knows that a new log line is written
        Intent intent = new Intent(LogFile.ACTION_NEW_LOG);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

}
