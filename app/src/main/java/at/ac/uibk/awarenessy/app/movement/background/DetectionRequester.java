

/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement.background;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import at.ac.uibk.awarenessy.app.utils.Log;

import at.ac.uibk.awarenessy.app.movement.LocationUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.ActivityRecognitionClient;

/**
 * Class for connecting to Location Services and activity recognition updates.
 * <b>
 * Note: Clients must ensure that Google Play services is available before requesting updates.
 * </b> Use GooglePlayServicesUtil.isGooglePlayServicesAvailable() to check.
 * <p/>
 * <p/>
 * To use a DetectionRequester, instantiate it and call requestUpdates(). Everything else is done
 * automatically.
 * <p/>
 * Taken and adapted from
 * <a href="https://developer.android.com/training/location/activity-recognition.html">Android
 * Developers</a>
 */
public class DetectionRequester implements ConnectionCallbacks, OnConnectionFailedListener {

    private static final String TAG = DetectionRequester.class.getSimpleName();

    // Storage for a context from the calling client
    private final Context mContext;

    // Stores the PendingIntent used to send activity recognition events back to the app
    private PendingIntent mActivityRecognitionPendingIntent;

    // Stores the current instantiation of the activity recognition client
    private ActivityRecognitionClient mActivityRecognitionClient;

    public DetectionRequester( Context context ) {
        // Save the context
        mContext = context;

        // Initialize the globals to null
        mActivityRecognitionPendingIntent = null;
        mActivityRecognitionClient = null;

    }

    /**
     * Returns the current PendingIntent to the caller.
     *
     * @return The PendingIntent used to request activity recognition updates
     */
    public PendingIntent getRequestPendingIntent() {
        return mActivityRecognitionPendingIntent;
    }

    /**
     * Sets the PendingIntent used to make activity recognition update requests
     *
     * @param intent The PendingIntent
     */
    public void setRequestPendingIntent( PendingIntent intent ) {
        mActivityRecognitionPendingIntent = intent;
    }

    /**
     * Start the activity recognition update request process by
     * getting a connection.
     */
    public void requestUpdates() {
        requestConnection();
    }

    /**
     * Make the actual update request. This is called from onConnected().
     */
    private void continueRequestActivityUpdates() {
        /*
         * Request updates, using the default detection interval.
         * The PendingIntent sends updates to ActivityRecognitionIntentService
         */
        getActivityRecognitionClient().requestActivityUpdates(LocationUtils
                                                                      .DETECTION_INTERVAL_MILLISECONDS, createRequestPendingIntent());
        // Disconnect the client
        requestDisconnection();
    }

    /**
     * Request a connection to Location Services. This call returns immediately,
     * but the request is not complete until onConnected() or onConnectionFailure() is called.
     */
    private void requestConnection() {
        getActivityRecognitionClient().connect();
    }

    /**
     * Get the current activity recognition client, or create a new one if necessary.
     * This method facilitates multiple requests for a client, even if a previous
     * request wasn't finished. Since only one client object exists while a connection
     * is underway, no memory leaks occur.
     *
     * @return An ActivityRecognitionClient object
     */
    private ActivityRecognitionClient getActivityRecognitionClient() {
        if ( mActivityRecognitionClient == null ) {

            mActivityRecognitionClient = new ActivityRecognitionClient(mContext, this, this);
        }
        return mActivityRecognitionClient;
    }

    /**
     * Get the current activity recognition client and disconnect from Location Services
     */
    private void requestDisconnection() {
        getActivityRecognitionClient().disconnect();
    }

    /*
     * Called by Location Services once the activity recognition client is connected.
     *
     * Continue by requesting activity updates.
     */
    @Override
    public void onConnected( Bundle arg0 ) {
        // If debugging, log the connection
        Log.d(TAG, "Connected");

        // Continue the process of requesting activity recognition updates
        continueRequestActivityUpdates();
    }

    /*
     * Called by Location Services once the activity recognition client is disconnected.
     */
    @Override
    public void onDisconnected() {
        // In debug mode, log the disconnection
        Log.d(TAG, "Disconnected");

        // Destroy the current activity recognition client
        mActivityRecognitionClient = null;
    }

    /**
     * Get a PendingIntent to send with the request to get activity recognition updates. Location
     * Services issues the Intent inside this PendingIntent whenever a activity recognition update
     * occurs.
     *
     * @return A PendingIntent for the IntentService that handles activity recognition updates.
     */
    private PendingIntent createRequestPendingIntent() {

        // If the PendingIntent already exists
        if ( null != getRequestPendingIntent() ) {

            // Return the existing intent
            return mActivityRecognitionPendingIntent;

            // If no PendingIntent exists
        } else {
            // Create an Intent pointing to the IntentService
            Intent intent = new Intent(mContext, ActivityRecognitionIntentService.class);


            /*
             IMPORTANT!! According to Google Example "https://developer.android
             .com/training/location/activity-recognition.html" use FLAG_UPDATE_CURRENT,
             but this does make problem when uninstalling and reinstalling app,
             while ActivityRecognitionIntentService is turned on.
             But FLAG_CANCEL_CURRENT fix this problem.
              */

            /* COMMENT FROM EXAMPLE
             * Return a PendingIntent to start the IntentService.
             * Always create a PendingIntent sent to Location Services
             * with FLAG_UPDATE_CURRENT, so that sending the PendingIntent
             * again updates the original. Otherwise, Location Services
             * can't match the PendingIntent to requests made with it.
             */
            PendingIntent pendingIntent = PendingIntent.getService(mContext, 0, intent,
                                                                   PendingIntent
                                                                           .FLAG_CANCEL_CURRENT
            );

            setRequestPendingIntent(pendingIntent);
            return pendingIntent;
        }

    }

    /*
     * Implementation of OnConnectionFailedListener.onConnectionFailed
     * If a connection or disconnection request fails, report the error
     * connectionResult is passed in from Location Services
     */
    @Override
    public void onConnectionFailed( ConnectionResult connectionResult ) {

        Log.d(TAG, "Connection failed");

        // Display Error Dialog
        Intent intent = new Intent(LocationUtils.ACTION_PLAY_SERVICE_ERROR);
        intent.putExtra(LocationUtils.EXTRA_ERROR_CODE, connectionResult.getErrorCode());

        mContext.sendBroadcast(intent);
    }

}
