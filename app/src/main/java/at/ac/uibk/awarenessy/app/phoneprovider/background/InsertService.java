/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider.background;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsMessage;

import at.ac.uibk.awarenessy.app.utils.Log;

import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;

import java.util.ArrayList;
import java.util.List;

import at.ac.uibk.awarenessy.app.BuildConfig;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.phoneprovider.PhoneUtils;
import at.ac.uibk.awarenessy.app.sms.SmsInterface;
import at.ac.uibk.awarenessy.app.utils.LogFile;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.Call;
import at.ac.uibk.awarenessy.dao.CallDao;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.SimpleLocation;
import at.ac.uibk.awarenessy.dao.SimpleLocationDao;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.dao.SmsDao;

/**
 * Service that requests the location and then inserts a Sms or Call into the database.
 * <p/>
 * Start this service with {@link at.ac.uibk.awarenessy.app.phoneprovider
 * .PhoneUtils#ACTION_INSERT_CALL}
 * or {@link at.ac.uibk.awarenessy.app.phoneprovider.PhoneUtils#ACTION_INSERT_SMS} to insert
 * a
 * Sms or a Call, respectively.
 */
public class InsertService extends Service implements GooglePlayServicesClient
        .ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    private static final String TAG = InsertService.class.getSimpleName();

    private SimpleLocationDao mLocationDao;
    private CallDao           mCallDao;
    private SmsDao            mSmsDao;

    private LocationClient mLocationClient;
    private Call           mCall;
    private List<Sms>      mSmsList;

    private PhoneUtils.INSERT_TYPE mInsertType;
    private DaoSession mDaoSession;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getAction();

        if (PhoneUtils.ACTION_INSERT_CALL.equals(action)) {

            mInsertType = PhoneUtils.INSERT_TYPE.CALL;

            mCall = (Call) intent.getSerializableExtra(PhoneUtils.EXTRA_CALL);


        } else if (PhoneUtils.ACTION_INSERT_SMS.equals(action)) {

            mInsertType = PhoneUtils.INSERT_TYPE.SMS;

            Object[] smsExtras = (Object[]) intent.getSerializableExtra(PhoneUtils.EXTRA_SMS);

            mSmsList = new ArrayList<Sms>(smsExtras.length);

            String toast = "";

            for (int i = 0; i < smsExtras.length; i++) {
                Object smsExtra = smsExtras[i];
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) smsExtra);

                String message = smsMessage.getMessageBody();
                String number = smsMessage.getOriginatingAddress();

                number = Utils.removeWhitespaces(number);

                long time = smsMessage.getTimestampMillis();

                toast += "SMS from " + SmsInterface.getName(number,
                        this)
                         + " :\n";
                toast += message;

                // if sms contains of multiple sms, add newline
                if (i != smsExtras.length - 1) {
                    toast += "\n";
                }

                Sms sms = new Sms(null, number, message, time, false, null);
                mSmsList.add(sms);
            }

            if (BuildConfig.DEBUG) {
                Toast.makeText(this, toast, Toast.LENGTH_LONG).show();
            }

        } else {
            Log.d(TAG, "Unknown Action. Stop self");
            stopSelf();
        }
        setUpLocationClientIfNeeded();
        mLocationClient.connect();

        return Service.START_REDELIVER_INTENT;


    }

     /*
    * Create a new location client, using the enclosing class to
    * handle callbacks.
    */

    private void setUpLocationClientIfNeeded() {

        if (mLocationClient == null) {
            mLocationClient = new LocationClient(this, this, this);
        }
    }

    /**
     * Set up db to store new locations
     */
    private void setUpDb() {


        mDaoSession = Utils.getDaoSession(this);
        mLocationDao = mDaoSession.getSimpleLocationDao();
        mCallDao = mDaoSession.getCallDao();
        mSmsDao = mDaoSession.getSmsDao();
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {

        Location location = mLocationClient.getLastLocation();
        SimpleLocation simpleLocation = null;

        setUpDb();

        if (location != null) {
            simpleLocation = new SimpleLocation(location);
            simpleLocation.setIsTrackingPoint(false);

            // Calc address
            String address = LocationUtils.calcLocationAddress(this, simpleLocation);
            simpleLocation.setAddress(address);
            Log.d(TAG, "Adress: " + simpleLocation.getAddress());


            mLocationDao.insert(simpleLocation);

            Log.d(TAG, "Location inserted: " + simpleLocation.toString());

        } else {
            Log.d(TAG, "No location found, insert without");
        }

        switch ( mInsertType ) {

            case CALL:
                // insert call with the given location
                insertCall(simpleLocation);
                break;
            case SMS:
                // insert sms with the given location
                insertSms(simpleLocation);
                break;
        }


        stopSelf();

    }

    private void insertSms( SimpleLocation simpleLocation ) {

        for ( Sms sms : mSmsList ) {

            if ( simpleLocation != null ) {
                // location isn't tracked in at.ac.uibk.awarenessy.app.movement.background
                // .BackgroundLocationService
                simpleLocation.setIsTrackingPoint(false);
//                simpleLocation.setSms(sms);

                sms.setSimpleLocation(simpleLocation);

            }

            mSmsDao.insert(sms);

            Log.d(TAG, "Sms inserted: " + sms.toString());
            LogFile.getInstance(this).log(LogFile.LOG_FILE_SMS, sms.toStringLog(this));
        }
    }

    private void insertCall( SimpleLocation simpleLocation ) {

        if ( simpleLocation != null ) {

            // location isn't tracked in at.ac.uibk.awarenessy.app.movement.background
            // .BackgroundLocationService
            simpleLocation.setIsTrackingPoint(false);


            mCall.setSimpleLocation(simpleLocation);
        }

        mCallDao.insert(mCall);

        Log.d(TAG, "Call inserted: " + mCall.toString());


    }

    @Override
    public void onDisconnected() {

        Log.d(TAG, "On Disconnected");

        setUpDb();

        mLocationClient = null;

        switch ( mInsertType ) {

            case CALL:
                // insert call with no location
                insertCall(null);
                break;
            case SMS:
                // insert sms with no location
                insertSms(null);
                break;
        }

        stopSelf();

    }

    @Override
    public void onConnectionFailed( ConnectionResult connectionResult ) {

        Log.d(TAG, "On Connection Failed");

        setUpDb();

        switch ( mInsertType ) {

            case CALL:
                // insert call with no location
                insertCall(null);
                break;
            case SMS:
                // insert sms with no location
                insertSms(null);
                break;
        }

        stopSelf();
    }
}
