/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.main;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import at.ac.uibk.awarenessy.app.R;

/**
 * Every fragment in the navigation drawer menu has to extend this class.
 * <p/>
 * Makes sure that every old menu item is removed (menu.clear), adds the main menu and inflates the
 * information menu which each menu fragment has to provide.
 * <p/>
 * A extending Fragment has to implement {@link ModuleFragment#showInfoDialog()} which has to open a Dialog
 * and show the usage of the fragment.
 */
public abstract class ModuleFragment extends Fragment {

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        // We have a menu item to show in action bar.
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {

        // clear possible menus from other fragments and inflate the standard main menu from the top activity
        menu.clear();
        inflater.inflate(R.menu.main, menu);

        // Place an action bar item which display the info dialog.
        inflater.inflate(R.menu.info_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {

        switch ( item.getItemId() ) {
            case R.id.action_info:
                showInfoDialog();
                return true;
            default:

                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Show an info dialog, what the usage and purpose of this Fragment/Module is.
     */
    public abstract void showInfoDialog();
}
