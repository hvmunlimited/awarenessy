/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represent a Place that has been tagged on Facebook.
 * <p/>
 * It consists of a Latitude/Longitude Position and the address of the location.
 *
 * @see  <a href="https://developers.facebook.com/docs/graph-api/reference/user/locations/">Facebook Developers</a>
 *
 */
public class FbPlace {

    // json names defined by facebook's Graph API
    private static final String CITY    = "city";
    private static final String COUNTRY = "country";
    private static final String STREET  = "street";

    private String mName;
      private LatLng mLatLng;
    private String mAddress;

    public FbPlace(JSONObject jsonFbPlace) throws JSONException {
        mName = jsonFbPlace.getString("name");

        JSONObject location = jsonFbPlace.getJSONObject
                ("location");

        double latitude = location.getDouble("latitude");
        double longitude = location.getDouble("longitude");

        mLatLng = new LatLng(latitude, longitude);

        String city = location.has(CITY) ? location.getString(CITY) : null;
        String country = location.has(COUNTRY) ? location.getString(COUNTRY) : null;
        String street = location.has(STREET) ? location.getString(STREET) : null;

        // if no address has been found, formatAddress returns "";
        mAddress = LocationUtils.formatAddress(street, city, country);

    }


    @Override
    public String toString() {
        return mName + " Lat/Long: " + mLatLng + " Address: " + mAddress;
    }

    public LatLng getLatLng() {
        return mLatLng;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getName() {
        return mName;
    }


}
