package at.ac.uibk.awarenessy.dao;

/**
 * Interface for Database Tables to generify the export of these tables.
 * Tables can be exported in {@link at.ac.uibk.awarenessy.app.main.SettingsFragment}
 * This interface has to be implemented of generated Entity files like {@link Sms}, {@link Call},
 * ..., so it has to be set in the generating method in Generator.java (in daoGenerator module).
 */
public interface Table {

    String[] toCSV();

}
