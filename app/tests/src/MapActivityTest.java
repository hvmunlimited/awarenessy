/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import at.ac.uibk.awarenessmonitor.app.R;


public class MapActivityTest extends ActivityInstrumentationTestCase2<MapActivity> {


    private MapActivity mapActivity;
    private GoogleMap map;

    public MapActivityTest() {
        super(MapActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);

        mapActivity = getActivity();

        map = ((MapFragment) mapActivity.getFragmentManager().findFragmentById(R.id.map)).getMap();

    }

    public void test_toggleMapType() {

    // check if initial map layer is correct
        mapActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int mapTypeBefore = map.getMapType();
                // valid map type
                assertEquals(mapTypeBefore, GoogleMap.MAP_TYPE_NORMAL);

            }
        });


         View changeLayerButton =  mapActivity.findViewById(R.id.action_change_map_layer);
        TouchUtils.clickView(this, changeLayerButton);

        // check type after click
        mapActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int mapTypeBefore = map.getMapType();
                // valid map type
                assertEquals(mapTypeBefore, GoogleMap.MAP_TYPE_HYBRID);

            }
        });



        // click 2nd time
        TouchUtils.clickView(this, changeLayerButton);

        // check if map type is again the intial state
        mapActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int mapTypeBefore = map.getMapType();
                // valid map type
                assertEquals(mapTypeBefore, GoogleMap.MAP_TYPE_NORMAL);

            }
        });
    }
}

